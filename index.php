<?php
	// error_reporting(E_ALL);
	// ini_set('display_errors', '1');

	require_once "controladores/rutas.controlador.php";
	require_once "controladores/plantilla.controlador.php";
	require_once "controladores/usuarios.controlador.php";
	require_once "controladores/tickets.controlador.php";
	require_once "controladores/clientes.controlador.php";
	require_once "controladores/notificaciones.controlador.php";
	require_once "controladores/proyectos.controlador.php";

	require_once "modelos/usuarios.modelo.php";
	require_once "modelos/tickets.modelo.php";
	require_once "modelos/clientes.modelo.php";
	require_once "modelos/notificaciones.modelo.php";
	require_once "modelos/proyectos.modelo.php";

	$plantilla = new ControladorPlantilla();
	$plantilla -> ctrPlantilla();
