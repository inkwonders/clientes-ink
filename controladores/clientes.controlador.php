<?php

  class Clientes{

    static public function ctrConsultaClientes($tabla, $tabla2, $condicion){						

			$respuesta = ModeloClientes::mdlConsultaClientes($tabla, $tabla2, $condicion);		  
			return $respuesta;		  
			
		}

	static public function ctrConsultaCliente($tabla,$id_cliente){						

			$respuesta = ModeloClientes::mdlConsultaCliente($tabla, $id_cliente);		  
			return $respuesta;		  
			
		}

    static public function ctrUUID(){						

			$respuesta = ModeloClientes::mdlUUID();		  
			return $respuesta;		  
			
		}


	static public function ctrIngresoCliente($tabla, $id_usuario, $nombre_cliente, $activo, $fecha, $usuario_alta, $code, $hash){						

			$respuesta = ModeloClientes::mdlInsertarClientes($tabla, $id_usuario, $nombre_cliente, $activo, $fecha, $usuario_alta, $code, $hash);		  
			return $respuesta;
			
		}

	static public function ctrEditarCliente($tabla, $nombre_cliente_editado, $id_cliente, $id_admin, $fecha){						

			$respuesta = ModeloClientes::mdlEditarCliente($tabla, $nombre_cliente_editado, $id_cliente, $id_admin, $fecha);		  
			return $respuesta;
			
		}
	
	static public function ctrStatusCliente($tabla, $status_cliente, $id_cliente, $fecha, $id_admin){						

			$respuesta = ModeloClientes::mdlStatusCliente($tabla, $status_cliente, $id_cliente, $fecha, $id_admin);		  
			return $respuesta;
			
		}

	static public function ctrEliminarCliente($tabla, $id_cliente){						

			$respuesta = ModeloClientes::mdlEliminarCliente($tabla, $id_cliente);		  
			return $respuesta;
			
		}

  }
