<?php

	class ControladorUsuarios{

	  	static public function ctrIngresoUsuario($correo,$pass){

			$ruta_global = "https://clientes.ink/";

	  		if(!empty($correo)){

	  			if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $correo)){

	  				$respuesta = ModeloUsuarios::mdlConsultaUsuario('v_usuarios_b7c50848','correo_usuario' ,$correo);

	  				if( $respuesta['correo_usuario'] == $correo && password_verify($pass, $respuesta['password_usuario']) ){

						if($respuesta['activo_usuario'] == 1){

							if($respuesta['tipo_usuario'] == 1){

								if(isset($_SESSION)){
								}else{
									session_start();
								}

								$_SESSION['sesion_activa'] = 1;
								$_SESSION['tipo_usuario'] = $respuesta['tipo_usuario'];
								$_SESSION['id_usuario'] = $respuesta['id_usuario'];
								$_SESSION['correo'] = $respuesta['correo_usuario'];


								echo '<script type="text/javascript">
					 					location.href = "'.$ruta_global.'tickets";
									</script>';

							}else if($respuesta['tipo_usuario'] == 2){

								if($respuesta['activo_cliente'] == 1){

									if(isset($_SESSION)){
									}else{
										session_start();
									}

									$_SESSION['sesion_activa'] = 1;
									$_SESSION['tipo_usuario'] = $respuesta['tipo_usuario'];
									$_SESSION['id_usuario'] = $respuesta['id_usuario'];
									$_SESSION['correo'] = $respuesta['correo_usuario'];
									$_SESSION['id_cliente'] = $respuesta['id_cliente'];

									echo '<script type="text/javascript">
						 					location.href = "'.$ruta_global.'tickets";
										</script>';

								}else{

									echo '<p class="mensaje_error">Cliente inactivo.</p>';

								}

							}

						}else{

							echo '<p class="mensaje_error">Usuario inactivo.</p>';

						}

	  				}else{

	            		echo '<p class="mensaje_error">Usuario o contraseña incorrectos.</p>';

	  				}

	  			}else{

					echo '<p class="mensaje_error">Verificar datos.</p>';

				}

	  		}

	  	}

		static public function ctrConsultaUsuario($tabla,$campo,$valor){

			$respuesta = ModeloUsuarios::mdlConsultaUsuario($tabla,$campo,$valor);
			return $respuesta;

		}

		static public function ctrConsultaUsuarios($tabla,$campo,$valor){

			$respuesta = ModeloUsuarios::mdlConsultaUsuarios($tabla,$campo,$valor);
			return $respuesta;

		}

		static public function ctrConsultaClienteUsuarios($tabla, $tabla2, $sk_cliente){

			$respuesta = ModeloUsuarios::mdlConsultaClienteUsuarios($tabla, $tabla2, $sk_cliente);
			return $respuesta;

		}

		static public function ctrAltaUsuario($tabla, $id_usuario, $u_nombre, $apellidoP, $apellidoM, $fecha_nacimiento, $telefono, $telefono1, $telefono2, $correo, $contrasena, $activo, $clave, $fecha, $tipo_usuario, $hash, $usuario_alta){

			$respuesta = ModeloUsuarios::mdlAltaUsuario($tabla, $id_usuario, $u_nombre, $apellidoP, $apellidoM, $fecha_nacimiento, $telefono, $telefono1, $telefono2, $correo, $contrasena, $activo, $clave, $fecha, $tipo_usuario, $hash, $usuario_alta);
			return $respuesta;

		}

		static public function ctrRelacionUsuarioCliente($tabla, $sk_usuario_cliente, $id_usuario, $id_cliente, $clave_relacion, $hash_relacion){

			$respuesta = ModeloUsuarios::mdlRelacionUsuarioCliente($tabla, $sk_usuario_cliente, $id_usuario, $id_cliente, $clave_relacion, $hash_relacion);
			return $respuesta;

		}

		static public function ctrActualizarRequeridosUsuario($tabla, $u_nombre, $apellidoP, $apellidoM, $telefono, $correo, $sk_usuario, $fecha_nacimiento, $telefono2, $telefono3, $fecha, $usuario_actualizacion){

			$respuesta = ModeloUsuarios::mdlActualizarRequeridosUsuario($tabla, $u_nombre, $apellidoP, $apellidoM, $telefono, $correo, $sk_usuario, $fecha_nacimiento, $telefono2, $telefono3, $fecha, $usuario_actualizacion);
			return $respuesta;

		}

		static public function ctrActualizarDatosContrasenaUsuario($tabla, $u_nombre, $apellidoP, $apellidoM, $telefono, $correo, $sk_usuario, $fecha_nacimiento, $telefono2, $telefono3, $password, $fecha, $usuario_actualizacion){

			$respuesta = ModeloUsuarios::mdlActualizarDatosContrasenaUsuario($tabla, $u_nombre, $apellidoP, $apellidoM, $telefono, $correo, $sk_usuario, $fecha_nacimiento, $telefono2, $telefono3, $password, $fecha, $usuario_actualizacion);
			return $respuesta;

		}

		static public function ctrStatusUsuario($tabla, $status_usuario, $id_usuario, $fecha, $id_admin){

			$respuesta = ModeloUsuarios::mdlStatusUsuario($tabla, $status_usuario, $id_usuario, $fecha, $id_admin);
			return $respuesta;

		}

		static public function ctrEliminarUsuario($tabla, $id_usuario){

			$respuesta = ModeloUsuarios::mdlEliminarUsuario($tabla, $id_usuario);
			return $respuesta;

		}

		static public function ctrEliminarRelacion($tabla, $id_usuario){

			$respuesta = ModeloUsuarios::mdlEliminarRelacion($tabla, $id_usuario);
			return $respuesta;

		}


  	}
