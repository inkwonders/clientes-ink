<?php

	class ControladorTickets{   		

	  	static public function ctrAltaTicket($tabla, $datos){						

			$respuesta = ModeloTickets::mdlAltaTicket($tabla, $datos);		  
			return $respuesta;		  
			
		}

		static public function ctrAltaArchivosTicket($tabla, $datos){						

			$respuesta = ModeloTickets::mdlAltaArchivosTicket($tabla, $datos);		  
			return $respuesta;		  
			
		}

		static public function ctrAltaEntradaTicket($tabla, $datos){						

			$respuesta = ModeloTickets::mdlAltaEntradaTicket($tabla, $datos);		  
			return $respuesta;		  
			
		}

		static public function ctrAltaArchivosEntrada($tabla, $datos){						

			$respuesta = ModeloTickets::mdlAltaArchivosEntrada($tabla, $datos);		  
			return $respuesta;		  
			
		}

		static public function ctrConsulta($tabla, $condicion){						

			$respuesta = ModeloTickets::mdlConsulta($tabla, $condicion);		  
			return $respuesta;		  
			
		}

		static public function ctrUUID(){						

			$respuesta = ModeloTickets::mdlUUID();		  
			return $respuesta;		  
			
		}

		static public function ctrConsultaMotivo($tabla,$campo,$valor){						

			$respuesta = ModeloTickets::mdlConsultaMotivo($tabla,$campo,$valor);		  
			return $respuesta;
			
		}

		static public function ctrConsultaTotalTickets(){						

			$respuesta = ModeloTickets::mdlConsultaTotalTickets();		  
			return $respuesta;		  
			
		}

		static public function ctrBorrarTodo($tabla, $campo, $valor){						

			$respuesta = ModeloTickets::mdlBorrarTodo($tabla, $campo, $valor);		  
			return $respuesta;		  
			
		}

		static public function ctrConsultaEntrada($tabla, $campo, $valor, $extra){						

			$respuesta = ModeloTickets::mdlConsultaEntrada($tabla, $campo, $valor, $extra);		  
			return $respuesta;		  
			
		}

		static public function ctrConsultaCantidadTickets(){						

			$respuesta = ModeloTickets::mdlConsultaCantidadTickets();		  
			return $respuesta;		  
			
		}

		static public function ctrConsultaTicketExistente($no_ticket){						

			$respuesta = ModeloTickets::mdlConsultaTicketExistente($no_ticket);		  
			return $respuesta;		  
			
		}

		static public function ctrConsultaArchivos($tabla, $campo, $valor, $extra){

			$respuesta = ModeloTickets::mdlConsultaArchivos($tabla, $campo, $valor, $extra);		  
			return $respuesta;
			
		}

		static public function ctrActualiza($tabla, $query){

			$respuesta = ModeloTickets::mdlActualiza($tabla,$query);
			return $respuesta;
			
		}

  	}