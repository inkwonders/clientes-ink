<?php

	class ControladorProyectos{   		


		static public function ctrMostrarProyectos($tabla, $condicion){						

			$respuesta = ModeloProyectos::mdlMostrarProyectos($tabla, $condicion);		  
			return $respuesta;	  
			
		}

	  	static public function ctrAltaProyecto($tabla, $datos){						

			$respuesta = ModeloProyectos::mdlAltaProyecto($tabla, $datos);		  
			return $respuesta;	  
			
		}

		static public function ctrAltaArchivosProyecto($tabla, $datos){						

			$respuesta = ModeloProyectos::mdlAltaArchivosProyecto($tabla, $datos);		  
			return $respuesta;		  
			
		}

		static public function ctrConsultaArchivos($tabla, $campo, $valor, $extra){

			$respuesta = ModeloProyectos::mdlConsultaArchivos($tabla, $campo, $valor, $extra);		  
			return $respuesta;
			
		}

		static public function ctrConsultaProyecto($tabla, $campo1, $campo2, $valor1, $valor2){

			$respuesta = ModeloProyectos::mdlConsultaProyecto($tabla, $campo1, $campo2, $valor1, $valor2);		  
			return $respuesta;
			
		}

		

  	}