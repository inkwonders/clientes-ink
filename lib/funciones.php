<?php

function validarArchivo($nombre, $tam)
{    
    $array_extensiones = array(
        "GIF",  "jpeg",  "JPG",  "SVG",  "pps",  "gif",  "png",  "ppsx",  "one",  "svg",  "psd",  "jpg",
        "AI",  "AIT",  "ai",  "ait",  "mov",  "docx",  "xlsx",  "mp4",  "pptx",  "doc",  "txt",  "xls",  "ppt",
        "pdf",  "PNG",  "JPEG"
    );

    $nombre_array = explode(".", $nombre);
    $extension = end($nombre_array);

    if (!in_array($extension, $array_extensiones)) {
        return false;
    } else if ($tam > ((1024 * 1024) * 5)) {
        return false;
    } else {
        return true;
    }
}

function genera_clave_unica($length = 10)
{
    $code = md5(uniqid(rand(), true));
    if ($length != "") return substr($code, 0, $length);
    else return $code;
}

function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object))
                    rrmdir($dir . DIRECTORY_SEPARATOR . $object);
                else
                    unlink($dir . DIRECTORY_SEPARATOR . $object);
            }
        }
        rmdir($dir);
    }
}


function mostrar_fecha_formato($fecha_nacimiento_inscripcion)
{
    $año = substr("$fecha_nacimiento_inscripcion", 0, 4);
    $mes = substr("$fecha_nacimiento_inscripcion", 5, 2);
    $dia = substr("$fecha_nacimiento_inscripcion", 8, 2);
    $mes_aux = regresa_mes($mes);
    $formato_fecha = $dia . " / " . $mes_aux . " / " . $año;
    return $formato_fecha;
}

function regresa_mes($var)
{
    switch ($var) {
        case '01':
            $mes = 'Enero';
            break;
        case '02':
            $mes = 'Febrero';
            break;
        case '03':
            $mes = 'Marzo';
            break;
        case '04':
            $mes = 'Abril';
            break;
        case '05':
            $mes = 'Mayo';
            break;
        case '06':
            $mes = 'Junio';
            break;
        case '07':
            $mes = 'Julio';
            break;
        case '08':
            $mes = 'Agosto';
            break;
        case '09':
            $mes = 'Septiembre';
            break;
        case '10':
            $mes = 'Octubre';
            break;
        case '11':
            $mes = 'Noviembre';
            break;
        case '12':
            $mes = 'Diciembre';
            break;
        default:
            // code...
            break;
    }
    return $mes;
}

function datos_estatus($estatus){
    $txt = "";
    switch ($estatus) {
        case 1:
            $txt = 'Nuevo';
            $clase = 'tipo_nuevo';
            break;
        case 2:
            $txt = 'Respuesta Admin';
            $clase = 'tipo_respuesta';
            break;
        case 3:
            $txt = 'Respuesta Cliente';
            $clase = 'tipo_respuesta';
            break;
        case 4:
            $txt = 'Cerrado';
            $clase = 'tipo_cerrado';
            break;                        
    }

    $arreglo = array(
        "texto" => $txt,
        "clase" => $clase
    );

    return $arreglo;
    
}