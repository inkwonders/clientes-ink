<html>

    <head>

      <meta charset='utf-8'>

      <title></title>

      <style>

      * {

        box-sizing: border-box;

      }

      body {

        padding: 0;

        margin: 0;

      }

      .padre {

        width: 100%;

        font-family: arial;

        background-color: #fff;

        display: flex;

        align-items: center;

        justify-content: center;

        margin: 0;

      }

      .contenedor {

        width: 600px;

        background-color: #efefef;

        margin-top: 50px;

        padding: 17px;

      }

      .mensaje {

        width: 100%;

        background-color: #fff;

        display: inline-block;

        margin: 0;

        padding: 14px;

    

      }

      .linea_h {

        width: 100%;

        height: 2px;

        background-color: #ebebeb;

      }

      .mensaje_sub_1 {

        width: 100%;

        font-size: 14px;

        padding: 7px 0 0 7px;

        color: #808080;

      }

      .mensaje_sub_2 {

        width: 100%;

        padding: 22px 14px 5px 14px;

        font-size: 13px;

        color: #999999;

        display: table;

      }

      .cont_boton {

        width: 100%;

        display: inline-block;

        text-align: center;

        align-items: center;

        justify-content: center;

        padding: 25px 0 14px 0;

      }

      .boton_pagar {

        background-color: #000;

        color: #fff;

        /* width: 70px; */

        height: 40px;

        padding: 2%;

        display: inline-block;

        text-align: center;

        font-size: 15px;

        cursor: pointer;

        margin: 0 auto !important;

        position: relative;

      }

      .cont_image {

        width: 100%;

        display: inline-block;

        padding-top: 25px;

        text-align: center;

      }

      .imagen {

        width: auto;    

        max-height: 200px;

      }

      .cont_txt {

        padding: 30px 0 16px 0;

        color: #808080;

      }

      .cont_image_logo {

        width: 100%;

        display: inline-block;

        padding: 20px 0 34px 0;

        text-align: center;

      }

      .logo_escuela {

        max-height: 80px;

      }

      .titulo{

        font-size: 18px;

        text-transform: uppercase;

      }      

      .footer1{

        padding: 10px 0 0 0;

        margin: 0 0 0 auto;

        display: table-cell;

        /* word-break: break-all; */

        width: 40%;

        line-height: 1.5;

      }

      .footer2{

        padding: 10px 0 0 0;

        line-height: 1.5;

        margin: 0 auto 0 0;

        display: table-cell;

        text-align: right;

        word-break: break-all;

        width: 40%;

      }

      </style>

    </head>

    <body>

      <div class='padre'>

        <div class='contenedor'>

          <div class='mensaje'>

            <div class='cont_image_logo'>

              <img class='logo_escuela' src='http://192.168.1.96:80/tickets/mvc/vistas/assets/img/logo2.png'>

            </div>

            <div class='linea_h'></div>

            <div class='cont_image'>

              <img class='imagen' src='http://192.168.1.96:80/tickets/mvc/vistas/assets/img/receipt.png'>

              <div class='cont_txt'>

                <span class='titulo'>Nueva Entrada</span>

              </div>

            </div>

            <div class='linea_h'></div>

            <br>

            <div class='mensaje_sub_1'>

              Se ha generado una nueva entrada con la siguiente información: <br />

              <ul>                

                <li>Folio Ticket: <b>15</b></li>

                <li>Fecha: <b>17 / Febrero / 2021</b> Hora: <b>12:09 hrs</b></li>

              </ul>          

            </div>

            <div class='cont_boton'>

            <a href='http://192.168.1.96:80/tickets/mvc/ticket/0910e199c7'>

              <div class='boton_pagar'>

                <span>Ver Ticket</span>

              </div>

            </a>

            </div>

          </div>

          <div class='mensaje_sub_2'>

            <div class='footer1'>

              Ink Wonders<br />

              Cerro del Tesoro #141, Col. Colinas del Cimatario, Quéretaro, Querétaro..<br />

              <a style='text-decoration:none; color:#0174e0;' href='mailto:josue@inkwonders.com'>josue@inkwonders.com</a><br/>

              <a style='text-decoration:none; color:#0174e0;' href='tel:4421234567'>4421234567</a><br/>

            </div>

          </div>

        </div>

      </div>

    </body>

    </html>
