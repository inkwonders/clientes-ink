-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 15, 2022 at 10:21 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ybdyihmy_db_ink_59d43d93`
--

-- --------------------------------------------------------

--
-- Table structure for table `clientes_30f39860`
--

CREATE TABLE `clientes_30f39860` (
  `sk_cliente` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `activo` int(11) NOT NULL COMMENT '1: activo 0: inactivo',
  `fecha_alta` datetime NOT NULL,
  `usuario_alta` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  `clave_cliente` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `hash_acciones` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `clientes_30f39860`
--

INSERT INTO `clientes_30f39860` (`sk_cliente`, `nombre`, `activo`, `fecha_alta`, `usuario_alta`, `fecha_actualizacion`, `usuario_actualizacion`, `clave_cliente`, `hash_acciones`) VALUES
('0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 'INK WONDERS PRUEBA', 1, '2021-02-04 17:59:42', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-16 23:40:05', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '0242ac1300', 'NmQ1MjlmMzEtNjdjNi0xMWViLWEyZDQtMDRkNGM0YWZlZTdj'),
('2f547e55-7782-11eb-ab88-5254009623cd', 'URBAN HANGERS', 1, '2021-02-25 08:57:38', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, '9dbf5946df', 'MmY1NDdlNTUtNzc4Mi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('30514bc8-7788-11eb-ab88-5254009623cd', 'Cliente Ink Prueba editado', 0, '2021-02-25 09:40:37', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:38', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '7ce0a813c1', 'MzA1MTRiYzgtNzc4OC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('4d9daa0f-76d6-11eb-ab88-5254009623cd', 'Móvil test', 0, '2021-02-24 12:27:16', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:42', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '251860c0ac', 'NGQ5ZGFhMGYtNzZkNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('4da1694b-76d6-11eb-ab88-5254009623cd', 'Móvil test', 0, '2021-02-24 12:27:16', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:45', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'bd27499d56', 'NGRhMTY5NGItNzZkNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('4da57cbc-76d6-11eb-ab88-5254009623cd', ' test', 0, '2021-02-24 12:27:16', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:47', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '7f2f94c866', 'NGRhNTdjYmMtNzZkNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('4dad17ca-76d6-11eb-ab88-5254009623cd', 'Uno nuevo', 0, '2021-02-24 12:27:16', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:48', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'cdcc9768e2', 'NGRhZDE3Y2EtNzZkNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('4db463d1-76d6-11eb-ab88-5254009623cd', ' test', 0, '2021-02-24 12:27:16', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:50', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'd62c8e8c10', 'NGRiNDYzZDEtNzZkNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('5a75acbb-76d6-11eb-ab88-5254009623cd', 'Nuevo', 0, '2021-02-24 12:27:37', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:52', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '6d4b472aae', 'NWE3NWFjYmItNzZkNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('619f7f36-76d2-11eb-ab88-5254009623cd', 'Probandoluz', 0, '2021-02-24 11:59:11', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:54', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '28924d672b', 'NjE5ZjdmMzYtNzZkMi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('70ab9305-76b7-11eb-ab88-5254009623cd', 'Nuevoclientecontodosloscaracteresjuntoss', 0, '2021-02-24 08:46:20', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:56', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ffb6635985', 'NzBhYjkzMDUtNzZiNy0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('72b9ba40-720d-11eb-ab88-5254009623cd', 'SONDERSONDER SONDERSONDERSONDERSONDERSO.', 1, '2021-02-18 10:19:24', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-23 16:45:58', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'f88fb88967', 'NzJiOWJhNDAtNzIwZC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('90784123-6fb5-11eb-adea-04d4c4afee7c', 'Cliente de prueba 3', 1, '2021-02-15 18:45:21', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-16 18:09:15', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '6c1f983a38', 'OTA3ODQxMjMtNmZiNS0xMWViLWFkZWEtMDRkNGM0YWZlZTdj'),
('b5ba5915-6fdb-11eb-adea-04d4c4afee7c', 'clienteprueba 2', 1, '2021-02-15 23:18:24', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'a31f30d8bb', 'YjViYTU5MTUtNmZkYi0xMWViLWFkZWEtMDRkNGM0YWZlZTdj'),
('c9e9efe5-76cf-11eb-ab88-5254009623cd', 'Look Sport', 1, '2021-02-24 11:40:38', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'd883c5d7a1', 'YzllOWVmZTUtNzZjZi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('d14d34f7-7138-11eb-94a9-04d4c4afee7c', 'Urban Hangers', 1, '2021-02-17 16:57:20', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'dfa593b633', 'ZDE0ZDM0ZjctNzEzOC0xMWViLTk0YTktMDRkNGM0YWZlZTdj'),
('e4fc2785-7079-11eb-8037-04d4c4afee7c', 'nuevo cliente', 1, '2021-02-16 18:10:40', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-18 10:35:06', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'be09825adc', 'ZTRmYzI3ODUtNzA3OS0xMWViLTgwMzctMDRkNGM0YWZlZTdj');

-- --------------------------------------------------------

--
-- Table structure for table `datos_ae1a8cc2`
--

CREATE TABLE `datos_ae1a8cc2` (
  `sk_datos` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `correo_secundario` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_secundario` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `datos_ae1a8cc2`
--

INSERT INTO `datos_ae1a8cc2` (`sk_datos`, `nombre`, `direccion`, `correo`, `correo_secundario`, `telefono`, `telefono_secundario`) VALUES
('4b69df73-dbf7-4e2b-8e83-17eb11799823', 'INK WONDERS', 'Cerro del Tesoro #141, Col. Colinas del Cimatario, Quéretaro, Querétaro.', 'eddy@inkwonders.com', NULL, '4421234567', 0);

-- --------------------------------------------------------

--
-- Table structure for table `motivos_bb9aa773`
--

CREATE TABLE `motivos_bb9aa773` (
  `sk_motivo` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `orden` int(11) NOT NULL,
  `clave_motivo` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `hash_acciones` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `motivos_bb9aa773`
--

INSERT INTO `motivos_bb9aa773` (`sk_motivo`, `nombre`, `orden`, `clave_motivo`, `hash_acciones`) VALUES
('a9c5a75b-72d4-11eb-9f9c-04d4c4afee7c', 'Cambios en funcionamiento', 5, '6bebe94957', 'ZDg4NWEyZjEtYTZlNC00YWU3LTk3YTYtODBlYzBjNzAwOWJh'),
('a9c5ae49-72d4-11eb-9f9c-04d4c4afee7c', 'Cambios en textos', 6, 'a8e3f7b426', 'YjNkZTk4MTYtNDdiMS00YmM4LWJlNGMtNTM3MWM5M2NmNmE0'),
('a9c5b312-72d4-11eb-9f9c-04d4c4afee7c', 'Estatus del proyecto', 7, '3e75aced7f', 'MzlhMWZiNjYtY2JiYy00ZjdlLWJhODktM2VkN2Y5YTg2Y2Uy'),
('a9c5b78f-72d4-11eb-9f9c-04d4c4afee7c', 'Facturación y pagos', 8, 'f4ea891bfc', 'ODM0NDA5ZGQtNWVhMC00MDBkLTk3OWUtN2I0NWI3MWU2ZDVh'),
('a9c5bd69-72d4-11eb-9f9c-04d4c4afee7c', 'Otro', 9, 'b634ac3d8a', 'ZDIyNjg0ZGUtNjZhYi00YzQ1LThlMWQtNGZiYzVjOThjZGM14e2a849536'),
('b3f3ca52-6bbf-11eb-9393-04d4c4afee7c', 'Creación de correo electrónico', 1, '2eef660ca9', 'NzQwZjI1YWUtNmJiZi0xMWViLTkzOTMtMDRkNGM0YWZlZTdj'),
('b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'Problemas con mi correo electrónico', 2, '47385bbcdf', 'OTE0NGE2NzktNmJiZi0xMWViLTkzOTMtMDRkNGM0YWZlZTdj'),
('b3f3d784-6bbf-11eb-9393-04d4c4afee7c', 'Página web caída', 3, '0eeddac1f9', 'OTVjMGM4M2MtNmJiZi0xMWViLTkzOTMtMDRkNGM0YWZlZTdj'),
('b3f3dd67-6bbf-11eb-9393-04d4c4afee7c', 'Cambios en diseño', 4, '7ecf38457f', 'OTc5YWUyNzItNmJiZi0xMWViLTkzOTMtMDRkNGM0YWZlZTdj');

-- --------------------------------------------------------

--
-- Table structure for table `proyectos_archivos_49bc6db7`
--

CREATE TABLE `proyectos_archivos_49bc6db7` (
  `sk_archivo` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `clave_archivo` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fk_proyecto` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `archivo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_archivo_original` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `orden` int(1) NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `hash_acciones` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `proyectos_archivos_49bc6db7`
--

INSERT INTO `proyectos_archivos_49bc6db7` (`sk_archivo`, `clave_archivo`, `fk_proyecto`, `archivo`, `nombre_archivo_original`, `orden`, `fecha_alta`, `hash_acciones`) VALUES
('244b417b-7861-11eb-ab88-5254009623cd', '79de864101', '23e082cd-7861-11eb-ab88-5254009623cd', 'proyecto_60393f021cb36_5783e9e01c_2021-02-26_12-33-38.pdf', 'edelweiss.pdf', 1, '2021-02-26 12:33:37', 'MjQ0ZGIzZDktNzg2MS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('24557a42-7861-11eb-ab88-5254009623cd', '3697208414', '23e082cd-7861-11eb-ab88-5254009623cd', 'proyecto_60393f022d8ce_5783e9e01c_2021-02-26_12-33-38.pdf', 'ClinicaEspalda_Web.pdf', 2, '2021-02-26 12:33:37', 'MjQ1N2M4MWQtNzg2MS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('2469edb5-76b4-11eb-ab88-5254009623cd', 'b78774817e', '24523617-76b4-11eb-ab88-5254009623cd', 'proyecto_60366f4436180_8d3dfe89c0_2021-02-24_09-22-44.jpg', 'img.jpg', 1, '2021-02-24 09:22:44', 'MjQ2YmVkOTYtNzZiNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('24806925-76b4-11eb-ab88-5254009623cd', '774b7ef054', '24523617-76b4-11eb-ab88-5254009623cd', 'proyecto_60366f4457da9_8d3dfe89c0_2021-02-24_09-22-44.xlsx', 'usuarios_uh_bd_old (1).xlsx', 2, '2021-02-24 09:22:44', 'MjQ4MTkxOGMtNzZiNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('6b081bfe-7861-11eb-ab88-5254009623cd', 'a9923b0c44', '6ad63f71-7861-11eb-ab88-5254009623cd', 'proyecto_60393f78c46c1_4212e5c2ac_2021-02-26_12-35-36.pdf', 'edelweiss.pdf', 1, '2021-02-26 12:35:36', 'NmIwYmE5NWUtNzg2MS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('6b30f6d8-7861-11eb-ab88-5254009623cd', '874d9364b3', '6ad63f71-7861-11eb-ab88-5254009623cd', 'proyecto_60393f79a0a6b_4212e5c2ac_2021-02-26_12-35-37.pdf', 'ClinicaEspalda_Web.pdf', 2, '2021-02-26 12:35:36', 'NmI4ZTNhZDQtNzg2MS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('8b3873b5-7861-11eb-ab88-5254009623cd', '23de0726bc', '8b33ec4d-7861-11eb-ab88-5254009623cd', 'proyecto_60393faec03fb_8508558a53_2021-02-26_12-36-30.pdf', 'edelweiss.pdf', 1, '2021-02-26 12:36:30', 'OGIzOTI0NDctNzg2MS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('c1ef7913-76b6-11eb-ab88-5254009623cd', '60bda56181', 'c1ea9d09-76b6-11eb-ab88-5254009623cd', 'proyecto_603673a774f4a_3e47d1d405_2021-02-24_09-41-27.svg', 'icon-hanger.svg', 1, '2021-02-24 09:41:27', 'YzFmMDAyNGUtNzZiNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('c1f4df4e-76b6-11eb-ab88-5254009623cd', 'a5bd472dab', 'c1ea9d09-76b6-11eb-ab88-5254009623cd', 'proyecto_603673a77e48e_3e47d1d405_2021-02-24_09-41-27.svg', 'icon-usuario-1.svg', 2, '2021-02-24 09:41:27', 'YzFmNWM1MzgtNzZiNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('c1f7ea5a-76b6-11eb-ab88-5254009623cd', 'c5c42b8a12', 'c1ea9d09-76b6-11eb-ab88-5254009623cd', 'proyecto_603673a78241a_3e47d1d405_2021-02-24_09-41-27.svg', 'icon-usuario-5.svg', 3, '2021-02-24 09:41:27', 'YzFmODRlOWItNzZiNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('c1fa99ae-76b6-11eb-ab88-5254009623cd', '4e0d2f7cc9', 'c1ea9d09-76b6-11eb-ab88-5254009623cd', 'proyecto_603673a7868fd_3e47d1d405_2021-02-24_09-41-27.svg', 'icon-votante.svg', 4, '2021-02-24 09:41:27', 'YzFmYjAwMTQtNzZiNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('f54dfa6b-76d0-11eb-ab88-5254009623cd', '4f0bf052ee', 'f545ec39-76d0-11eb-ab88-5254009623cd', 'proyecto_60369f9c8ccde_4781c6ace6_2021-02-24_12-49-00.pdf', 'Crono Look Sport-17 Feb.pdf', 1, '2021-02-24 12:49:00', 'ZjU0ZTZmYTctNzZkMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('f55036cb-76d0-11eb-ab88-5254009623cd', '2a436b8bf3', 'f545ec39-76d0-11eb-ab88-5254009623cd', 'proyecto_60369f9c90b29_4781c6ace6_2021-02-24_12-49-00.pdf', 'Crono Look Sport-Inicial.pdf', 2, '2021-02-24 12:49:00', 'ZjU1MGQ2NDgtNzZkMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('f552e9d5-76d0-11eb-ab88-5254009623cd', '12fbe72d58', 'f545ec39-76d0-11eb-ab88-5254009623cd', 'proyecto_60369f9c95abd_4781c6ace6_2021-02-24_12-49-00.pdf', 'Kickoff.pdf', 3, '2021-02-24 12:49:00', 'ZjU1M2VmMTQtNzZkMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk');

-- --------------------------------------------------------

--
-- Table structure for table `proyectos_f60e66cc`
--

CREATE TABLE `proyectos_f60e66cc` (
  `sk_proyecto` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `clave_proyecto` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fk_cliente` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `estatus` int(11) NOT NULL,
  `titulo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `usuario_alta` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_finalizacion` datetime DEFAULT NULL,
  `usuario_finalizacion` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hash_acciones` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `proyectos_f60e66cc`
--

INSERT INTO `proyectos_f60e66cc` (`sk_proyecto`, `clave_proyecto`, `fk_cliente`, `estatus`, `titulo`, `descripcion`, `fecha_alta`, `usuario_alta`, `fecha_finalizacion`, `usuario_finalizacion`, `hash_acciones`) VALUES
('23e082cd-7861-11eb-ab88-5254009623cd', '5783e9e01c', '2f547e55-7782-11eb-ab88-5254009623cd', 1, 'josue 3', '<p>asdasd</p>', '2021-02-26 12:33:37', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'MjQ0Mjc2ZDAtNzg2MS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('24523617-76b4-11eb-ab88-5254009623cd', '8d3dfe89c0', 'e4fc2785-7079-11eb-8037-04d4c4afee7c', 1, 'proyecto nuevo cliente', '<h1>Titulo</h1><p><br></p><p><span>Lorem ipsum es el texto que se usa habitualmente en diseño gráfico\r\n en demostraciones de tipografías o de borradores de diseño para probar \r\nel diseño visual antes de insertar el texto final.</span></p><p><br></p><p><span>Lorem ipsum es el texto que se usa habitualmente en diseño gráfico\r\n en demostraciones de tipografías o de borradores de diseño para probar \r\nel diseño visual antes de insertar el texto final.</span></p><p><br></p><ol><li>uno</li><li>dos</li><li>tres<span><br></span><span></span></li></ol><p><br></p><table class=\"table table-bordered\"><tbody><tr><td>uno<br></td><td>dos<br></td><td>tres<br></td></tr><tr><td>texto<br></td><td>texto</td><td>texto</td></tr></tbody></table><p><u>textotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotexto</u>.</p><p><br></p><p><b>textotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotextotexto</b></p>', '2021-02-24 09:22:44', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'MjQ1NWM5YTgtNzZiNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('37c518a8-765f-11eb-ab88-5254009623cd', '06679959d7', '72b9ba40-720d-11eb-ab88-5254009623cd', 1, 'Prueba', '<p>Algo</p>', '2021-02-23 23:14:49', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'MzdjNjUzNmMtNzY1Zi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('400088ea-7788-11eb-ab88-5254009623cd', '605343c6ef', '30514bc8-7788-11eb-ab88-5254009623cd', 1, 'Proyecto exitoso', '<p>hola!<br></p>', '2021-02-25 10:41:03', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'NDAwNTY2NGYtNzc4OC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('4c7ad43b-778f-11eb-ab88-5254009623cd', '3c753e53f0', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 1, ' lorem 2', '<p>prueba<br></p>', '2021-02-25 11:31:31', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'NGM3Yjg2NmQtNzc4Zi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('588fb79a-7860-11eb-ab88-5254009623cd', '680cb8c2b3', '30514bc8-7788-11eb-ab88-5254009623cd', 1, 'Josué', '<p>Subiendo 2 pdfs que me paso Eddy</p>', '2021-02-26 12:27:56', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'NTg5MDg1ZTYtNzg2MC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('5994699f-7860-11eb-ab88-5254009623cd', '1ca00968e3', '4dad17ca-76d6-11eb-ab88-5254009623cd', 1, 'Proyecto prueba luz carga de archivos', '<p>Hola, probando</p>', '2021-02-26 12:27:58', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'NTk5NTYwZWUtNzg2MC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('6ad63f71-7861-11eb-ab88-5254009623cd', '4212e5c2ac', '30514bc8-7788-11eb-ab88-5254009623cd', 1, 'Josue 4', '<p>uno ya al final</p>', '2021-02-26 12:35:36', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'NmFkZmE1MDMtNzg2MS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('838898ae-7854-11eb-ab88-5254009623cd', '7598e92913', '2f547e55-7782-11eb-ab88-5254009623cd', 1, 'Urban Community', '<p style=\"text-align: center; \"><b>Re diseño de comunidad Urban Hangers</b></p><p><br></p><p>Se desarrollará una nueva página web para que la comunidad de \"Hangers\" pueda crear su usuario, votar, comentar y subir sus diseños.</p><p><br></p><p>Además, se creará un panel administrativo para que se puedan inhabilitar usuarios o diseños, descargar mockups, crear y eliminar tendencias y otras funcionalidades.</p><p><br></p><p><b>Fecha de inicio:</b> 29 / Ene / 2021</p><p><b>Fecha de entrega:</b> 29 Mzo / 2021</p><p><br></p><pre><b>Documentos adjuntos:</b> Cotización, Contrato, Cronograma de pagos, Cronograma de entregas, Detalle de alcances del proyecto.</pre>', '2021-02-26 11:03:14', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'ODM4OWMwNTctNzg1NC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('83d90ff3-76b9-11eb-ab88-5254009623cd', 'f028a18505', 'e4fc2785-7079-11eb-8037-04d4c4afee7c', 1, 'lorem', '<p><span>Lorem ipsum es el texto que se usa habitualmente en diseño gráfico\r\n en demostraciones de tipografías o de borradores de diseño para probar \r\nel diseño visual antes de insertar el texto final.</span></p>', '2021-02-24 10:01:11', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'ODNkYTc4MjItNzZiOS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('8b33ec4d-7861-11eb-ab88-5254009623cd', '8508558a53', '30514bc8-7788-11eb-ab88-5254009623cd', 1, 'Josue 5', '<p>asdasda</p>', '2021-02-26 12:36:30', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'OGIzNGQxODYtNzg2MS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('ad60537b-785f-11eb-ab88-5254009623cd', '41b7680614', '2f547e55-7782-11eb-ab88-5254009623cd', 1, 'UH Community', '<p style=\"line-height: 25.200000762939453px; caret-color: rgb(33, 37, 41); color: rgb(33, 37, 41); text-align: center;\">Re diseño de comunidad Urban Hangers</p><p style=\"line-height: 25.200000762939453px; caret-color: rgb(33, 37, 41); color: rgb(33, 37, 41);\"><br></p><p style=\"line-height: 25.200000762939453px; caret-color: rgb(33, 37, 41); color: rgb(33, 37, 41);\">Se desarrollará una nueva página web para que la comunidad de \"Hangers\" pueda crear su usuario, votar, comentar y subir sus diseños.</p><p style=\"line-height: 25.200000762939453px; caret-color: rgb(33, 37, 41); color: rgb(33, 37, 41);\"><br></p><p style=\"line-height: 25.200000762939453px; caret-color: rgb(33, 37, 41); color: rgb(33, 37, 41);\">Además, se creará un panel administrativo para que se puedan inhabilitar usuarios o diseños, descargar mockups, crear y eliminar tendencias y otras funcionalidades.</p><p style=\"line-height: 25.200000762939453px; caret-color: rgb(33, 37, 41); color: rgb(33, 37, 41);\"><br></p><p style=\"line-height: 25.200000762939453px; caret-color: rgb(33, 37, 41); color: rgb(33, 37, 41);\">Fecha de inicio:&nbsp;29 / Ene / 2021</p><p style=\"line-height: 25.200000762939453px; caret-color: rgb(33, 37, 41); color: rgb(33, 37, 41);\">Fecha de entrega:&nbsp;29 Mzo / 2021</p><p style=\"line-height: 25.200000762939453px; caret-color: rgb(33, 37, 41); color: rgb(33, 37, 41);\"><br></p><pre style=\"font-size: 14px; caret-color: rgb(33, 37, 41);\">Documentos adjuntos: Cotización, Contrato, Cronograma de pagos, Cronograma de entregas, Detalle de alcances del proyecto.</pre>', '2021-02-26 12:23:09', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'YWQ2MTkyYmEtNzg1Zi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('c1ea9d09-76b6-11eb-ab88-5254009623cd', '3e47d1d405', 'b5ba5915-6fdb-11eb-adea-04d4c4afee7c', 1, 'Prueba con mucho texto', '<p>HolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHolaHola</p>', '2021-02-24 09:41:27', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'YzFlZGFlNGYtNzZiNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('e6334243-7860-11eb-ab88-5254009623cd', 'e8b4718807', '30514bc8-7788-11eb-ab88-5254009623cd', 1, 'josue 2', '<p>asd</p>', '2021-02-26 12:31:53', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'ZTY0NGEwNDktNzg2MC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('f545ec39-76d0-11eb-ab88-5254009623cd', '4781c6ace6', 'c9e9efe5-76cf-11eb-ab88-5254009623cd', 1, 'Desarrollo: Plataforma de administración - ETAPA 1', '<p style=\"text-align: center; \"><b>Desarrollo de un software en la nube con la identidad de Look Sport que incluya los siguientes módulos:</b></p><p><br></p><ol><li>Administración de Usuarios.</li><li>Alta de Productos.</li><li>Alta, Importación y Procesamiento de Órdenes de Compra (a través de archivos XML).</li><li>Almacén para recibir productos e ingresar inventarios.</li><li>Ventas - Homologación de los diferentes reportes de ventas realizados por cada plataforma o</li><li>Marketplace (a través de archivos XLSX).</li><li>Reportes.</li></ol><p><b><br></b></p><p><b>NOTAS:&nbsp;</b></p><p><br></p><ol><li>Adaptación del módulo de Almacén tanto para computadoras de escritorio (Desktop) como dispositivos móviles (smartphones). El resto de módulos serán únicamente desarrollados para Desktop.</li><li>Servicio de Soporte y Mantenimiento por 12 meses posteriores a la entrega del software.</li></ol>', '2021-02-24 12:49:00', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 'ZjU0N2JiYmMtNzZkMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk');

-- --------------------------------------------------------

--
-- Table structure for table `tickets_47aaef15`
--

CREATE TABLE `tickets_47aaef15` (
  `sk_ticket` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `clave_ticket` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fk_cliente` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `folio` int(11) NOT NULL,
  `estatus` int(1) NOT NULL,
  `titulo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fk_motivo` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'validar 500',
  `calificacion` int(11) DEFAULT NULL,
  `comentario_calificacion` text COLLATE utf8_spanish_ci COMMENT 'validar 500',
  `fecha_calificacion` datetime DEFAULT NULL,
  `fecha_alta` datetime NOT NULL,
  `usuario_alta` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hash_acciones` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `carpeta_ticket` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `tickets_47aaef15`
--

INSERT INTO `tickets_47aaef15` (`sk_ticket`, `clave_ticket`, `fk_cliente`, `folio`, `estatus`, `titulo`, `fk_motivo`, `descripcion`, `calificacion`, `comentario_calificacion`, `fecha_calificacion`, `fecha_alta`, `usuario_alta`, `fecha_actualizacion`, `usuario_actualizacion`, `hash_acciones`, `carpeta_ticket`) VALUES
('04894208-7177-11eb-ab88-5254009623cd', 'e727e084de', '90784123-6fb5-11eb-adea-04d4c4afee7c', 16, 4, 'No descarga zip urban hangers', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'No se puede descargar la carpeta con zips', NULL, NULL, NULL, '2021-02-17 17:22:35', '1690dcb1-70a8-11eb-8037-04d4c4afee7c', '2021-02-26 11:08:42', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MDQ4OWE5OTUtNzE3Ny0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_16_e727e084de'),
('0698c0db-70ac-11eb-8037-04d4c4afee7c', '3a1d10c036', '90784123-6fb5-11eb-adea-04d4c4afee7c', 10, 4, 'otra', 'b3f3d784-6bbf-11eb-9393-04d4c4afee7c', 'sdfsdfdfdsff', NULL, NULL, NULL, '2021-02-16 17:09:31', '1690dcb1-70a8-11eb-8037-04d4c4afee7c', '2021-02-26 11:09:39', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MDY5YjMyNTItNzBhYy0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'ticket_10_3a1d10c036'),
('0cccf142-706d-11eb-8037-04d4c4afee7c', '6861f4528a', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 5, 4, 'ticket alain', 'b3f3d784-6bbf-11eb-9393-04d4c4afee7c', 'prueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba de de ticketprueba', NULL, NULL, NULL, '2021-02-16 09:38:43', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-16 09:39:53', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'MGNjZjQ4ZDAtNzA2ZC0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'ticket_5_6861f4528a'),
('13c82757-730b-11eb-ab88-5254009623cd', 'a83790e7ab', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 26, 4, 'Probando Tickets Creando', 'a9c5bd69-72d4-11eb-9f9c-04d4c4afee7c', '<p>Hola como estan? ando probando todo lo de tickets a ver que tal funciona&nbsp;</p>', NULL, NULL, NULL, '2021-02-19 17:34:57', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-22 09:39:07', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MTNjODc5MTItNzMwYi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_26_a83790e7ab'),
('18e10e5f-76b8-11eb-ab88-5254009623cd', '2fe8056173', '70ab9305-76b7-11eb-ab88-5254009623cd', 29, 2, 'Probandoagregarunnuevoticketconelmáximode', 'b3f3d784-6bbf-11eb-9393-04d4c4afee7c', 'MáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáxim', NULL, NULL, NULL, '2021-02-24 09:51:02', 'aac4a702-76b7-11eb-ab88-5254009623cd', '2021-02-26 11:07:22', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MThlMjgzNTktNzZiOC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_29_2fe8056173'),
('1a837c88-76b9-11eb-ab88-5254009623cd', '5759474e8a', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 34, 2, 'ÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉ', 'b3f3ca52-6bbf-11eb-9393-04d4c4afee7c', '<p>ÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉÉ<br></p>', NULL, NULL, NULL, '2021-02-24 09:58:15', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-24 13:31:06', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MWE4M2ViNTctNzZiOS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_34_5759474e8a'),
('1b32678c-713f-11eb-94a9-04d4c4afee7c', '5f847062be', '90784123-6fb5-11eb-adea-04d4c4afee7c', 12, 4, 'hola de nuevo', 'b3f3d784-6bbf-11eb-9393-04d4c4afee7c', 'hola :) ', 2, 'no me gusto', '2021-02-17 10:44:37', '2021-02-17 10:42:21', '1690dcb1-70a8-11eb-8037-04d4c4afee7c', '2021-02-17 10:43:44', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MWIzNGM5NjctNzEzZi0xMWViLTk0YTktMDRkNGM0YWZlZTdj', 'ticket_12_5f847062be'),
('25c564ee-721e-11eb-ab88-5254009623cd', 'cbcff4c37a', '72b9ba40-720d-11eb-ab88-5254009623cd', 25, 4, 'testing', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'ssss', 1, 'Todo cool !', '2021-02-18 13:22:55', '2021-02-18 13:18:57', 'b527b3b5-720d-11eb-ab88-5254009623cd', '2021-02-18 13:21:45', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MjVjNWIyYWItNzIxZS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_25_cbcff4c37a'),
('2762ca53-7177-11eb-ab88-5254009623cd', '5dd7625e48', '90784123-6fb5-11eb-adea-04d4c4afee7c', 17, 2, 'hola', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'sdbhfbdf', NULL, NULL, NULL, '2021-02-17 17:23:34', '1690dcb1-70a8-11eb-8037-04d4c4afee7c', '2021-02-18 11:37:02', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'Mjc2MzVmOGYtNzE3Ny0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_17_5dd7625e48'),
('27978fd7-6cbb-11eb-b74c-04d4c4afee7c', '855513e815', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 1, 4, 'hola123123', 'b3f3dd67-6bbf-11eb-9393-04d4c4afee7c', 'asdasd', 2, 'no me gusto', '2021-02-16 17:40:19', '2021-02-11 16:47:44', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-12 00:00:00', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'Mjc5OWQ4ZGItNmNiYi0xMWViLWI3NGMtMDRkNGM0YWZlZTdj', 'ticket_1_855513e815'),
('28b61430-7139-11eb-94a9-04d4c4afee7c', '9f0833d440', 'd14d34f7-7138-11eb-94a9-04d4c4afee7c', 11, 4, 'Falla al subir archivos Falla al subir archivos...', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archivos Falla al subir archi', 1, 'todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo bien, gracias todo b', '2021-02-17 10:04:23', '2021-02-17 09:59:47', 'f01520ce-7138-11eb-94a9-04d4c4afee7c', '2021-02-17 10:01:08', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MjhiODZiOTgtNzEzOS0xMWViLTk0YTktMDRkNGM0YWZlZTdj', 'ticket_11_9f0833d440'),
('328d7948-7177-11eb-ab88-5254009623cd', '358d93f857', '90784123-6fb5-11eb-adea-04d4c4afee7c', 18, 2, 'hola 3', 'b3f3d784-6bbf-11eb-9393-04d4c4afee7c', 'dsgfgdfhfh', NULL, NULL, NULL, '2021-02-17 17:23:52', '1690dcb1-70a8-11eb-8037-04d4c4afee7c', '2021-02-17 17:27:39', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MzI4ZGEwMjYtNzE3Ny0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_18_358d93f857'),
('4320cb6b-76b8-11eb-ab88-5254009623cd', 'a4e092b455', '70ab9305-76b7-11eb-ab88-5254009623cd', 30, 2, 'Probandoagregarunnuevoticketconelmáximodecaracter', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'MáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracteresMáximodecaracMáximodecar', NULL, NULL, NULL, '2021-02-24 09:52:13', 'aac4a702-76b7-11eb-ab88-5254009623cd', '2021-02-26 11:07:11', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'NDMyMmYxMWQtNzZiOC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_30_a4e092b455'),
('49e12f12-76b8-11eb-ab88-5254009623cd', 'de6b75d7e7', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 31, 2, 'Titulocontodosloscaracteresposibles123123123123123', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', '<h3><b>«La curiosidad mató al gato».</b></h3><p>Cuántas veces habremos escuchado esta expresión y cuántas otras me habré cuestionado el por qué de ésta.</p><p>La curiosidad, sumada a la prudencia, es el motor de nuestro avance \ncomo sociedad y, reduciendo la muestra, de nuestros progresos \npersonales. La curiosidad marca la diferencia entre unos y otros: nace \ncomo instinto, viaja en la motivación y concluye en la satisfacción de \nun aprendizaje.&nbsp;Ella es la protagonista de la sociedad de la información\n donde, las tecnologías, nos permiten satisfacer ese deseo de \nconocimiento inmediato.</p><p>La curiosidad, en su justa medida, beneficia al que la tiene.</p><p>Por lo tanto, me pregunto hasta qué punto decidió, el gato, que el \nriesgo se sumase a su curiosidad para que, ésta, le acabara matando.</p><p>Me introduzco de este modo para anticipar una entrada en el blog en \nforma de agradecimiento por adelantado para aquellas personas que, \nmañana, encuentren el texto de “la subasta de mi vida” en el metro y, en\n primer lugar, tengan ese «instinto» de averiguar de qué va ese papel. \nEn segundo lugar, para quien dedique unos minutos a leérselo y, en \ntercer lugar, para quien llegue hasta estas líneas puesto que, como \nmínimo, ya serás más curioso que la mayoría y, yo, siempre lo he \nconsiderado una importante ventaja.</p><p>\n\n\n\n\n\n</p><p>Sin ningún tipo de pretensión oculta en forma de estrategia, sin \nningún objetivo de difusión de propaganda o publicidad, sin ningún tipo \nde fin económicoasdasdasdasdasdasdasdasdasdasdasdasdsdasdasdasdas</p>', NULL, NULL, NULL, '2021-02-24 09:52:25', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-26 11:07:01', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'NDllMWIwN2UtNzZiOC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_31_de6b75d7e7'),
('636a36a3-730b-11eb-ab88-5254009623cd', 'aff66a6994', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 27, 4, 'Prueba ticket 2', 'a9c5bd69-72d4-11eb-9f9c-04d4c4afee7c', '<p>hola, ando probando de nuevo lo de los tickets porque no cambie algo&nbsp;</p>', NULL, NULL, NULL, '2021-02-19 17:37:11', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-26 11:07:33', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'NjM2YWI1YzgtNzMwYi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_27_aff66a6994'),
('6691df09-76b8-11eb-ab88-5254009623cd', '4f62a592e7', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 32, 4, '12345678901234567890123456789012345678901234567890', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', '<p>asdasd</p>', NULL, NULL, NULL, '2021-02-24 09:53:13', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-25 09:42:16', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'NjY5MjE0MjEtNzZiOC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_32_4f62a592e7'),
('6799cb6d-09d3-11ec-a3a6-5254009623cd', '4935c9f742', 'e4fc2785-7079-11eb-8037-04d4c4afee7c', 38, 4, 'prueba', 'b3f3d784-6bbf-11eb-9393-04d4c4afee7c', '<p>pruebade<br></p>', NULL, NULL, NULL, '2021-08-30 15:46:52', 'addb5c01-76b3-11eb-ab88-5254009623cd', '2021-08-31 11:02:45', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'Njc5OWVhODEtMDlkMy0xMWVjLWEzYTYtNTI1NDAwOTYyM2Nk', 'ticket_38_4935c9f742'),
('787c11ad-70a7-11eb-8037-04d4c4afee7c', '9515f9a533', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 7, 4, 'Hola! ', 'b3f3d784-6bbf-11eb-9393-04d4c4afee7c', 'Estoy desde mi celular', NULL, NULL, NULL, '2021-02-16 16:36:54', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-16 16:39:40', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'Nzg3ZTdlOGMtNzBhNy0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'ticket_7_9515f9a533'),
('8db70adf-706c-11eb-8037-04d4c4afee7c', 'ad60e66c92', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 3, 4, 'Ticket nuevo con todos los caracteres que se permi', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'Este ticket es para probar toda la info que se muestre bien, que no tenga errores o asi... Este ticket es para probar toda la info que se muestre bien, que no tenga errores o asi... Este ticket es para probar toda la info que se muestre bien, que no tenga errores o asi... Este ticket es para probar toda la info que se muestre bien, que no tenga errores o asi... Este ticket es para probar toda la info que se muestre bien, que no tenga errores o asi... Este ticket es para probar toda la info que s', 1, 'todo muy bien', '2021-02-16 17:06:11', '2021-02-16 09:35:10', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-16 16:59:30', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'OGRiYTgxY2ItNzA2Yy0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'ticket_3_ad60e66c92'),
('8f030877-76b5-11eb-ab88-5254009623cd', '67dd391c36', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 28, 2, 'Ticket Josue 24/02/21 Utilizando todo el texto 123', 'b3f3dd67-6bbf-11eb-9393-04d4c4afee7c', '<h3><b>«La curiosidad mató al gato».</b></h3><p>Cuántas veces habremos escuchado esta expresión y cuántas otras me habré cuestionado el por qué de ésta.</p><p>La curiosidad, sumada a la prudencia, es el motor de nuestro avance \ncomo sociedad y, reduciendo la muestra, de nuestros progresos \npersonales. La curiosidad marca la diferencia entre unos y otros: nace \ncomo instinto, viaja en la motivación y concluye en la satisfacción de \nun aprendizaje.&nbsp;Ella es la protagonista de la sociedad de la información\n donde, las tecnologías, nos permiten satisfacer ese deseo de \nconocimiento inmediato.</p><p>La curiosidad, en su justa medida, beneficia al que la tiene.</p><p>Por lo tanto, me pregunto ', NULL, NULL, NULL, '2021-02-24 09:32:52', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-24 11:36:04', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'OGYwNWI5NjQtNzZiNS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_28_67dd391c36'),
('90cfe8fb-76b8-11eb-ab88-5254009623cd', 'a64ba7a565', '70ab9305-76b7-11eb-ab88-5254009623cd', 33, 2, 'Probandoagregarunnuevoticketconelmaximodecaractere', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', '<p><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketconelmáximodecaracter</span><span>Probandoagregarunnuevoticketco</span></p>', NULL, NULL, NULL, '2021-02-24 09:54:24', 'aac4a702-76b7-11eb-ab88-5254009623cd', '2021-02-26 11:06:49', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'OTBkMTEzYjktNzZiOC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_33_a64ba7a565'),
('9ea8702b-7142-11eb-94a9-04d4c4afee7c', '72059d56be', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 13, 2, 'Correos\'\'', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'hola123', NULL, NULL, NULL, '2021-02-17 11:07:30', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-17 20:24:23', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'OWVhYWQ1YjUtNzE0Mi0xMWViLTk0YTktMDRkNGM0YWZlZTdj', 'ticket_13_72059d56be'),
('a3bf7fee-706c-11eb-8037-04d4c4afee7c', 'fa8f8cd596', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 4, 4, 'Otro ticket sin archivos, motivo 1', 'b3f3ca52-6bbf-11eb-9393-04d4c4afee7c', 'Este ticket es para probar toda la info que se muestre bien, que no tenga errores o asi... Este ticket es para probar toda la info que se muestre bien, que no tenga errores o asi... Este ticket es para probar toda la info que se muestre bien, que no tenga errores o asi... Este ticket es para probar toda la info que se muestre bien, que no tenga errores o asi... Este ticket es para probar toda la info que se muestre bien, que no tenga errores o asi... Este ticket es para probar toda la info que s', NULL, NULL, NULL, '2021-02-16 09:35:47', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-26 11:10:38', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'YTNjMjc3NDQtNzA2Yy0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'ticket_4_fa8f8cd596'),
('aac1a7ea-720e-11eb-ab88-5254009623cd', 'f543fa2491', '72b9ba40-720d-11eb-ab88-5254009623cd', 20, 4, 'Creación de nuevo correo', 'b3f3ca52-6bbf-11eb-9393-04d4c4afee7c', 'Hola, buenas tardes. \r\n\r\nMe podrían crear un nuevo correo que sea eddypulido@sonder.mx ? Por favor. \r\n\r\nMuchas gracias', 1, 'Pronta respuesta. Todo ok', '2021-02-18 11:31:28', '2021-02-18 11:28:08', 'b527b3b5-720d-11eb-ab88-5254009623cd', '2021-02-18 11:29:39', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'YWFjMjRhYzctNzIwZS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_20_f543fa2491'),
('b914adaf-7210-11eb-ab88-5254009623cd', 'd470d9c838', '72b9ba40-720d-11eb-ab88-5254009623cd', 21, 4, 'corto', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'corto', NULL, NULL, NULL, '2021-02-18 11:42:50', 'b527b3b5-720d-11eb-ab88-5254009623cd', '2021-02-26 11:08:26', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'YjkxZGI0YjktNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_21_d470d9c838'),
('bbbae463-6cc4-11eb-b74c-04d4c4afee7c', '018a8b81cb', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 2, 4, 'otro antes Prueba', 'b3f3ca52-6bbf-11eb-9393-04d4c4afee7c', 'hola', 1, 'muy bien', '2021-02-16 16:37:22', '2021-02-11 17:56:18', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-15 17:59:42', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'YmJiZGYzNWUtNmNjNC0xMWViLWI3NGMtMDRkNGM0YWZlZTdj', 'ticket_2_018a8b81cb'),
('c62577ce-7142-11eb-94a9-04d4c4afee7c', '77564a2ae4', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 14, 4, 'correos2\'\'', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'asdasdasdasd', NULL, NULL, NULL, '2021-02-17 11:08:37', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-17 16:01:29', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'YzYyNWEyMGItNzE0Mi0xMWViLTk0YTktMDRkNGM0YWZlZTdj', 'ticket_14_77564a2ae4'),
('cc641a15-7210-11eb-ab88-5254009623cd', 'd6f88ae328', '72b9ba40-720d-11eb-ab88-5254009623cd', 22, 2, 'Todo ............................................5', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'Corto', NULL, NULL, NULL, '2021-02-18 11:43:23', 'b527b3b5-720d-11eb-ab88-5254009623cd', '2021-02-26 11:08:14', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'Y2M2NGQyYzUtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_22_d6f88ae328'),
('debd9b61-7210-11eb-ab88-5254009623cd', '4d60fe2765', '72b9ba40-720d-11eb-ab88-5254009623cd', 23, 4, 'Corto', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'Todo ..............................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................5', NULL, NULL, NULL, '2021-02-18 11:43:54', 'b527b3b5-720d-11eb-ab88-5254009623cd', '2021-02-26 11:07:57', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZGViZmM0MTItNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_23_4d60fe2765'),
('e8d06904-76d1-11eb-ab88-5254009623cd', '2cc9acc5ed', 'c9e9efe5-76cf-11eb-ab88-5254009623cd', 36, 4, 'Configuración no funciona', 'a9c5bd69-72d4-11eb-9f9c-04d4c4afee7c', '<p>Estoy intentando dar de alta los parámetros de configuración en la plataforma para después poder crear productos y no funciona. No se guardan.</p>', 1, 'Todo excelente !!!', '2021-02-24 13:30:09', '2021-02-24 12:55:49', '1a623981-76d1-11eb-ab88-5254009623cd', '2021-02-24 13:24:15', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZThkMGYzOTUtNzZkMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_36_2cc9acc5ed'),
('e8e3f06a-7142-11eb-94a9-04d4c4afee7c', '0910e199c7', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 15, 2, 'correos3', 'b3f3ca52-6bbf-11eb-9393-04d4c4afee7c', 'prueba de correo ticket', NULL, NULL, NULL, '2021-02-17 11:09:35', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-17 12:13:22', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZThlNjYxNWEtNzE0Mi0xMWViLTk0YTktMDRkNGM0YWZlZTdj', 'ticket_15_0910e199c7'),
('eb57629c-7177-11eb-ab88-5254009623cd', '7ab6805ea7', '90784123-6fb5-11eb-adea-04d4c4afee7c', 19, 4, 'Otro ticket', 'b3f3d784-6bbf-11eb-9393-04d4c4afee7c', 'Sigue sin poder descargarse el zip', NULL, NULL, NULL, '2021-02-17 17:29:02', '1690dcb1-70a8-11eb-8037-04d4c4afee7c', '2021-02-17 17:48:47', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZWI1N2Q0NzMtNzE3Ny0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_19_7ab6805ea7'),
('f138856b-9bbe-11eb-836c-5254009623cd', 'aad1d70fd3', 'e4fc2785-7079-11eb-8037-04d4c4afee7c', 37, 4, 'dfdfgdfg', 'b3f3dd67-6bbf-11eb-9393-04d4c4afee7c', '<p>dfgdfgdgdfgdfgdfgdfg<br></p>', 1, 'todo chido', '2021-04-12 13:45:06', '2021-04-12 13:43:15', 'addb5c01-76b3-11eb-ab88-5254009623cd', '2021-04-12 13:44:27', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZjEzOGVhZTMtOWJiZS0xMWViLTgzNmMtNTI1NDAwOTYyM2Nk', 'ticket_37_aad1d70fd3'),
('f594ace3-7210-11eb-ab88-5254009623cd', '038e45371a', '72b9ba40-720d-11eb-ab88-5254009623cd', 24, 4, 'Todo S .........................................50', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'Todo  .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 ........50', 2, ' .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .............50', '2021-02-18 11:52:15', '2021-02-18 11:44:32', 'b527b3b5-720d-11eb-ab88-5254009623cd', '2021-02-18 11:50:37', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZjU5NjdkOGYtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_24_038e45371a'),
('f65f71fe-70ab-11eb-8037-04d4c4afee7c', '5629029a34', '90784123-6fb5-11eb-adea-04d4c4afee7c', 8, 4, 'hola, probando al calificar', 'b3f3d784-6bbf-11eb-9393-04d4c4afee7c', 'dsfhdbfhbdf', NULL, NULL, NULL, '2021-02-16 17:09:03', '1690dcb1-70a8-11eb-8037-04d4c4afee7c', '2021-02-26 11:10:12', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZjY2MWQzYWYtNzBhYi0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'ticket_8_5629029a34'),
('f80916fd-76b9-11eb-ab88-5254009623cd', 'ca2f2f70ed', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 35, 2, 'Lorem  diseño gráfico', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', '<p><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">Lorem ipsum es el texto que se usa habitualmente en diseño gráfico en demostraciones de tipografías o de borradores de diseño para probar el diseño visual antes de insertar el texto final.</span><br></p>', NULL, NULL, NULL, '2021-02-24 10:04:26', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-08-27 15:43:45', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZjgwYjM5NTQtNzZiOS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'ticket_35_ca2f2f70ed'),
('fb4d45c6-706d-11eb-8037-04d4c4afee7c', '455399671a', '0c7b72cb-6745-11eb-92eb-04d4c4afee7c', 6, 4, 'Otro ticket', 'b3f3ca52-6bbf-11eb-9393-04d4c4afee7c', 'hola', NULL, NULL, NULL, '2021-02-16 09:45:23', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', '2021-02-16 09:50:01', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'ZmI0ZmFkMzMtNzA2ZC0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'ticket_6_455399671a'),
('fe795d17-70ab-11eb-8037-04d4c4afee7c', '8407f31717', '90784123-6fb5-11eb-adea-04d4c4afee7c', 9, 4, 'nueva prueba', 'b3f3d13e-6bbf-11eb-9393-04d4c4afee7c', 'dsfnkjdnsjnds', NULL, NULL, NULL, '2021-02-16 17:09:17', '1690dcb1-70a8-11eb-8037-04d4c4afee7c', '2021-02-26 11:09:54', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZmU3YmM1YWMtNzBhYi0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'ticket_9_8407f31717');

-- --------------------------------------------------------

--
-- Table structure for table `tickets_archivos_cb0a4d09`
--

CREATE TABLE `tickets_archivos_cb0a4d09` (
  `sk_archivo` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `clave_archivo` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fk_ticket` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `archivo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_archivo_original` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `orden` int(2) NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `hash_acciones` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `tickets_archivos_cb0a4d09`
--

INSERT INTO `tickets_archivos_cb0a4d09` (`sk_archivo`, `clave_archivo`, `fk_ticket`, `archivo`, `nombre_archivo_original`, `orden`, `fecha_alta`, `hash_acciones`) VALUES
('048db28a-7177-11eb-ab88-5254009623cd', 'e7dca709c4', '04894208-7177-11eb-ab88-5254009623cd', 'ticket_602da53b9c598_e727e084de_2021-02-17_17-22-35.PNG', 'diseno.PNG', 1, '2021-02-17 17:22:35', 'MDQ4ZTFiOTEtNzE3Ny0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('0cd8d67a-706d-11eb-8037-04d4c4afee7c', '6a6ebd8369', '0cccf142-706d-11eb-8037-04d4c4afee7c', 'ticket_602be7036c75d_6861f4528a_2021-02-16_09-38-43.jpg', 'img.jpg', 1, '2021-02-16 09:38:43', 'MGNkYzNlNTctNzA2ZC0xMWViLTgwMzctMDRkNGM0YWZlZTdj'),
('0ce0f6c4-706d-11eb-8037-04d4c4afee7c', '5a0cb4931f', '0cccf142-706d-11eb-8037-04d4c4afee7c', 'ticket_602be70377d76_6861f4528a_2021-02-16_09-38-43.png', 'img_dia.png', 2, '2021-02-16 09:38:43', 'MGNlMzVjYjAtNzA2ZC0xMWViLTgwMzctMDRkNGM0YWZlZTdj'),
('0cedef10-706d-11eb-8037-04d4c4afee7c', '9c3d0d273a', '0cccf142-706d-11eb-8037-04d4c4afee7c', 'ticket_602be7038c5b9_6861f4528a_2021-02-16_09-38-43.png', '1000-1000.png', 3, '2021-02-16 09:38:43', 'MGNmMDM0ZTgtNzA2ZC0xMWViLTgwMzctMDRkNGM0YWZlZTdj'),
('1b420e1a-713f-11eb-94a9-04d4c4afee7c', '7061183934', '1b32678c-713f-11eb-94a9-04d4c4afee7c', 'ticket_602d476de22d9_5f847062be_2021-02-17_10-42-21.jpg', 'img.jpg', 1, '2021-02-17 10:42:21', 'MWI0NDI2ZjYtNzEzZi0xMWViLTk0YTktMDRkNGM0YWZlZTdj'),
('27a18387-6cbb-11eb-b74c-04d4c4afee7c', '62b83762a4', '27978fd7-6cbb-11eb-b74c-04d4c4afee7c', 'ticket_6025b41075c3c_855513e815_2021-02-11_16-47-44.png', 'mockup.png', 1, '2021-02-11 16:47:44', 'MjdhNjM0MzctNmNiYi0xMWViLWI3NGMtMDRkNGM0YWZlZTdj'),
('27af409d-6cbb-11eb-b74c-04d4c4afee7c', 'e1e886dc1d', '27978fd7-6cbb-11eb-b74c-04d4c4afee7c', 'ticket_6025b410890ab_855513e815_2021-02-11_16-47-44.jpg', 'perfil.jpg', 2, '2021-02-11 16:47:44', 'MjdiMjQ5ZDYtNmNiYi0xMWViLWI3NGMtMDRkNGM0YWZlZTdj'),
('27be7954-6cbb-11eb-b74c-04d4c4afee7c', '8c5daf3bbb', '27978fd7-6cbb-11eb-b74c-04d4c4afee7c', 'ticket_6025b410a1858_855513e815_2021-02-11_16-47-44.jpg', 'perfil2.jpg', 3, '2021-02-11 16:47:44', 'MjdjMTk3MmItNmNiYi0xMWViLWI3NGMtMDRkNGM0YWZlZTdj'),
('27cab5d9-6cbb-11eb-b74c-04d4c4afee7c', '3ae3fa3dcc', '27978fd7-6cbb-11eb-b74c-04d4c4afee7c', 'ticket_6025b410b0709_855513e815_2021-02-11_16-47-44.png', 'person-3160886_1920.png', 4, '2021-02-11 16:47:44', 'MjdjYWU5NjktNmNiYi0xMWViLWI3NGMtMDRkNGM0YWZlZTdj'),
('28c1c69f-7139-11eb-94a9-04d4c4afee7c', 'da5bcfe0f0', '28b61430-7139-11eb-94a9-04d4c4afee7c', 'ticket_602d3d7394b34_9f0833d440_2021-02-17_09-59-47.jpg', 'ailor-moon.jpg', 1, '2021-02-17 09:59:47', 'MjhjNjNkN2UtNzEzOS0xMWViLTk0YTktMDRkNGM0YWZlZTdj'),
('28d43bea-7139-11eb-94a9-04d4c4afee7c', '2bd22d7fb2', '28b61430-7139-11eb-94a9-04d4c4afee7c', 'ticket_602d3d73aed82_9f0833d440_2021-02-17_09-59-47.PNG', 'diseno.PNG', 2, '2021-02-17 09:59:47', 'MjhkNjllN2ItNzEzOS0xMWViLTk0YTktMDRkNGM0YWZlZTdj'),
('28dd90e2-7139-11eb-94a9-04d4c4afee7c', '63a4b1df45', '28b61430-7139-11eb-94a9-04d4c4afee7c', 'ticket_602d3d73bda9c_9f0833d440_2021-02-17_09-59-47.jpg', 'mockup.jpg', 3, '2021-02-17 09:59:47', 'MjhkZmUxYzUtNzEzOS0xMWViLTk0YTktMDRkNGM0YWZlZTdj'),
('28e6b566-7139-11eb-94a9-04d4c4afee7c', '6ed9d79662', '28b61430-7139-11eb-94a9-04d4c4afee7c', 'ticket_602d3d73cc7ea_9f0833d440_2021-02-17_09-59-47.png', 'pruebadiseno.png', 4, '2021-02-17 09:59:47', 'MjhlOTI2NzctNzEzOS0xMWViLTk0YTktMDRkNGM0YWZlZTdj'),
('28f030fa-7139-11eb-94a9-04d4c4afee7c', '21d99dba7c', '28b61430-7139-11eb-94a9-04d4c4afee7c', 'ticket_602d3d73dbad9_9f0833d440_2021-02-17_09-59-47.png', 'Sailor Moon on Moon Anime_thumb[4].png', 5, '2021-02-17 09:59:47', 'MjhmMmEyZjQtNzEzOS0xMWViLTk0YTktMDRkNGM0YWZlZTdj'),
('4329fd5f-76b8-11eb-ab88-5254009623cd', 'e035d5d6a9', '4320cb6b-76b8-11eb-ab88-5254009623cd', 'ticket_6036762dc07f4_a4e092b455_2021-02-24_09-52-13.svg', 'icon-usuario-1.svg', 1, '2021-02-24 09:52:13', 'NDMyYWY4ZDYtNzZiOC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('432d4ccc-76b8-11eb-ab88-5254009623cd', '30faf8b4b8', '4320cb6b-76b8-11eb-ab88-5254009623cd', 'ticket_6036762dc47f8_a4e092b455_2021-02-24_09-52-13.svg', 'icon-usuario-5.svg', 2, '2021-02-24 09:52:13', 'NDMyZDdmY2ItNzZiOC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('43336c90-76b8-11eb-ab88-5254009623cd', '71733d5217', '4320cb6b-76b8-11eb-ab88-5254009623cd', 'ticket_6036762dcee1e_a4e092b455_2021-02-24_09-52-13.svg', 'icon-votante.svg', 3, '2021-02-24 09:52:13', 'NDMzM2YzYzUtNzZiOC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('7888eb0d-70a7-11eb-8037-04d4c4afee7c', 'dfa827e9c6', '787c11ad-70a7-11eb-8037-04d4c4afee7c', 'ticket_602c4906c1453_9515f9a533_2021-02-16_16-36-54.jpg', 'img.jpg', 1, '2021-02-16 16:36:54', 'Nzg4YjRhZWQtNzBhNy0xMWViLTgwMzctMDRkNGM0YWZlZTdj'),
('8dc40ae8-706c-11eb-8037-04d4c4afee7c', '6a0da2489b', '8db70adf-706c-11eb-8037-04d4c4afee7c', 'ticket_602be62e3849c_ad60e66c92_2021-02-16_09-35-10.png', 'blank-1886001_1920.png', 1, '2021-02-16 09:35:10', 'OGRjNjZjNmUtNzA2Yy0xMWViLTgwMzctMDRkNGM0YWZlZTdj'),
('8dd0f67e-706c-11eb-8037-04d4c4afee7c', '1c4f95c64e', '8db70adf-706c-11eb-8037-04d4c4afee7c', 'ticket_602be62e4cf40_ad60e66c92_2021-02-16_09-35-10.png', 'mockup.png', 2, '2021-02-16 09:35:10', 'OGRkMzU2ZWEtNzA2Yy0xMWViLTgwMzctMDRkNGM0YWZlZTdj'),
('8ddf5cbb-706c-11eb-8037-04d4c4afee7c', '306b347813', '8db70adf-706c-11eb-8037-04d4c4afee7c', 'ticket_602be62e63ecb_ad60e66c92_2021-02-16_09-35-10.jpg', 'perfil.jpg', 3, '2021-02-16 09:35:10', 'OGRlMWJiNWItNzA2Yy0xMWViLTgwMzctMDRkNGM0YWZlZTdj'),
('8deab79d-706c-11eb-8037-04d4c4afee7c', '4931d1c0dd', '8db70adf-706c-11eb-8037-04d4c4afee7c', 'ticket_602be62e76440_ad60e66c92_2021-02-16_09-35-10.jpg', 'perfil2.jpg', 4, '2021-02-16 09:35:10', 'OGRlZDI4OTYtNzA2Yy0xMWViLTgwMzctMDRkNGM0YWZlZTdj'),
('8df22aeb-706c-11eb-8037-04d4c4afee7c', 'bd02216162', '8db70adf-706c-11eb-8037-04d4c4afee7c', 'ticket_602be62e82188_ad60e66c92_2021-02-16_09-35-10.png', 'person-3160886_1920.png', 5, '2021-02-16 09:35:10', 'OGRmNDhkYTYtNzA2Yy0xMWViLTgwMzctMDRkNGM0YWZlZTdj'),
('8f0e0185-76b5-11eb-ab88-5254009623cd', 'f221e6c702', '8f030877-76b5-11eb-ab88-5254009623cd', 'ticket_603671a497649_67dd391c36_2021-02-24_09-32-52.png', 'blank-1886001_1920.png', 1, '2021-02-24 09:32:52', 'OGYwZWMyNzQtNzZiNS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('8f112f0d-76b5-11eb-ab88-5254009623cd', '6d409c85dc', '8f030877-76b5-11eb-ab88-5254009623cd', 'ticket_603671a49cf12_67dd391c36_2021-02-24_09-32-52.png', 'mockup.png', 2, '2021-02-24 09:32:52', 'OGYxMjJiMTYtNzZiNS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('8f192897-76b5-11eb-ab88-5254009623cd', '9bd96d1f03', '8f030877-76b5-11eb-ab88-5254009623cd', 'ticket_603671a4aa114_67dd391c36_2021-02-24_09-32-52.jpg', 'perfil.jpg', 3, '2021-02-24 09:32:52', 'OGYxYTZmMTktNzZiNS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('8f1ff792-76b5-11eb-ab88-5254009623cd', '522481922c', '8f030877-76b5-11eb-ab88-5254009623cd', 'ticket_603671a4b4388_67dd391c36_2021-02-24_09-32-52.jpg', 'perfil2.jpg', 4, '2021-02-24 09:32:52', 'OGYyMGFjZTMtNzZiNS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('8f24b139-76b5-11eb-ab88-5254009623cd', '427f85cd4c', '8f030877-76b5-11eb-ab88-5254009623cd', 'ticket_603671a4bb984_67dd391c36_2021-02-24_09-32-52.png', 'person-3160886_1920.png', 5, '2021-02-24 09:32:52', 'OGYyNTYzODQtNzZiNS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('b91f7f1f-7210-11eb-ab88-5254009623cd', '4ce208dbef', 'b914adaf-7210-11eb-ab88-5254009623cd', 'ticket_602ea71b8fa80_d470d9c838_2021-02-18_11-42-50.png', 'Square.png', 1, '2021-02-18 11:42:50', 'YjkyMDYyYjAtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('bbcd1147-6cc4-11eb-b74c-04d4c4afee7c', 'eebf66e28e', 'bbbae463-6cc4-11eb-b74c-04d4c4afee7c', 'ticket_6025c4228172c_018a8b81cb_2021-02-11_17-56-18.jpg', 'perfil2.jpg', 1, '2021-02-11 17:56:18', 'YmJkMDIwYWMtNmNjNC0xMWViLWI3NGMtMDRkNGM0YWZlZTdj'),
('bbd94ed7-6cc4-11eb-b74c-04d4c4afee7c', 'af2de0b711', 'bbbae463-6cc4-11eb-b74c-04d4c4afee7c', 'ticket_6025c422950f3_018a8b81cb_2021-02-11_17-56-18.png', 'person-3160886_1920.png', 2, '2021-02-11 17:56:18', 'YmJkYzVmMzYtNmNjNC0xMWViLWI3NGMtMDRkNGM0YWZlZTdj'),
('cc666113-7210-11eb-ab88-5254009623cd', 'f7a74f52a5', 'cc641a15-7210-11eb-ab88-5254009623cd', 'ticket_602ea73be26cc_d6f88ae328_2021-02-18_11-43-23.jpg', 'Sudadera Unisex.jpg', 1, '2021-02-18 11:43:23', 'Y2M2NmVkYTUtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('cc685577-7210-11eb-ab88-5254009623cd', '2654144eda', 'cc641a15-7210-11eb-ab88-5254009623cd', 'ticket_602ea73be519f_d6f88ae328_2021-02-18_11-43-23.jpg', 'Mujer Cuello Redondo.jpg', 2, '2021-02-18 11:43:23', 'Y2M2ODlhZTgtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('cc6a32f6-7210-11eb-ab88-5254009623cd', 'bc5f9eb5d2', 'cc641a15-7210-11eb-ab88-5254009623cd', 'ticket_602ea73be837d_d6f88ae328_2021-02-18_11-43-23.png', 'Square.png', 3, '2021-02-18 11:43:23', 'Y2M2YTg5MzUtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('cc6c2000-7210-11eb-ab88-5254009623cd', '2b7444b950', 'cc641a15-7210-11eb-ab88-5254009623cd', 'ticket_602ea73beb2cd_d6f88ae328_2021-02-18_11-43-23.png', 'mini_magick20190215-29647-grf1kw.png', 4, '2021-02-18 11:43:23', 'Y2M2YzY5M2YtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('cc6db92d-7210-11eb-ab88-5254009623cd', 'e33759cc32', 'cc641a15-7210-11eb-ab88-5254009623cd', 'ticket_602ea73bedcaf_d6f88ae328_2021-02-18_11-43-23.jpg', 'maxresdefault.jpg', 5, '2021-02-18 11:43:23', 'Y2M2ZTAzNDQtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('ded818b1-7210-11eb-ab88-5254009623cd', 'ccd75e5828', 'debd9b61-7210-11eb-ab88-5254009623cd', 'ticket_602ea75b045cc_4d60fe2765_2021-02-18_11-43-54.jpg', 'Sudadera Unisex.jpg', 1, '2021-02-18 11:43:54', 'ZGVlZWY0OWYtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('df055f68-7210-11eb-ab88-5254009623cd', 'bf6f3cc40a', 'debd9b61-7210-11eb-ab88-5254009623cd', 'ticket_602ea75b28fb9_4d60fe2765_2021-02-18_11-43-54.png', 'Square.png', 2, '2021-02-18 11:43:54', 'ZGYwNWQ3NmItNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('df0a8cbb-7210-11eb-ab88-5254009623cd', '2c5830ccec', 'debd9b61-7210-11eb-ab88-5254009623cd', 'ticket_602ea75b31498_4d60fe2765_2021-02-18_11-43-54.png', '670d3c4-es-oxxopay-stub.png', 3, '2021-02-18 11:43:54', 'ZGYwYjA3NTQtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('df0d413e-7210-11eb-ab88-5254009623cd', 'e99f81823f', 'debd9b61-7210-11eb-ab88-5254009623cd', 'ticket_602ea75b36854_4d60fe2765_2021-02-18_11-43-54.png', '9dae3cf338175009d83ad71b38df827e.png', 4, '2021-02-18 11:43:54', 'ZGYwZTRkODQtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('df105d7d-7210-11eb-ab88-5254009623cd', 'ef0ac6dfac', 'debd9b61-7210-11eb-ab88-5254009623cd', 'ticket_602ea75b3aff9_4d60fe2765_2021-02-18_11-43-54.jpg', 'maxresdefault.jpg', 5, '2021-02-18 11:43:54', 'ZGYxMTEyZjMtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('e8d38df7-76d1-11eb-ab88-5254009623cd', '64858192cf', 'e8d06904-76d1-11eb-ab88-5254009623cd', 'ticket_6036a1352274f_2cc9acc5ed_2021-02-24_12-55-49.png', 'Captura de Pantalla 2021-02-24 a la(s) 12.55.36 p.m..png', 1, '2021-02-24 12:55:49', 'ZThkNDY2ZjMtNzZkMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('eb59c1a0-7177-11eb-ab88-5254009623cd', 'c88e2e5aa2', 'eb57629c-7177-11eb-ab88-5254009623cd', 'ticket_602da6bed686e_7ab6805ea7_2021-02-17_17-29-02.PNG', 'diseno.PNG', 1, '2021-02-17 17:29:02', 'ZWI1ZGVmYjYtNzE3Ny0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('f13b7956-9bbe-11eb-836c-5254009623cd', '35b0fa5b60', 'f138856b-9bbe-11eb-836c-5254009623cd', 'ticket_607494c3c9760_aad1d70fd3_2021-04-12_13-43-15.png', 'transformarte-b.png', 1, '2021-04-12 13:43:15', 'ZjEzYmM5YWUtOWJiZS0xMWViLTgzNmMtNTI1NDAwOTYyM2Nk'),
('f5a41df3-7210-11eb-ab88-5254009623cd', '4ac71be346', 'f594ace3-7210-11eb-ab88-5254009623cd', 'ticket_602ea7811d23b_038e45371a_2021-02-18_11-44-32.jpg', 'Sudadera Unisex.jpg', 1, '2021-02-18 11:44:32', 'ZjVhNGM2NmUtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('f5abd605-7210-11eb-ab88-5254009623cd', '3cf8838cce', 'f594ace3-7210-11eb-ab88-5254009623cd', 'ticket_602ea7812a124_038e45371a_2021-02-18_11-44-32.png', 'Square.png', 2, '2021-02-18 11:44:32', 'ZjVhY2M5ZmQtNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('f5b1f188-7210-11eb-ab88-5254009623cd', '6c4471888c', 'f594ace3-7210-11eb-ab88-5254009623cd', 'ticket_602ea7813a0a6_038e45371a_2021-02-18_11-44-32.jpg', 'Mujer Cuello Redondo.jpg', 3, '2021-02-18 11:44:32', 'ZjViNmQ0N2ItNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('f5ba860f-7210-11eb-ab88-5254009623cd', '6e7393797d', 'f594ace3-7210-11eb-ab88-5254009623cd', 'ticket_602ea78140c92_038e45371a_2021-02-18_11-44-32.jpeg', 'images.jpeg', 4, '2021-02-18 11:44:32', 'ZjViYjBjMjItNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('f5c2f9ca-7210-11eb-ab88-5254009623cd', 'fbfb2b92b7', 'f594ace3-7210-11eb-ab88-5254009623cd', 'ticket_602ea7814f305_038e45371a_2021-02-18_11-44-32.png', '670d3c4-es-oxxopay-stub.png', 5, '2021-02-18 11:44:32', 'ZjVjM2ZlNzItNzIxMC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk');

-- --------------------------------------------------------

--
-- Table structure for table `tickets_entradas_09f26b6a`
--

CREATE TABLE `tickets_entradas_09f26b6a` (
  `sk_entrada` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `fk_ticket` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `clave_entrada` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_entrada` int(1) NOT NULL,
  `comentario` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'Validar 500',
  `fecha_alta` datetime NOT NULL,
  `usuario_alta` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `hash_acciones` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `carpeta_entrada` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `tickets_entradas_09f26b6a`
--

INSERT INTO `tickets_entradas_09f26b6a` (`sk_entrada`, `fk_ticket`, `clave_entrada`, `tipo_entrada`, `comentario`, `fecha_alta`, `usuario_alta`, `hash_acciones`, `carpeta_entrada`) VALUES
('03a51fcc-7855-11eb-ab88-5254009623cd', '90cfe8fb-76b8-11eb-ab88-5254009623cd', '85f7078feb', 1, '<p>Testing</p>', '2021-02-26 11:06:49', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MDNhNTlmZjctNzg1NS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_85f7078feb'),
('0608de1c-7524-11eb-ab88-5254009623cd', '13c82757-730b-11eb-ab88-5254009623cd', '8ca55ebcb6', 1, 'ULTIMA ENTRADA ANTES DE CERRAR EL CASO !&nbsp;', '2021-02-22 09:38:34', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MDYwOWZhMGYtNzUyNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_8ca55ebcb6'),
('06212a59-730c-11eb-ab88-5254009623cd', '13c82757-730b-11eb-ab88-5254009623cd', '8c7a5645dc', 2, '<p>otra respuesta porque no me recargo</p>', '2021-02-19 17:41:44', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'MDYyMTU0NmYtNzMwYy0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_8c7a5645dc'),
('09b0fa74-7780-11eb-ab88-5254009623cd', '6691df09-76b8-11eb-ab88-5254009623cd', '15a1435161', 1, '<p>Vamos a cerrar este ticket porque no encontramos nada <br></p>', '2021-02-25 09:42:16', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MDliMzZhYzUtNzc4MC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_15a1435161'),
('0ac6b658-7855-11eb-ab88-5254009623cd', '49e12f12-76b8-11eb-ab88-5254009623cd', '884509621c', 1, '<p>Testing<br></p>', '2021-02-26 11:07:01', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MGFjNzBjZjItNzg1NS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_884509621c'),
('0c942f6a-706e-11eb-8037-04d4c4afee7c', 'fb4d45c6-706d-11eb-8037-04d4c4afee7c', '65013a1d55', 2, 'entrada!', '2021-02-16 09:45:52', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'MGM5NjkwNTgtNzA2ZS0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'entrada_65013a1d55'),
('10d2d2e0-7855-11eb-ab88-5254009623cd', '4320cb6b-76b8-11eb-ab88-5254009623cd', '2248b11867', 1, '<p>Testing<br></p>', '2021-02-26 11:07:11', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MTBkMzBiYzAtNzg1NS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_2248b11867'),
('1411a947-714b-11eb-94a9-04d4c4afee7c', 'e8e3f06a-7142-11eb-94a9-04d4c4afee7c', 'abbb5b3137', 2, 'entrada de admin', '2021-02-17 12:08:03', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'MTQxM2VhZTUtNzE0Yi0xMWViLTk0YTktMDRkNGM0YWZlZTdj', 'entrada_abbb5b3137'),
('1740e655-7855-11eb-ab88-5254009623cd', '18e10e5f-76b8-11eb-ab88-5254009623cd', '4f612fd762', 1, '<p>Testing<br></p>', '2021-02-26 11:07:22', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MTc0MTQyZGItNzg1NS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_4f612fd762'),
('19db0a75-7524-11eb-ab88-5254009623cd', '13c82757-730b-11eb-ab88-5254009623cd', '8d01817f6a', 1, '<p>BUENO UNA MAS&nbsp;</p>', '2021-02-22 09:39:07', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MTlkZDAwNmItNzUyNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_8d01817f6a'),
('1c3c37b3-9bbf-11eb-836c-5254009623cd', 'f138856b-9bbe-11eb-836c-5254009623cd', 'ea74f072ea', 1, '<p>dsfsdfdsfdsfdsf<br></p>', '2021-04-12 13:44:27', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MWMzY2Q4ZDMtOWJiZi0xMWViLTgzNmMtNTI1NDAwOTYyM2Nk', 'entrada_ea74f072ea'),
('1df97afe-7855-11eb-ab88-5254009623cd', '636a36a3-730b-11eb-ab88-5254009623cd', '36a7b5e928', 1, '<p>Testing<br></p>', '2021-02-26 11:07:33', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MWRmOWJjZjMtNzg1NS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_36a7b5e928'),
('1ea7d4c0-76d4-11eb-ab88-5254009623cd', 'e8d06904-76d1-11eb-ab88-5254009623cd', '7654177a4f', 1, '<p>Se me pasó adjuntar el archivo, va:&nbsp;</p><p><br></p><pre><font color=\"#000000\" face=\"label\" size=\"4\"><span style=\"caret-color: rgb(0, 0, 0); white-space: normal;\">------</span></font></pre><pre>Hola Carlos, creo que tal vez puede ser por el caché o las cookies de tu navegador.<br>Te mando aquí un PDF con las instrucciones de como eliminarlo, me puedes ayudar a hacer eso y después volver a revisar ? Porfa<br>Gracias, saludos.</pre>', '2021-02-24 13:11:38', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MWViZTBkOTEtNzZkNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_7654177a4f'),
('24963089-76ba-11eb-ab88-5254009623cd', 'f80916fd-76b9-11eb-ab88-5254009623cd', '32b6bf2df7', 2, '<p></p><p><span style=\"color: rgb(77, 81, 86); font-family: arial, sans-serif; font-size: 14px;\">Lorem ipsum es el texto que se usa habitualmente en diseño gráfico en demostraciones de tipografías o de borradores de diseño para probar el diseño visual antes de insertar el texto final.</span><br></p>', '2021-02-24 10:05:41', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'MjQ5Njg1MWEtNzZiYS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_32b6bf2df7'),
('2b14bcc4-730d-11eb-ab88-5254009623cd', '13c82757-730b-11eb-ab88-5254009623cd', 'db0b4e3fe8', 2, '<p><b>Hola!! Probando respuestas con Eddy</b></p><p><br></p><ol><li><b>Lista uno</b></li><li><b>Lista dos</b></li><li><b>Lista tres&nbsp;</b></li></ol><p><br></p><table class=\"table table-bordered\"><tbody><tr><td>tabla</td><td>tabla</td><td>tabla</td></tr><tr><td>contenido</td><td>contenido<br></td><td>contenido<br></td></tr></tbody></table><p><b style=\"background-color: rgb(0, 255, 0);\">asdasdasd</b></p>', '2021-02-19 17:49:55', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'MmIxNGZmMGItNzMwZC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_db0b4e3fe8'),
('2c0d5bca-7211-11eb-ab88-5254009623cd', 'f594ace3-7210-11eb-ab88-5254009623cd', '3e9f271828', 1, 'Todo .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........50', '2021-02-18 11:46:04', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MmMwZTUwZWMtNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_3e9f271828'),
('2c3cdf6e-7855-11eb-ab88-5254009623cd', 'debd9b61-7210-11eb-ab88-5254009623cd', '4a1ec0284f', 1, '<p>Testing<br></p>', '2021-02-26 11:07:57', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MmMzZDIzNjctNzg1NS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_4a1ec0284f'),
('2e214b33-7780-11eb-ab88-5254009623cd', 'f80916fd-76b9-11eb-ab88-5254009623cd', 'a7aa0276f3', 1, '<pre>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</pre>', '2021-02-25 09:43:17', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MmUyMjIzMzQtNzc4MC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_a7aa0276f3'),
('2fd49ab5-76b8-11eb-ab88-5254009623cd', '8f030877-76b5-11eb-ab88-5254009623cd', '9e8ee7a225', 2, '<h3><b>«La curiosidad mató al gato».</b></h3><p>Cuántas veces habremos escuchado esta expresión y cuántas otras me habré cuestionado el por qué de ésta.</p><p>La curiosidad, sumada a la prudencia, es el motor de nuestro avance \ncomo sociedad y, reduciendo la muestra, de nuestros progresos \npersonales. La curiosidad marca la diferencia entre unos y otros: nace \ncomo instinto, viaja en la motivación y concluye en la satisfacción de \nun aprendizaje.&nbsp;Ella es la protagonista de la sociedad de la información\n donde, las tecnologías, nos permiten satisfacer ese deseo de \nconocimiento inmediato.</p><p>La curiosidad, en su justa medida, beneficia al que la tiene.</p><p>Por lo tanto, me pregunto hasta qué punto decidió, el gato, que el \nriesgo se sumase a su curiosidad para que, ésta, le acabara matando.</p><p>Me introduzco de este modo para anticipar una entrada en el blog en \nforma de agradecimiento por adelantado para aquellas personas que, \nmañana, encuentren el texto de “la subasta de mi vida” en el metro y, en\n primer lugar, tengan ese «instinto» de averiguar de qué va ese papel. \nEn segundo lugar, para quien dedique unos minutos a leérselo y, en \ntercer lugar, para quien llegue hasta estas líneas puesto que, como \nmínimo, ya serás más curioso que la mayoría y, yo, siempre lo he \nconsiderado una importante ventaja.</p><p>\n\n\n\n\n\n</p><p>Sin ningún tipo de pretensión oculta en forma de estrategia, sin \nningún objetivo de difusión de propaganda o publicidad, sin ningún tipo \nde fin económico.</p>', '2021-02-24 09:51:41', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'MmZkNGUzMDctNzZiOC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_9e8ee7a225'),
('367b3f97-7855-11eb-ab88-5254009623cd', 'cc641a15-7210-11eb-ab88-5254009623cd', 'a517583818', 1, '<p>Testing<br></p>', '2021-02-26 11:08:14', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'MzY3YjY5MWUtNzg1NS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_a517583818'),
('36b8f3af-706d-11eb-8037-04d4c4afee7c', '0cccf142-706d-11eb-8037-04d4c4afee7c', '112e372512', 2, 'comentarios de entrada 1', '2021-02-16 09:39:53', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'MzZiYjcyZDctNzA2ZC0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'entrada_112e372512'),
('3a38ce0c-714b-11eb-94a9-04d4c4afee7c', 'e8e3f06a-7142-11eb-94a9-04d4c4afee7c', '3b7024ae48', 2, 'entrada cliente 2, no era admin jeje ', '2021-02-17 12:09:07', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'M2EzYjI1ZWEtNzE0Yi0xMWViLTk0YTktMDRkNGM0YWZlZTdj', 'entrada_3b7024ae48'),
('3dc1ec6a-7855-11eb-ab88-5254009623cd', 'b914adaf-7210-11eb-ab88-5254009623cd', '7a4804e553', 1, '<p>Testing<br></p>', '2021-02-26 11:08:26', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'M2RjNDg1NDEtNzg1NS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_7a4804e553'),
('3e5dca0e-76b7-11eb-ab88-5254009623cd', '8f030877-76b5-11eb-ab88-5254009623cd', 'a8a80e3158', 1, '<h3><b>«La curiosidad mató al gato».</b></h3>\r\n<p>Cuántas veces habremos escuchado esta expresión y cuántas otras me habré cuestionado el por qué de ésta.</p>\r\n<p>La curiosidad, sumada a la prudencia, es el motor de nuestro avance \r\ncomo sociedad y, reduciendo la muestra, de nuestros progresos \r\npersonales. La curiosidad marca la diferencia entre unos y otros: nace \r\ncomo instinto, viaja en la motivación y concluye en la satisfacción de \r\nun aprendizaje.&nbsp;Ella es la protagonista de la sociedad de la información\r\n donde, las tecnologías, nos permiten satisfacer ese deseo de \r\nconocimiento inmediato.</p>\r\n<p>La curiosidad, en su justa medida, beneficia al que la tiene.</p>\r\n<p>Por lo tanto', '2021-02-24 09:44:56', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'M2U1ZmQxYTAtNzZiNy0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_a8a80e3158'),
('423cb6ae-706e-11eb-8037-04d4c4afee7c', 'fb4d45c6-706d-11eb-8037-04d4c4afee7c', 'b71d0b5303', 2, 'otra entrada mas', '2021-02-16 09:47:22', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'NDIzZjQ5MjktNzA2ZS0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'entrada_b71d0b5303'),
('4748a6bf-7855-11eb-ab88-5254009623cd', '04894208-7177-11eb-ab88-5254009623cd', '228881d848', 1, '<p>Testing<br></p>', '2021-02-26 11:08:42', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'NDc0OGYwNGEtNzg1NS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_228881d848'),
('4ca0cf28-713f-11eb-94a9-04d4c4afee7c', '1b32678c-713f-11eb-94a9-04d4c4afee7c', '8d17e3b515', 1, 'Respuesta xsxdxdx', '2021-02-17 10:43:44', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'NGNhMzMwZGMtNzEzZi0xMWViLTk0YTktMDRkNGM0YWZlZTdj', 'entrada_8d17e3b515'),
('57c19e5d-76d2-11eb-ab88-5254009623cd', 'e8d06904-76d1-11eb-ab88-5254009623cd', 'bb9efdf7bc', 1, '<p>Que tal Carlos, cómo estás ? Buenas tardes.&nbsp;</p><p><br></p><p>Déjame revisar que es lo que está pasando y te regreso con la solución.&nbsp;</p><p><br></p><p>Una duda, con qué navegador lo estás revisando ?&nbsp;</p><p><br></p><p>Saludos</p>', '2021-02-24 12:58:55', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'NTdjNGU5YTgtNzZkMi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_bb9efdf7bc'),
('5873fb2c-7211-11eb-ab88-5254009623cd', 'f594ace3-7210-11eb-ab88-5254009623cd', 'ba9ee914eb', 2, 'Todo  .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 ........50', '2021-02-18 11:47:18', 'b527b3b5-720d-11eb-ab88-5254009623cd', 'NTg3NWRhZGUtNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_ba9ee914eb'),
('59322410-7139-11eb-94a9-04d4c4afee7c', '28b61430-7139-11eb-94a9-04d4c4afee7c', 'ac64b59714', 1, 'Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Ticket resuelto Tick', '2021-02-17 10:01:08', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'NTkzNTc5MzktNzEzOS0xMWViLTk0YTktMDRkNGM0YWZlZTdj', 'entrada_ac64b59714'),
('5ddabb2e-7190-11eb-ab88-5254009623cd', '9ea8702b-7142-11eb-94a9-04d4c4afee7c', '34efe1f045', 1, 'Prueba entrada', '2021-02-17 20:24:02', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'NWRkYjJmNGQtNzE5MC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_34efe1f045'),
('61dcf012-7211-11eb-ab88-5254009623cd', 'f594ace3-7210-11eb-ab88-5254009623cd', 'f307ac5c10', 2, ' .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .............50', '2021-02-18 11:47:34', 'b527b3b5-720d-11eb-ab88-5254009623cd', 'NjFkZGFhYmQtNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_f307ac5c10'),
('68d20c4d-7211-11eb-ab88-5254009623cd', 'f594ace3-7210-11eb-ab88-5254009623cd', '3ac9103913', 2, ' .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .............50', '2021-02-18 11:47:46', 'b527b3b5-720d-11eb-ab88-5254009623cd', 'NjhkNDZlMjItNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_3ac9103913'),
('6931cd86-7855-11eb-ab88-5254009623cd', '0698c0db-70ac-11eb-8037-04d4c4afee7c', '15de7d0fc8', 1, '<p>Testing<br></p>', '2021-02-26 11:09:39', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'NjkzNDAxOGYtNzg1NS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_15de7d0fc8'),
('6a1af0c6-7190-11eb-ab88-5254009623cd', '9ea8702b-7142-11eb-94a9-04d4c4afee7c', '1bf2d8fe2d', 1, 'Entrada dos', '2021-02-17 20:24:23', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'NmExYjg3MWQtNzE5MC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_1bf2d8fe2d'),
('6d4b949a-7211-11eb-ab88-5254009623cd', 'f594ace3-7210-11eb-ab88-5254009623cd', 'd61ab0b655', 2, ' .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .............50', '2021-02-18 11:47:53', 'b527b3b5-720d-11eb-ab88-5254009623cd', 'NmQ0YzZhNjEtNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_d61ab0b655'),
('7237d0f8-7855-11eb-ab88-5254009623cd', 'fe795d17-70ab-11eb-8037-04d4c4afee7c', '68c763b93b', 1, '<p>Testing<br></p>', '2021-02-26 11:09:54', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'NzIzODExYjMtNzg1NS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_68c763b93b'),
('72ee51ab-730c-11eb-ab88-5254009623cd', '13c82757-730b-11eb-ab88-5254009623cd', 'c480cddd67', 2, '<p>faltaban las modales, deja pruebo!!</p>', '2021-02-19 17:44:46', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'NzJlZWQ2ZWQtNzMwYy0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_c480cddd67'),
('78d74c99-0777-11ec-a3a6-5254009623cd', 'f80916fd-76b9-11eb-ab88-5254009623cd', '34fcfa7294', 1, '<p>sdfsdfdsfdsfdsfdsf<br></p>', '2021-08-27 15:43:45', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'NzhkNzZhYWItMDc3Ny0xMWVjLWEzYTYtNTI1NDAwOTYyM2Nk', 'entrada_34fcfa7294'),
('7c92da83-7855-11eb-ab88-5254009623cd', 'f65f71fe-70ab-11eb-8037-04d4c4afee7c', 'f46c7a3f13', 1, '<p>Testing<br></p>', '2021-02-26 11:10:12', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'N2M5MzlhMjAtNzg1NS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_f46c7a3f13'),
('8a44843f-721e-11eb-ab88-5254009623cd', '25c564ee-721e-11eb-ab88-5254009623cd', '4ffce90319', 1, 'ssss', '2021-02-18 13:21:45', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'OGE0NTkxNGItNzIxZS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_4ffce90319'),
('8c7f7c46-7855-11eb-ab88-5254009623cd', 'a3bf7fee-706c-11eb-8037-04d4c4afee7c', '5d4f6957d5', 1, '<p>Testing<br></p>', '2021-02-26 11:10:38', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'OGM3ZjlmNWEtNzg1NS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_5d4f6957d5'),
('9045ae4a-716b-11eb-ab88-5254009623cd', 'c62577ce-7142-11eb-94a9-04d4c4afee7c', '05cc727953', 1, 'hola!!', '2021-02-17 16:00:36', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'OTA0NjNiZGQtNzE2Yi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_05cc727953'),
('969e3a72-76d4-11eb-ab88-5254009623cd', 'e8d06904-76d1-11eb-ab88-5254009623cd', '97e3277632', 2, '<p>Listo Eddy, ya quedó.&nbsp;</p><p><br></p><p>Muchas gracias</p>', '2021-02-24 13:14:59', '1a623981-76d1-11eb-ab88-5254009623cd', 'OTZhMDU2OTMtNzZkNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_97e3277632'),
('a00acedd-76d2-11eb-ab88-5254009623cd', 'e8d06904-76d1-11eb-ab88-5254009623cd', 'a85df9a460', 2, '<p>Lo estoy revisando desde Safari de una MacBook Air.</p>', '2021-02-24 13:00:55', '1a623981-76d1-11eb-ab88-5254009623cd', 'YTAzMGIwYWYtNzZkMi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_a85df9a460'),
('a098597e-70aa-11eb-8037-04d4c4afee7c', '8db70adf-706c-11eb-8037-04d4c4afee7c', '341c61528d', 1, 'respuesta admin', '2021-02-16 16:59:30', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'YTA5YWE4Y2MtNzBhYS0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'entrada_341c61528d'),
('a0c45680-706e-11eb-8037-04d4c4afee7c', 'fb4d45c6-706d-11eb-8037-04d4c4afee7c', '1aff4378bf', 2, 'entrada de nuevo !', '2021-02-16 09:50:01', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'YTBjNmI4MzUtNzA2ZS0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'entrada_1aff4378bf'),
('a3eaae97-76d3-11eb-ab88-5254009623cd', 'e8d06904-76d1-11eb-ab88-5254009623cd', '2e6cb53dea', 1, '<p>Hola Carlos, buenas tardes.&nbsp;</p><p><br></p><p>Ya quedó resuelto, me ayudas a revisar que todo esté correcto ? Por favor</p><p><br></p><p>Cualquier cosa, estoy al pendiente.&nbsp;</p><p><br></p><p>Saludos.</p><p><br></p><p><br></p><p><br></p><pre>NOTA: En caso de no recibir respuesta, este ticket se cerrará en 72 horas.</pre>', '2021-02-24 13:08:12', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'YTNlY2EwMTAtNzZkMy0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_2e6cb53dea'),
('ad5d27b3-717a-11eb-ab88-5254009623cd', 'eb57629c-7177-11eb-ab88-5254009623cd', 'fd1ff167d0', 1, 'Ddghhh', '2021-02-17 17:48:47', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'YWQ1ZGZhODQtNzE3YS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_fd1ff167d0'),
('b02a75d9-716b-11eb-ab88-5254009623cd', 'c62577ce-7142-11eb-94a9-04d4c4afee7c', 'bedb5e0f7a', 2, 'que paso=??', '2021-02-17 16:01:29', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'YjAyYWQ1ZWUtNzE2Yi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_bedb5e0f7a'),
('b9b05904-7177-11eb-ab88-5254009623cd', '328d7948-7177-11eb-ab88-5254009623cd', '1fa59ce637', 1, 'Respuesta entrada', '2021-02-17 17:27:39', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'YjliMGEzOWQtNzE3Ny0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_1fa59ce637'),
('c53fa288-76c6-11eb-ab88-5254009623cd', '8f030877-76b5-11eb-ab88-5254009623cd', '896a4fdd9e', 1, '<h1>titulo</h1><p><br></p><p>parrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafo.</p><p><br></p><p>paparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafoparrafo</p>', '2021-02-24 11:36:04', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'YzU0MDIxMjAtNzZjNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_896a4fdd9e'),
('c75ead40-76d3-11eb-ab88-5254009623cd', 'e8d06904-76d1-11eb-ab88-5254009623cd', '44d28f232e', 2, '<p>Hola Eddy.</p><p><br></p><p>Sigue sin funcionar !</p>', '2021-02-24 13:09:11', '1a623981-76d1-11eb-ab88-5254009623cd', 'Yzc1ZmRjOGQtNzZkMy0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_44d28f232e'),
('cedd3f5f-7211-11eb-ab88-5254009623cd', 'f594ace3-7210-11eb-ab88-5254009623cd', '079ff56b2a', 1, ' .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .........................................50 .......cierroe.', '2021-02-18 11:50:37', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'Y2VkZDlmZDctNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_079ff56b2a'),
('d0261eb9-76b5-11eb-ab88-5254009623cd', '8f030877-76b5-11eb-ab88-5254009623cd', '6af099ea49', 1, '<h3><b>«La curiosidad mató al gato».</b></h3>\r\n<p>Cuántas veces habremos escuchado esta expresión y cuántas otras me habré cuestionado el por qué de ésta.</p>\r\n<p>La curiosidad, sumada a la prudencia, es el motor de nuestro avance \r\ncomo sociedad y, reduciendo la muestra, de nuestros progresos \r\npersonales. La curiosidad marca la diferencia entre unos y otros: nace \r\ncomo instinto, viaja en la motivación y concluye en la satisfacción de \r\nun aprendizaje.&nbsp;Ella es la protagonista de la sociedad de la información\r\n donde, las tecnologías, nos permiten satisfacer ese deseo de \r\nconocimiento inmediato.</p>\r\n<p>La curiosidad, en su justa medida, beneficia al que la tiene.</p>\r\n<p>Por lo tanto', '2021-02-24 09:34:41', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZDAyNmIzNDYtNzZiNS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_6af099ea49'),
('d21a8f04-714b-11eb-94a9-04d4c4afee7c', 'e8e3f06a-7142-11eb-94a9-04d4c4afee7c', 'db76787278', 1, 'entrada ahora si desde admin', '2021-02-17 12:13:22', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZDIxY2YwODktNzE0Yi0xMWViLTk0YTktMDRkNGM0YWZlZTdj', 'entrada_db76787278'),
('d7271011-76d6-11eb-ab88-5254009623cd', '1a837c88-76b9-11eb-ab88-5254009623cd', 'f29e3e87ae', 1, '<p>A ver</p>', '2021-02-24 13:31:06', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZDcyNzljZjYtNzZkNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_f29e3e87ae'),
('db666eb0-70a7-11eb-8037-04d4c4afee7c', '787c11ad-70a7-11eb-8037-04d4c4afee7c', '687edb462d', 1, 'todo bien ', '2021-02-16 16:39:40', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZGI2OGNkMGMtNzBhNy0xMWViLTgwMzctMDRkNGM0YWZlZTdj', 'entrada_687edb462d'),
('dcac4b66-6fe9-11eb-adea-04d4c4afee7c', 'bbbae463-6cc4-11eb-b74c-04d4c4afee7c', '066cf548cb', 2, 'Ya tiene entrada :)', '2021-02-15 17:59:42', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'ZGNhZmFmMzEtNmZlOS0xMWViLWFkZWEtMDRkNGM0YWZlZTdj', 'entrada_066cf548cb'),
('dfa39846-76d2-11eb-ab88-5254009623cd', 'e8d06904-76d1-11eb-ab88-5254009623cd', '978e93b18e', 1, '<p>Perfecto Carlos, muchas gracias.&nbsp;</p><p><br></p><p>Déjame revisar y te aviso en cuanto quede solucionado.&nbsp;</p><p><br></p><p>Gracias, saludos.</p>', '2021-02-24 13:02:43', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZGZhNDhhNjAtNzZkMi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_978e93b18e'),
('e11d7f2d-0a74-11ec-a3a6-5254009623cd', '6799cb6d-09d3-11ec-a3a6-5254009623cd', 'bdba0fdc64', 1, '<p>prueba<br></p>', '2021-08-31 11:02:45', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZTExZGNkMWMtMGE3NC0xMWVjLWEzYTYtNTI1NDAwOTYyM2Nk', 'entrada_bdba0fdc64'),
('e1238495-720e-11eb-ab88-5254009623cd', 'aac1a7ea-720e-11eb-ab88-5254009623cd', 'ecb542c76b', 1, 'Hola, buenas tardes. \r\n\r\nTu correo ha sido creado con la siguiente información: \r\n\r\n- correo: eddypulido@sondr.mx\r\n- password: eddypulido\r\n\r\nProcedemos a cerrar el caso. Gracias', '2021-02-18 11:29:39', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZTEyNGU1NjktNzIwZS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_ecb542c76b'),
('e20091b5-76d5-11eb-ab88-5254009623cd', 'e8d06904-76d1-11eb-ab88-5254009623cd', '6cad7e5b0d', 1, '<p>Excelente, voy a cerrar ya este ticket de soporte.&nbsp;</p><p><br></p><p>Cualquier cosa, estoy al pendiente &nbsp;</p><p><br></p><p>saludos&nbsp;</p>', '2021-02-24 13:24:15', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZTIwMWI2YmMtNzZkNS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_6cad7e5b0d'),
('e8e9d4e7-720f-11eb-ab88-5254009623cd', '2762ca53-7177-11eb-ab88-5254009623cd', 'ebac2321bc', 1, 'Hola\r\n\r\n123\r\n\r\nSalto', '2021-02-18 11:37:02', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZTkwMjI5ZjEtNzIwZi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_ebac2321bc'),
('f41b3740-7179-11eb-ab88-5254009623cd', 'eb57629c-7177-11eb-ab88-5254009623cd', 'a34503cad8', 1, 'Respuesta dos', '2021-02-17 17:43:36', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'ZjQxYjcyYzAtNzE3OS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_a34503cad8'),
('f57ca2f5-730b-11eb-ab88-5254009623cd', '13c82757-730b-11eb-ab88-5254009623cd', '71469d1992', 2, '<p>hola, voy a responder como cliente</p>', '2021-02-19 17:41:16', '6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'ZjU3Y2Q2ODUtNzMwYi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_71469d1992'),
('f89af81b-76d3-11eb-ab88-5254009623cd', 'e8d06904-76d1-11eb-ab88-5254009623cd', '7531576e80', 1, '<p>Hola Carlos, creo que tal vez puede ser por el caché o las cookies de tu navegador.</p><p><br></p><p>Te mando aquí un PDF con las instrucciones de como eliminarlo, me puedes ayudar a hacer eso y después volver a revisar ? Porfa</p><p><br></p><p>Gracias, saludos.</p>', '2021-02-24 13:10:34', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'Zjg5ZDhlNjUtNzZkMy0xMWViLWFiODgtNTI1NDAwOTYyM2Nk', 'entrada_7531576e80');

-- --------------------------------------------------------

--
-- Table structure for table `tickets_entradas_archivos_c53737c2`
--

CREATE TABLE `tickets_entradas_archivos_c53737c2` (
  `sk_archivo` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `clave_archivo_entrada` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `fk_entrada` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `archivo_entrada` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_archivo_original` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `orden` int(2) NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `hash_acciones` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `tickets_entradas_archivos_c53737c2`
--

INSERT INTO `tickets_entradas_archivos_c53737c2` (`sk_archivo`, `clave_archivo_entrada`, `fk_entrada`, `archivo_entrada`, `nombre_archivo_original`, `orden`, `fecha_alta`, `hash_acciones`) VALUES
('0610f8cc-7524-11eb-ab88-5254009623cd', '1ba2ef5c33', '0608de1c-7524-11eb-ab88-5254009623cd', 'entrada_6033cffa941b5_8ca55ebcb6_2021-02-22_09-38-34.png', '1000x1000.png', 1, '2021-02-22 09:38:34', 'MDYxMjQzN2QtNzUyNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('19e07d51-7524-11eb-ab88-5254009623cd', 'b60123fc69', '19db0a75-7524-11eb-ab88-5254009623cd', 'entrada_6033d01bce334_8d01817f6a_2021-02-22_09-39-07.png', 'blank-1886001_1920.png', 1, '2021-02-22 09:39:07', 'MTllMWRhZGItNzUyNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('19e8c8c7-7524-11eb-ab88-5254009623cd', '29bbf61fd3', '19db0a75-7524-11eb-ab88-5254009623cd', 'entrada_6033d01bdd718_8d01817f6a_2021-02-22_09-39-07.png', 'mockup.png', 2, '2021-02-22 09:39:07', 'MTllYTVmMDAtNzUyNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('19ede387-7524-11eb-ab88-5254009623cd', '6f0bff88da', '19db0a75-7524-11eb-ab88-5254009623cd', 'entrada_6033d01be2826_8d01817f6a_2021-02-22_09-39-07.jpg', 'perfil.jpg', 3, '2021-02-22 09:39:07', 'MTllZWE5ODItNzUyNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('19f15518-7524-11eb-ab88-5254009623cd', 'b99c3954f2', '19db0a75-7524-11eb-ab88-5254009623cd', 'entrada_6033d01be7bdf_8d01817f6a_2021-02-22_09-39-07.jpg', 'perfil2.jpg', 4, '2021-02-22 09:39:07', 'MTlmMWVmM2ItNzUyNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('19f4ca43-7524-11eb-ab88-5254009623cd', 'd27e2e6d20', '19db0a75-7524-11eb-ab88-5254009623cd', 'entrada_6033d01bed58c_8d01817f6a_2021-02-22_09-39-07.png', 'person-3160886_1920.png', 5, '2021-02-22 09:39:07', 'MTlmNTczMDgtNzUyNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('1efc8ee0-76d4-11eb-ab88-5254009623cd', 'da671d8eef', '1ea7d4c0-76d4-11eb-ab88-5254009623cd', 'entrada_6036a4eb356a6_7654177a4f_2021-02-24_13-11-38.pdf', 'Kickoff.pdf', 1, '2021-02-24 13:11:38', 'MWYxZWMyZmYtNzZkNC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('2c1273e2-7211-11eb-ab88-5254009623cd', '3e592d0875', '2c0d5bca-7211-11eb-ab88-5254009623cd', 'entrada_602ea7dc6b0cd_3e9f271828_2021-02-18_11-46-04.png', '9dae3cf338175009d83ad71b38df827e.png', 1, '2021-02-18 11:46:04', 'MmMxMmY4YjAtNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('2c1cfbd3-7211-11eb-ab88-5254009623cd', '667944de37', '2c0d5bca-7211-11eb-ab88-5254009623cd', 'entrada_602ea7dc7c60c_3e9f271828_2021-02-18_11-46-04.png', '670d3c4-es-oxxopay-stub.png', 2, '2021-02-18 11:46:04', 'MmMxZGM3OTItNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('2c2b803d-7211-11eb-ab88-5254009623cd', 'b2135654a8', '2c0d5bca-7211-11eb-ab88-5254009623cd', 'entrada_602ea7dc92f97_3e9f271828_2021-02-18_11-46-04.png', 'mini_magick20190215-29647-grf1kw.png', 3, '2021-02-18 11:46:04', 'MmMyYmVmMGEtNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('2c31aa7e-7211-11eb-ab88-5254009623cd', '54ea31d629', '2c0d5bca-7211-11eb-ab88-5254009623cd', 'entrada_602ea7dc9e4f7_3e9f271828_2021-02-18_11-46-04.jpg', 'maxresdefault.jpg', 4, '2021-02-18 11:46:04', 'MmMzMmVjYWQtNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('2c35cce9-7211-11eb-ab88-5254009623cd', '5a4db1c6b3', '2c0d5bca-7211-11eb-ab88-5254009623cd', 'entrada_602ea7dca4558_3e9f271828_2021-02-18_11-46-04.jpg', 'Mujer Cuello Redondo.jpg', 5, '2021-02-18 11:46:04', 'MmMzNmM2M2ItNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('36caab76-706d-11eb-8037-04d4c4afee7c', '13f1d873c9', '36b8f3af-706d-11eb-8037-04d4c4afee7c', 'entrada_602be749c570b_112e372512_2021-02-16_09-39-53.jpg', 'img.jpg', 1, '2021-02-16 09:39:53', 'MzZjZDA4YzEtNzA2ZC0xMWViLTgwMzctMDRkNGM0YWZlZTdj'),
('587a184d-7211-11eb-ab88-5254009623cd', 'fd88cefd13', '5873fb2c-7211-11eb-ab88-5254009623cd', 'entrada_602ea826e5665_ba9ee914eb_2021-02-18_11-47-18.jpg', 'Sudadera Unisex.jpg', 1, '2021-02-18 11:47:18', 'NTg3YWU0NzYtNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('587d3cf1-7211-11eb-ab88-5254009623cd', 'b99fe7af4e', '5873fb2c-7211-11eb-ab88-5254009623cd', 'entrada_602ea826ea6ce_ba9ee914eb_2021-02-18_11-47-18.png', 'Square.png', 2, '2021-02-18 11:47:18', 'NTg3ZTE2ZDUtNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('58825803-7211-11eb-ab88-5254009623cd', '8ab1c87ae0', '5873fb2c-7211-11eb-ab88-5254009623cd', 'entrada_602ea826f1e5d_ba9ee914eb_2021-02-18_11-47-18.jpg', 'Mujer Cuello Redondo.jpg', 3, '2021-02-18 11:47:18', 'NTg4MmMwOTAtNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('5884a0cc-7211-11eb-ab88-5254009623cd', '8a4f578c40', '5873fb2c-7211-11eb-ab88-5254009623cd', 'entrada_602ea82702f73_ba9ee914eb_2021-02-18_11-47-18.png', 'mini_magick20190215-29647-grf1kw.png', 4, '2021-02-18 11:47:18', 'NTg4NWU2NmItNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('5887f1da-7211-11eb-ab88-5254009623cd', '6f378c5d31', '5873fb2c-7211-11eb-ab88-5254009623cd', 'entrada_602ea827067cd_ba9ee914eb_2021-02-18_11-47-18.jpg', 'maxresdefault.jpg', 5, '2021-02-18 11:47:18', 'NTg4ODM2ZjUtNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('5944e3d3-7139-11eb-94a9-04d4c4afee7c', '9eff164609', '59322410-7139-11eb-94a9-04d4c4afee7c', 'entrada_602d3dc4f0805_ac64b59714_2021-02-17_10-01-08.jpg', 'ailor-moon.jpg', 1, '2021-02-17 10:01:08', 'NTk0NzNkZmYtNzEzOS0xMWViLTk0YTktMDRkNGM0YWZlZTdj'),
('59541902-7139-11eb-94a9-04d4c4afee7c', '8e95fbaa1f', '59322410-7139-11eb-94a9-04d4c4afee7c', 'entrada_602d3dc514abc_ac64b59714_2021-02-17_10-01-08.PNG', 'diseno.PNG', 2, '2021-02-17 10:01:08', 'NTk1Njc3ZDAtNzEzOS0xMWViLTk0YTktMDRkNGM0YWZlZTdj'),
('595d9c5f-7139-11eb-94a9-04d4c4afee7c', '3c28f7cb94', '59322410-7139-11eb-94a9-04d4c4afee7c', 'entrada_602d3dc523f1a_ac64b59714_2021-02-17_10-01-08.jpg', 'mockup.jpg', 3, '2021-02-17 10:01:08', 'NTk2MDAyZjUtNzEzOS0xMWViLTk0YTktMDRkNGM0YWZlZTdj'),
('5965c1c1-7139-11eb-94a9-04d4c4afee7c', '6e85d3fce0', '59322410-7139-11eb-94a9-04d4c4afee7c', 'entrada_602d3dc530f79_ac64b59714_2021-02-17_10-01-08.png', 'pruebadiseno.png', 4, '2021-02-17 10:01:08', 'NTk2ODI3MTItNzEzOS0xMWViLTk0YTktMDRkNGM0YWZlZTdj'),
('596cea08-7139-11eb-94a9-04d4c4afee7c', '23944b14a6', '59322410-7139-11eb-94a9-04d4c4afee7c', 'entrada_602d3dc53c739_ac64b59714_2021-02-17_10-01-08.png', 'Sailor Moon on Moon Anime_thumb[4].png', 5, '2021-02-17 10:01:08', 'NTk2ZjUxYjMtNzEzOS0xMWViLTk0YTktMDRkNGM0YWZlZTdj'),
('61ebddf0-7211-11eb-ab88-5254009623cd', '03f94e3ab9', '61dcf012-7211-11eb-ab88-5254009623cd', 'entrada_602ea836bf16f_f307ac5c10_2021-02-18_11-47-34.jpg', 'Sudadera Unisex.jpg', 1, '2021-02-18 11:47:34', 'NjFlYzY2YjctNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('68e53645-7211-11eb-ab88-5254009623cd', 'c42c840a10', '68d20c4d-7211-11eb-ab88-5254009623cd', 'entrada_602ea84277ed4_3ac9103913_2021-02-18_11-47-46.png', '670d3c4-es-oxxopay-stub.png', 1, '2021-02-18 11:47:46', 'NjhlNmZiYzAtNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('68e9c0f5-7211-11eb-ab88-5254009623cd', 'd67f92b1ec', '68d20c4d-7211-11eb-ab88-5254009623cd', 'entrada_602ea8427e5d3_3ac9103913_2021-02-18_11-47-46.png', '9dae3cf338175009d83ad71b38df827e.png', 2, '2021-02-18 11:47:46', 'NjhlYWZkOTAtNzIxMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('90490977-716b-11eb-ab88-5254009623cd', 'a7c67fdd47', '9045ae4a-716b-11eb-ab88-5254009623cd', 'entrada_602d92041ade9_05cc727953_2021-02-17_16-00-36.jpg', 'perfil2.jpg', 1, '2021-02-17 16:00:36', 'OTA0OTYwZWMtNzE2Yi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('904a8004-716b-11eb-ab88-5254009623cd', '3742a99654', '9045ae4a-716b-11eb-ab88-5254009623cd', 'entrada_602d92041d847_05cc727953_2021-02-17_16-00-36.png', 'person-3160886_1920.png', 2, '2021-02-17 16:00:36', 'OTA0YjA4ODUtNzE2Yi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('b02cee8f-716b-11eb-ab88-5254009623cd', 'abdf99e657', 'b02a75d9-716b-11eb-ab88-5254009623cd', 'entrada_602d923995617_bedb5e0f7a_2021-02-17_16-01-29.jpg', 'perfil.jpg', 1, '2021-02-17 16:01:29', 'YjAyZDFiZjktNzE2Yi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('d029d0f0-76b5-11eb-ab88-5254009623cd', '70fd4bf14d', 'd0261eb9-76b5-11eb-ab88-5254009623cd', 'entrada_60367211d08aa_6af099ea49_2021-02-24_09-34-41.png', 'person-3160886_1920.png', 1, '2021-02-24 09:34:41', 'ZDAyYTkwMGMtNzZiNS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios_35d99c1a`
--

CREATE TABLE `usuarios_35d99c1a` (
  `sk_usuario` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `nombres` varchar(80) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Validar 50 caracteres',
  `paterno` varchar(80) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Validar 50 caracteres',
  `materno` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Validar 50 caracteres',
  `fecha_nacimiento` date DEFAULT NULL,
  `telefono` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Este será el telefono 1 del formulario (obligatorio) ',
  `telefono1` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Este es el telefono de casa (opcional)',
  `telefono2` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `correo` varchar(130) COLLATE utf8_spanish_ci NOT NULL COMMENT 'validar 100 caracteres',
  `password` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `clave_usuario` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `url_restablecer` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '0:inactivo 1: activo 2:eliminado',
  `fecha_alta` datetime NOT NULL,
  `usuario_alta` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `usuario_actualizacion` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  `promociones` int(1) NOT NULL DEFAULT '0',
  `tipo_usuario` int(11) NOT NULL COMMENT '1: Adm 2: Cliente',
  `hash_acciones` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `usuarios_35d99c1a`
--

INSERT INTO `usuarios_35d99c1a` (`sk_usuario`, `nombres`, `paterno`, `materno`, `fecha_nacimiento`, `telefono`, `telefono1`, `telefono2`, `correo`, `password`, `clave_usuario`, `url_restablecer`, `activo`, `fecha_alta`, `usuario_alta`, `fecha_actualizacion`, `usuario_actualizacion`, `promociones`, `tipo_usuario`, `hash_acciones`) VALUES
('1690dcb1-70a8-11eb-8037-04d4c4afee7c', 'Luz María', 'Hernández', 'Aguirre', '1998-04-16', '4422597115', '', '', 'luz@inkwonders.com', '$2a$07$asxx54ahjppf45sd87a5au1mMwPFOiFOa2BiMswhkNpbB7hBZc6pa', '828b7e4b2e', NULL, 0, '2021-02-16 23:41:19', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:30', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 0, 2, 'MTY5MGRjYjEtNzBhOC0xMWViLTgwMzctMDRkNGM0YWZlZTdj'),
('1a623981-76d1-11eb-ab88-5254009623cd', 'Carlos', 'Mercado', '', NULL, '4422797386', '', '', 'carlosm@inkwonders.com', '$2a$07$asxx54ahjppf45sd87a5aub7LdtrTXnn.ZQdALsthndsluPeTbv.a', '4b2e9da614', NULL, 0, '2021-02-24 11:50:02', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:11', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 0, 2, 'MWE2MjM5ODEtNzZkMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 'Eddy', 'Pulido', NULL, '2021-02-04', '4421112233', NULL, NULL, 'eddy@inkwonders.com', '$2y$10$OUZ2vUI702Uz2hJr/3nNBOs4ffrOw4ng1./m3tGkLNgo.w8Laega.', 'bb9aa773aa', NULL, 1, '2021-02-04 18:02:14', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 0, 1, 'NGVhODFiNGUtNjc0NS0xMWViLTkyZWItMDRkNGM0YWZlZTdj'),
('6636e877-67c7-11eb-a2d4-04d4c4afee7c', 'Josué', 'Vargas', 'Martínez', NULL, '1112223355', '', '', 'josue.vargas24@gmail.com', '$2a$07$asxx54ahjppf45sd87a5auvBy6hQgLzY9CNF.lh.oC6Wj7bktoxri', 'b67baa3b6c', NULL, 0, '2021-02-05 09:32:47', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:32', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 0, 2, 'NDk3MjhhZGEtNjdjNy0xMWViLWEyZDQtMDRkNGM0YWZlZTdj'),
('7f72e7de-76d6-11eb-ab88-5254009623cd', 'El', 'Nuevo', '', NULL, '4422797386', '', '', 'eddy@grupo29.mx', '$2a$07$asxx54ahjppf45sd87a5aub7LdtrTXnn.ZQdALsthndsluPeTbv.a', 'a09f8020ce', NULL, 0, '2021-02-24 12:28:39', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:15', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 0, 2, 'N2Y3MmU3ZGUtNzZkNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('7f9c2cd7-7853-11eb-ab88-5254009623cd', 'Andrés', 'Loyola', 'Arana', NULL, '4422260870', '', '', 'andres6522@gmail.com', '$2a$07$asxx54ahjppf45sd87a5aulOrXmdeE2e8uDhFkoxKVnQtOr5TqrXC', '8986e4f5e4', NULL, 1, '2021-02-26 09:55:58', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', NULL, NULL, 0, 2, 'N2Y5YzJjZDctNzg1My0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('aac4a702-76b7-11eb-ab88-5254009623cd', 'Luz', 'María', 'Aguirre', NULL, '4422597115', '', '', 'mariadelaluz.aguirre@gmail.com', '$2a$07$asxx54ahjppf45sd87a5auPogWp34LBfLag6qjGtmBBPb4GJ2g5yu', 'ffc07dc7d8', NULL, 0, '2021-02-24 08:47:58', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:18', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 0, 2, 'YWFjNGE3MDItNzZiNy0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('addb5c01-76b3-11eb-ab88-5254009623cd', 'Alain', 'lemus', 'muñoz', NULL, '5553129371', '', '', 'alain@inkwonders.com', '$2a$07$asxx54ahjppf45sd87a5auPogWp34LBfLag6qjGtmBBPb4GJ2g5yu', 'ac54956308', NULL, 1, '2021-02-24 08:19:25', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-04-12 12:42:19', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 0, 2, 'YWRkYjVjMDEtNzZiMy0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('b527b3b5-720d-11eb-ab88-5254009623cd', 'Eddy', 'Pulido', 'Martín', '0000-00-00', '4422797386', '', '', 'eddy.hb29@gmail.com', '$2a$07$asxx54ahjppf45sd87a5au5fGyv06aS.MkQ0c2aXeU23hwr9SG.Em', '2c7be42b61', NULL, 0, '2021-02-18 10:21:16', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-24 11:52:18', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 0, 2, 'YjUyN2IzYjUtNzIwZC0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('f01520ce-7138-11eb-94a9-04d4c4afee7c', 'Urban ', 'Hangers', 'Community', '1998-04-16', '4422597115', '', '', 'urbanhangers@inkwonders.com', '$2a$07$asxx54ahjppf45sd87a5au.63riCG7WXsuh0gGLoYVhvQW3yhcMqe', '7fd3e66a6a', NULL, 0, '2021-02-17 16:58:12', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', '2021-02-26 09:56:28', '4ea81b4e-6745-11eb-92eb-04d4c4afee7c', 0, 2, 'ZjAxNTIwY2UtNzEzOC0xMWViLTk0YTktMDRkNGM0YWZlZTdj');

-- --------------------------------------------------------

--
-- Table structure for table `usuario_clientes_91caadc1`
--

CREATE TABLE `usuario_clientes_91caadc1` (
  `sk_usuario_cliente` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `fk_usuario` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `fk_cliente` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `hash_acciones` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `usuario_clientes_91caadc1`
--

INSERT INTO `usuario_clientes_91caadc1` (`sk_usuario_cliente`, `fk_usuario`, `fk_cliente`, `clave`, `hash_acciones`) VALUES
('1a649af3-76d1-11eb-ab88-5254009623cd', '1a623981-76d1-11eb-ab88-5254009623cd', 'c9e9efe5-76cf-11eb-ab88-5254009623cd', 'ce92b81edf', 'MWE2NDlhZjMtNzZkMS0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('7f756f6a-76d6-11eb-ab88-5254009623cd', '7f72e7de-76d6-11eb-ab88-5254009623cd', '4dad17ca-76d6-11eb-ab88-5254009623cd', 'ae579f8b4d', 'N2Y3NTZmNmEtNzZkNi0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('7f9f0ce7-7853-11eb-ab88-5254009623cd', '7f9c2cd7-7853-11eb-ab88-5254009623cd', '2f547e55-7782-11eb-ab88-5254009623cd', '86944f380b', 'N2Y5ZjBjZTctNzg1My0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('aac92ed8-76b7-11eb-ab88-5254009623cd', 'aac4a702-76b7-11eb-ab88-5254009623cd', '70ab9305-76b7-11eb-ab88-5254009623cd', '721d49653b', 'YWFjOTJlZDgtNzZiNy0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('adddecf1-76b3-11eb-ab88-5254009623cd', 'addb5c01-76b3-11eb-ab88-5254009623cd', 'e4fc2785-7079-11eb-8037-04d4c4afee7c', 'b95553a1fe', 'YWRkZGVjZjEtNzZiMy0xMWViLWFiODgtNTI1NDAwOTYyM2Nk'),
('f0184602-7138-11eb-94a9-04d4c4afee7c', 'f01520ce-7138-11eb-94a9-04d4c4afee7c', 'd14d34f7-7138-11eb-94a9-04d4c4afee7c', '9113e1960c', 'ZjAxODQ2MDItNzEzOC0xMWViLTk0YTktMDRkNGM0YWZlZTdj');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_usuarios_b7c50848`
-- (See below for the actual view)
--
CREATE TABLE `v_usuarios_b7c50848` (
`id_usuario` varchar(40)
,`nombre_completo_usuario` varchar(242)
,`nombre_usuario` varchar(80)
,`paterno_usuario` varchar(80)
,`materno_usuario` varchar(80)
,`fecha_nacimiento_usuario` date
,`telefono_usuario` varchar(10)
,`telefono1_usuario` varchar(10)
,`telefono2_usuario` varchar(10)
,`correo_usuario` varchar(130)
,`password_usuario` varchar(100)
,`activo_usuario` int(11)
,`url_restablecer_password` varchar(100)
,`promociones` int(11)
,`fecha_alta` datetime
,`usuario_alta` varchar(40)
,`fecha_actualizacion` datetime
,`usuario_actualizacion` varchar(40)
,`tipo_usuario` int(11)
,`no_usuario` varchar(10)
,`hash_usuario` varchar(150)
,`id_cliente` varchar(40)
,`nombre_cliente` varchar(100)
,`activo_cliente` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `v_usuarios_b7c50848`
--
DROP TABLE IF EXISTS `v_usuarios_b7c50848`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_usuarios_b7c50848`  AS  (select `u`.`sk_usuario` AS `id_usuario`,concat_ws(' ',`u`.`nombres`,`u`.`paterno`,`u`.`materno`) AS `nombre_completo_usuario`,`u`.`nombres` AS `nombre_usuario`,`u`.`paterno` AS `paterno_usuario`,`u`.`materno` AS `materno_usuario`,`u`.`fecha_nacimiento` AS `fecha_nacimiento_usuario`,`u`.`telefono` AS `telefono_usuario`,`u`.`telefono1` AS `telefono1_usuario`,`u`.`telefono2` AS `telefono2_usuario`,`u`.`correo` AS `correo_usuario`,`u`.`password` AS `password_usuario`,`u`.`activo` AS `activo_usuario`,`u`.`url_restablecer` AS `url_restablecer_password`,`u`.`promociones` AS `promociones`,`u`.`fecha_alta` AS `fecha_alta`,`u`.`usuario_alta` AS `usuario_alta`,`u`.`fecha_actualizacion` AS `fecha_actualizacion`,`u`.`usuario_actualizacion` AS `usuario_actualizacion`,`u`.`tipo_usuario` AS `tipo_usuario`,`u`.`clave_usuario` AS `no_usuario`,`u`.`hash_acciones` AS `hash_usuario`,`uc`.`fk_cliente` AS `id_cliente`,`c`.`nombre` AS `nombre_cliente`,`c`.`activo` AS `activo_cliente` from ((`usuarios_35d99c1a` `u` join `clientes_30f39860` `c`) join `usuario_clientes_91caadc1` `uc`) where ((`u`.`sk_usuario` = `uc`.`fk_usuario`) and (`u`.`tipo_usuario` = 2) and (`uc`.`fk_usuario` = `u`.`sk_usuario`) and (`uc`.`fk_cliente` = `c`.`sk_cliente`))) union (select `u`.`sk_usuario` AS `id_usuario`,concat_ws(' ',`u`.`nombres`,`u`.`paterno`,`u`.`materno`) AS `nombre_completo_usuario`,`u`.`nombres` AS `nombre_usuario`,`u`.`paterno` AS `paterno_usuario`,`u`.`materno` AS `materno_usuario`,`u`.`fecha_nacimiento` AS `fecha_nacimiento_usuario`,`u`.`telefono` AS `telefono_usuario`,`u`.`telefono1` AS `telefono1_usuario`,`u`.`telefono2` AS `telefono2_usuario`,`u`.`correo` AS `correo_usuario`,`u`.`password` AS `password_usuario`,`u`.`activo` AS `activo_usuario`,`u`.`url_restablecer` AS `url_restablecer_password`,`u`.`promociones` AS `promociones`,`u`.`fecha_alta` AS `fecha_alta`,`u`.`usuario_alta` AS `usuario_alta`,`u`.`fecha_actualizacion` AS `fecha_actualizacion`,`u`.`usuario_actualizacion` AS `usuario_actualizacion`,`u`.`tipo_usuario` AS `tipo_usuario`,`u`.`clave_usuario` AS `no_usuario`,`u`.`hash_acciones` AS `hash_usuario`,NULL AS `id_cliente`,NULL AS `nombre_cliente`,NULL AS `activo_cliente` from `usuarios_35d99c1a` `u` where (`u`.`tipo_usuario` = 1)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes_30f39860`
--
ALTER TABLE `clientes_30f39860`
  ADD PRIMARY KEY (`sk_cliente`),
  ADD KEY `fk_usuario_alta_c` (`usuario_alta`),
  ADD KEY `fk_usuario_actualizacion_c` (`usuario_actualizacion`);

--
-- Indexes for table `datos_ae1a8cc2`
--
ALTER TABLE `datos_ae1a8cc2`
  ADD PRIMARY KEY (`sk_datos`);

--
-- Indexes for table `motivos_bb9aa773`
--
ALTER TABLE `motivos_bb9aa773`
  ADD PRIMARY KEY (`sk_motivo`);

--
-- Indexes for table `proyectos_archivos_49bc6db7`
--
ALTER TABLE `proyectos_archivos_49bc6db7`
  ADD PRIMARY KEY (`sk_archivo`),
  ADD KEY `fk_proyecto_archivos` (`fk_proyecto`);

--
-- Indexes for table `proyectos_f60e66cc`
--
ALTER TABLE `proyectos_f60e66cc`
  ADD PRIMARY KEY (`sk_proyecto`),
  ADD KEY `fk_cliente_proyectos` (`fk_cliente`),
  ADD KEY `fk_usuario_alta_proyectos` (`usuario_alta`),
  ADD KEY `fk_usuario_finalizacion_proyectos` (`usuario_finalizacion`);

--
-- Indexes for table `tickets_47aaef15`
--
ALTER TABLE `tickets_47aaef15`
  ADD PRIMARY KEY (`sk_ticket`),
  ADD KEY `fk_usuario_alta_tickets` (`usuario_alta`),
  ADD KEY `fk_usuario_actualizacion_tickets` (`usuario_actualizacion`),
  ADD KEY `fk_motivo_tickets` (`fk_motivo`),
  ADD KEY `fk_cliente_tickets` (`fk_cliente`);

--
-- Indexes for table `tickets_archivos_cb0a4d09`
--
ALTER TABLE `tickets_archivos_cb0a4d09`
  ADD PRIMARY KEY (`sk_archivo`),
  ADD KEY `fk_ticket_archivos_tickets` (`fk_ticket`);

--
-- Indexes for table `tickets_entradas_09f26b6a`
--
ALTER TABLE `tickets_entradas_09f26b6a`
  ADD PRIMARY KEY (`sk_entrada`),
  ADD KEY `fk_ticket_entrada` (`fk_ticket`),
  ADD KEY `fk_usuario_entrada_ticket` (`usuario_alta`);

--
-- Indexes for table `tickets_entradas_archivos_c53737c2`
--
ALTER TABLE `tickets_entradas_archivos_c53737c2`
  ADD PRIMARY KEY (`sk_archivo`),
  ADD KEY `fk_entrada_archivos_entrada` (`fk_entrada`);

--
-- Indexes for table `usuarios_35d99c1a`
--
ALTER TABLE `usuarios_35d99c1a`
  ADD PRIMARY KEY (`sk_usuario`),
  ADD UNIQUE KEY `correo` (`correo`),
  ADD KEY `fk_usuario_alta_u` (`usuario_alta`),
  ADD KEY `fk_usuario_actualizacion_u` (`usuario_actualizacion`);

--
-- Indexes for table `usuario_clientes_91caadc1`
--
ALTER TABLE `usuario_clientes_91caadc1`
  ADD PRIMARY KEY (`sk_usuario_cliente`),
  ADD KEY `fk_cliente_uc` (`fk_cliente`),
  ADD KEY `fk_usuario_u` (`fk_usuario`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `clientes_30f39860`
--
ALTER TABLE `clientes_30f39860`
  ADD CONSTRAINT `fk_usuario_actualizacion_c` FOREIGN KEY (`usuario_actualizacion`) REFERENCES `usuarios_35d99c1a` (`sk_usuario`),
  ADD CONSTRAINT `fk_usuario_alta_c` FOREIGN KEY (`usuario_alta`) REFERENCES `usuarios_35d99c1a` (`sk_usuario`);

--
-- Constraints for table `proyectos_archivos_49bc6db7`
--
ALTER TABLE `proyectos_archivos_49bc6db7`
  ADD CONSTRAINT `fk_proyecto_archivos` FOREIGN KEY (`fk_proyecto`) REFERENCES `proyectos_f60e66cc` (`sk_proyecto`);

--
-- Constraints for table `proyectos_f60e66cc`
--
ALTER TABLE `proyectos_f60e66cc`
  ADD CONSTRAINT `fk_cliente_proyectos` FOREIGN KEY (`fk_cliente`) REFERENCES `clientes_30f39860` (`sk_cliente`),
  ADD CONSTRAINT `fk_usuario_alta_proyectos` FOREIGN KEY (`usuario_alta`) REFERENCES `usuarios_35d99c1a` (`sk_usuario`),
  ADD CONSTRAINT `fk_usuario_finalizacion_proyectos` FOREIGN KEY (`usuario_finalizacion`) REFERENCES `usuarios_35d99c1a` (`sk_usuario`);

--
-- Constraints for table `tickets_47aaef15`
--
ALTER TABLE `tickets_47aaef15`
  ADD CONSTRAINT `fk_cliente_tickets` FOREIGN KEY (`fk_cliente`) REFERENCES `clientes_30f39860` (`sk_cliente`),
  ADD CONSTRAINT `fk_motivo_tickets` FOREIGN KEY (`fk_motivo`) REFERENCES `motivos_bb9aa773` (`sk_motivo`),
  ADD CONSTRAINT `fk_usuario_actualizacion_tickets` FOREIGN KEY (`usuario_actualizacion`) REFERENCES `usuarios_35d99c1a` (`sk_usuario`),
  ADD CONSTRAINT `fk_usuario_alta_tickets` FOREIGN KEY (`usuario_alta`) REFERENCES `usuarios_35d99c1a` (`sk_usuario`);

--
-- Constraints for table `tickets_archivos_cb0a4d09`
--
ALTER TABLE `tickets_archivos_cb0a4d09`
  ADD CONSTRAINT `fk_ticket_archivos_tickets` FOREIGN KEY (`fk_ticket`) REFERENCES `tickets_47aaef15` (`sk_ticket`);

--
-- Constraints for table `tickets_entradas_09f26b6a`
--
ALTER TABLE `tickets_entradas_09f26b6a`
  ADD CONSTRAINT `fk_ticket_entrada` FOREIGN KEY (`fk_ticket`) REFERENCES `tickets_47aaef15` (`sk_ticket`),
  ADD CONSTRAINT `fk_usuario_entrada_ticket` FOREIGN KEY (`usuario_alta`) REFERENCES `usuarios_35d99c1a` (`sk_usuario`);

--
-- Constraints for table `tickets_entradas_archivos_c53737c2`
--
ALTER TABLE `tickets_entradas_archivos_c53737c2`
  ADD CONSTRAINT `fk_entrada_archivos_entrada` FOREIGN KEY (`fk_entrada`) REFERENCES `tickets_entradas_09f26b6a` (`sk_entrada`);

--
-- Constraints for table `usuarios_35d99c1a`
--
ALTER TABLE `usuarios_35d99c1a`
  ADD CONSTRAINT `fk_usuario_actualizacion_u` FOREIGN KEY (`usuario_actualizacion`) REFERENCES `usuarios_35d99c1a` (`sk_usuario`),
  ADD CONSTRAINT `fk_usuario_alta_u` FOREIGN KEY (`usuario_alta`) REFERENCES `usuarios_35d99c1a` (`sk_usuario`);

--
-- Constraints for table `usuario_clientes_91caadc1`
--
ALTER TABLE `usuario_clientes_91caadc1`
  ADD CONSTRAINT `fk_cliente_uc` FOREIGN KEY (`fk_cliente`) REFERENCES `clientes_30f39860` (`sk_cliente`),
  ADD CONSTRAINT `fk_usuario_u` FOREIGN KEY (`fk_usuario`) REFERENCES `usuarios_35d99c1a` (`sk_usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
