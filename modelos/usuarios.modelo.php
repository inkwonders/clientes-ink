<?php

	require_once "conexion.php";

  	class ModeloUsuarios{  		

	    static public function mdlConsultaUsuario($tabla,$item ,$valor){

	      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = '$valor'");

	      $stmt -> execute();

	      return $stmt -> fetch();

	      $stmt = null;

	    }	
		
		static public function mdlAltaUsuario($tabla, $id_usuario, $u_nombre, $apellidoP, $apellidoM, $fecha_nacimiento, $telefono, $telefono1, $telefono2, $correo, $contrasena, $activo, $clave, $fecha, $tipo_usuario, $hash, $usuario_alta){

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (sk_usuario, nombres, paterno, materno, fecha_nacimiento, telefono, telefono1, telefono2, correo, password, clave_usuario, activo, fecha_alta, usuario_alta, tipo_usuario, hash_acciones) VALUES (:sk_usuario, :nombres, :paterno, :materno, :fecha_nacimiento, :telefono, :telefono1, :telefono2, :correo, :password, :clave_usuario, :activo, :fecha_alta, :usuario_alta, :tipo_usuario, :hash_acciones)");
	
			$stmt->bindParam(":sk_usuario", $id_usuario, PDO::PARAM_STR);
			$stmt->bindParam(":nombres", $u_nombre, PDO::PARAM_STR);
			$stmt->bindParam(":paterno", $apellidoP, PDO::PARAM_STR);		
			$stmt->bindParam(":materno", $apellidoM, PDO::PARAM_STR);
			$stmt->bindParam(":fecha_nacimiento", $fecha_nacimiento, PDO::PARAM_STR);
			$stmt->bindParam(":telefono", $telefono, PDO::PARAM_STR);
			$stmt->bindParam(":telefono1", $telefono1, PDO::PARAM_STR);	
			$stmt->bindParam(":telefono2", $telefono2, PDO::PARAM_STR);	
			$stmt->bindParam(":correo", $correo, PDO::PARAM_STR);	
			$stmt->bindParam(":password", $contrasena, PDO::PARAM_STR);
			$stmt->bindParam(":clave_usuario", $clave, PDO::PARAM_STR);
			$stmt->bindParam(":activo", $activo, PDO::PARAM_STR);
			$stmt->bindParam(":fecha_alta", $fecha, PDO::PARAM_STR);
			$stmt->bindParam(":usuario_alta", $usuario_alta, PDO::PARAM_STR);
			$stmt->bindParam(":tipo_usuario", $tipo_usuario, PDO::PARAM_STR);		
			$stmt->bindParam(":hash_acciones", $hash, PDO::PARAM_STR);		
	
			if ($stmt->execute()) {
	
				return "ok";
	
			} else {
	
				return "error";
	
			}
			
			$stmt = null;
	  
		  }

		static public function mdlRelacionUsuarioCliente($tabla, $sk_usuario_cliente, $id_usuario, $id_cliente, $clave_relacion, $hash_relacion){

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (sk_usuario_cliente, fk_usuario, fk_cliente, clave, hash_acciones) VALUES (:sk_usuario_cliente, :fk_usuario, :fk_cliente, :clave, :hash_acciones)");
	
			$stmt->bindParam(":sk_usuario_cliente", $sk_usuario_cliente, PDO::PARAM_STR);
			$stmt->bindParam(":fk_usuario", $id_usuario, PDO::PARAM_STR);
			$stmt->bindParam(":fk_cliente", $id_cliente, PDO::PARAM_STR);		
			$stmt->bindParam(":clave", $clave_relacion, PDO::PARAM_STR);
			$stmt->bindParam(":hash_acciones", $hash_relacion, PDO::PARAM_STR);		
	
			if ($stmt->execute()) {
	
				return "ok";
	
			} else {
	
				return "error";
	
			}
			
			$stmt = null;
	  
		  }

		  static public function mdlConsultaUsuarios($tabla,$item ,$valor){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = '$valor'");
  
			$stmt -> execute();
  
			return $stmt -> fetchAll();
  
			$stmt = null;
  
		  }	

		  static public function mdlConsultaClienteUsuarios($tabla, $tabla2, $sk_cliente){

			$stmt = Conexion::conectar()->prepare("SELECT nombre FROM $tabla WHERE sk_cliente = (SELECT fk_cliente FROM $tabla2 WHERE fk_usuario = '$sk_cliente')");
  
			$stmt -> execute();
  
			return $stmt -> fetch();
  
			$stmt = null;
  
		  }
		  
		  static public function mdlActualizarRequeridosUsuario($tabla, $u_nombre, $apellidoP, $apellidoM, $telefono, $correo, $sk_usuario, $fecha_nacimiento, $telefono2, $telefono3, $fecha, $usuario_actualizacion){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombres = :nombres, paterno = :paterno, materno = :materno, telefono = :telefono, correo = :correo, fecha_nacimiento = :fecha_nacimiento, telefono1 = :telefono1, telefono2 = :telefono2, fecha_actualizacion = :fecha_actualizacion, usuario_actualizacion = :usuario_actualizacion  WHERE sk_usuario = :sk_usuario");
			$stmt->bindParam(":sk_usuario", $sk_usuario, PDO::PARAM_STR);
			$stmt->bindParam(":nombres", $u_nombre, PDO::PARAM_STR);
			$stmt->bindParam(":paterno", $apellidoP, PDO::PARAM_STR);		
			$stmt->bindParam(":materno", $apellidoM, PDO::PARAM_STR);
			$stmt->bindParam(":telefono", $telefono, PDO::PARAM_STR);	
			$stmt->bindParam(":correo", $correo, PDO::PARAM_STR);
			$stmt->bindParam(":fecha_nacimiento", $fecha_nacimiento, PDO::PARAM_STR);
			$stmt->bindParam(":telefono1", $telefono2, PDO::PARAM_STR);
			$stmt->bindParam(":telefono2", $telefono3, PDO::PARAM_STR);	
			$stmt->bindParam(":fecha_actualizacion", $fecha, PDO::PARAM_STR);	
			$stmt->bindParam(":usuario_actualizacion", $usuario_actualizacion, PDO::PARAM_STR);	
	
			if ($stmt->execute()) {
	
				return "ok";
	
			} else {
	
				return "error";
	
			}
			
			$stmt = null;
	  
		  }

		  static public function mdlActualizarDatosContrasenaUsuario($tabla, $u_nombre, $apellidoP, $apellidoM, $telefono, $correo, $sk_usuario, $fecha_nacimiento, $telefono2, $telefono3, $password, $fecha, $usuario_actualizacion){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombres = :nombres, paterno = :paterno, materno = :materno, telefono = :telefono, correo = :correo, fecha_nacimiento = :fecha_nacimiento, telefono1 = :telefono1, telefono2 = :telefono2, password = :password, fecha_actualizacion = :fecha_actualizacion, usuario_actualizacion = :usuario_actualizacion  WHERE sk_usuario = :sk_usuario");
			$stmt->bindParam(":sk_usuario", $sk_usuario, PDO::PARAM_STR);
			$stmt->bindParam(":nombres", $u_nombre, PDO::PARAM_STR);
			$stmt->bindParam(":paterno", $apellidoP, PDO::PARAM_STR);		
			$stmt->bindParam(":materno", $apellidoM, PDO::PARAM_STR);
			$stmt->bindParam(":telefono", $telefono, PDO::PARAM_STR);	
			$stmt->bindParam(":correo", $correo, PDO::PARAM_STR);
			$stmt->bindParam(":fecha_nacimiento", $fecha_nacimiento, PDO::PARAM_STR);
			$stmt->bindParam(":telefono1", $telefono2, PDO::PARAM_STR);
			$stmt->bindParam(":telefono2", $telefono3, PDO::PARAM_STR);	
			$stmt->bindParam(":password", $password, PDO::PARAM_STR);
			$stmt->bindParam(":fecha_actualizacion", $fecha, PDO::PARAM_STR);	
			$stmt->bindParam(":usuario_actualizacion", $usuario_actualizacion, PDO::PARAM_STR);	
	
			if ($stmt->execute()) {
	
				return "ok";
	
			} else {
	
				return "error";
	
			}
			
			$stmt = null;
	  
		  }

		  static public function mdlStatusUsuario($tabla, $status_usuario, $id_usuario, $fecha, $id_admin){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla  SET activo = :activo, fecha_actualizacion = :fecha_actualizacion, usuario_actualizacion = :usuario_actualizacion WHERE sk_usuario = :sk_usuario");
	
			$stmt->bindParam(":sk_usuario", $id_usuario, PDO::PARAM_STR);
			$stmt->bindParam(":activo", $status_usuario, PDO::PARAM_STR);	
			$stmt->bindParam(":fecha_actualizacion", $fecha, PDO::PARAM_STR);	
			$stmt->bindParam(":usuario_actualizacion", $id_admin, PDO::PARAM_STR);
			if ($stmt->execute()) {
	
				return "ok";
	
			} else {
	
				return "error";
	
			}
			
			$stmt = null;
	  
		  }

		  static public function mdlEliminarUsuario($tabla, $id_usuario){

			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE sk_usuario = :sk_usuario");
	
			$stmt->bindParam(":sk_usuario", $id_usuario, PDO::PARAM_STR);
		
			if ($stmt->execute()) {
	
				return "ok";
	
			} else {
	
				$stmt->debugDumpParams();
	
			}
			
			$stmt = null;
	  
		  }

		  static public function mdlEliminarRelacion($tabla, $id_usuario){

			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE fk_usuario = :fk_usuario");
	
			$stmt->bindParam(":fk_usuario", $id_usuario, PDO::PARAM_STR);
		
			if ($stmt->execute()) {
	
				return "ok";
	
			} else {
	
				$stmt->debugDumpParams();
	
			}
			
			$stmt = null;
	  
		  }
  	}
