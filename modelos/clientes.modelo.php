<?php

require_once "conexion.php";

class ModeloClientes
{

	static public function mdlConsultaClientes($tabla, $tabla2, $condicion){

		$stmt = Conexion::conectar()->prepare("SELECT t1.sk_cliente, t1.nombre AS nombre, t1.fecha_alta AS fecha_alta, t1.activo AS activo, (SELECT COUNT(t2.fk_usuario) FROM $tabla2 AS t2 WHERE t2.fk_cliente = t1.sk_cliente) AS numero_clientes FROM $tabla AS t1 $condicion");
  
		$stmt -> execute();
  
		return $stmt -> fetchAll();
  
		$stmt = null;
  
	  }
	
	static public function mdlConsultaCliente($tabla, $id_cliente){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE sk_cliente = '$id_cliente'");
  
		$stmt -> execute();
  
		return $stmt -> fetch();
  
		$stmt = null;
  
	  }

	static public function mdlUUID(){		

		$stmt = Conexion::conectar()->prepare("SELECT UUID() AS id");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt = null;

	}
	  
	static public function mdlInsertarClientes($tabla, $id_usuario, $nombre_cliente, $activo, $fecha, $usuario_alta, $code, $hash){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (sk_cliente, nombre, activo, fecha_alta, usuario_alta, clave_cliente, hash_acciones) VALUES (:sk_cliente, :nombre, :activo, :fecha_alta, :usuario_alta, :clave_cliente, :hash_acciones)");

		$stmt->bindParam(":sk_cliente", $id_usuario, PDO::PARAM_STR);
		$stmt->bindParam(":nombre", $nombre_cliente, PDO::PARAM_STR);
		$stmt->bindParam(":activo", $activo, PDO::PARAM_STR);		
		$stmt->bindParam(":fecha_alta", $fecha, PDO::PARAM_STR);
		$stmt->bindParam(":usuario_alta", $usuario_alta, PDO::PARAM_STR);
		$stmt->bindParam(":clave_cliente", $code, PDO::PARAM_STR);
		$stmt->bindParam(":hash_acciones", $hash, PDO::PARAM_STR);		

		if ($stmt->execute()) {

			return "ok";

		} else {

			return "error";

		}
		
		$stmt = null;
  
	  }
	
	static public function mdlEditarCliente($tabla, $nombre_cliente_editado, $id_cliente, $id_admin, $fecha){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla  SET nombre = :nombre, fecha_actualizacion = :fecha_actualizacion, usuario_actualizacion = :usuario_actualizacion WHERE sk_cliente = :sk_cliente");

		$stmt->bindParam(":sk_cliente", $id_cliente, PDO::PARAM_STR);
		$stmt->bindParam(":nombre", $nombre_cliente_editado, PDO::PARAM_STR);	
		$stmt->bindParam(":fecha_actualizacion", $fecha, PDO::PARAM_STR);	
		$stmt->bindParam(":usuario_actualizacion", $id_admin, PDO::PARAM_STR);

		if ($stmt->execute()) {

			return "ok";

		} else {

			return "error";

		}
		
		$stmt = null;
  
	  }

	static public function mdlStatusCliente($tabla, $status_cliente, $id_cliente, $fecha, $id_admin){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla  SET activo = :activo, fecha_actualizacion = :fecha_actualizacion, usuario_actualizacion = :usuario_actualizacion WHERE sk_cliente = :sk_cliente");

		$stmt->bindParam(":sk_cliente", $id_cliente, PDO::PARAM_STR);
		$stmt->bindParam(":activo", $status_cliente, PDO::PARAM_STR);	
		$stmt->bindParam(":fecha_actualizacion", $fecha, PDO::PARAM_STR);	
		$stmt->bindParam(":usuario_actualizacion", $id_admin, PDO::PARAM_STR);
		if ($stmt->execute()) {

			return "ok";

		} else {

			return "error";

		}
		
		$stmt = null;
  
	  }

	static public function mdlEliminarCliente($tabla, $id_cliente){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE sk_cliente = :sk_cliente");

		$stmt->bindParam(":sk_cliente", $id_cliente, PDO::PARAM_STR);
	
		if ($stmt->execute()) {

			return "ok";

		} else {

			return "error";

		}
		
		$stmt = null;
  
	  }
}