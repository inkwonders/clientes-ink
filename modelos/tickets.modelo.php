<?php

require_once "conexion.php";

class ModeloTickets
{

	static public function mdlAltaTicket($tabla, $datos)
	{

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (sk_ticket, clave_ticket, fk_cliente, folio, estatus, titulo, fk_motivo, descripcion, calificacion, comentario_calificacion, fecha_calificacion, fecha_alta, usuario_alta, fecha_actualizacion, usuario_actualizacion, hash_acciones, carpeta_ticket) VALUES (:sk_ticket, :clave_ticket, :fk_cliente, :folio, :estatus, :titulo, :fk_motivo, :descripcion, :calificacion, :comentario_calificacion, :fecha_calificacion, :fecha_alta, :usuario_alta, :fecha_actualizacion, :usuario_actualizacion, :hash_acciones, :carpeta_ticket)");

		$stmt->bindParam(":sk_ticket", $datos["sk_ticket"], PDO::PARAM_STR);
		$stmt->bindParam(":clave_ticket", $datos["clave_ticket"], PDO::PARAM_STR);
		$stmt->bindParam(":fk_cliente", $datos["fk_cliente"], PDO::PARAM_STR);
		$stmt->bindParam(":folio", $datos["folio"], PDO::PARAM_INT);
		$stmt->bindParam(":estatus", $datos["estatus"], PDO::PARAM_INT);
		$stmt->bindParam(":titulo", $datos["titulo"], PDO::PARAM_STR);
		$stmt->bindParam(":fk_motivo", $datos["fk_motivo"], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", $datos["descripcion"], PDO::PARAM_STR);
		$stmt->bindParam(":calificacion", $datos["calificacion"], PDO::PARAM_STR);
		$stmt->bindParam(":comentario_calificacion", $datos["comentario_calificacion"], PDO::PARAM_STR);
		$stmt->bindParam(":fecha_calificacion", $datos["fecha_calificacion"], PDO::PARAM_STR);
		$stmt->bindParam(":fecha_alta", $datos["fecha_alta"], PDO::PARAM_STR);
		$stmt->bindParam(":usuario_alta", $datos["usuario_alta"], PDO::PARAM_STR);
		$stmt->bindParam(":fecha_actualizacion", $datos["fecha_actualizacion"], PDO::PARAM_STR);
		$stmt->bindParam(":usuario_actualizacion", $datos["usuario_actualizacion"], PDO::PARAM_STR);
		$stmt->bindParam(":hash_acciones", $datos["hash_acciones"], PDO::PARAM_STR);
		$stmt->bindParam(":carpeta_ticket", $datos["carpeta_ticket"], PDO::PARAM_STR);


		if ($stmt->execute()) {

			return "ok";
		} else {

			return "error";
		}

		$stmt = null;
	}

	static public function mdlAltaArchivosTicket($tabla, $datos)
	{

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (sk_archivo, clave_archivo, fk_ticket, archivo, nombre_archivo_original, orden, fecha_alta, hash_acciones) VALUES (:sk_archivo, :clave_archivo, :fk_ticket, :archivo, :nombre_archivo_original, :orden, :fecha_alta, :hash_acciones)");

		$stmt->bindParam(":sk_archivo", $datos["sk_archivo"], PDO::PARAM_STR);
		$stmt->bindParam(":clave_archivo", $datos["clave_archivo"], PDO::PARAM_STR);
		$stmt->bindParam(":fk_ticket", $datos["fk_ticket"], PDO::PARAM_STR);
		$stmt->bindParam(":archivo", $datos["archivo"], PDO::PARAM_STR);
		$stmt->bindParam(":nombre_archivo_original", $datos["nombre_archivo_original"], PDO::PARAM_STR);
		$stmt->bindParam(":orden", $datos["orden"], PDO::PARAM_INT);
		$stmt->bindParam(":fecha_alta", $datos["fecha_alta"], PDO::PARAM_STR);
		$stmt->bindParam(":hash_acciones", $datos["hash_acciones"], PDO::PARAM_STR);

		if ($stmt->execute()) {

			return "ok";
		} else {

			return "error";
		}

		$stmt = null;
	}

	static public function mdlAltaEntradaTicket($tabla, $datos)
	{

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (sk_entrada, fk_ticket, clave_entrada, tipo_entrada, comentario, fecha_alta, usuario_alta, hash_acciones, carpeta_entrada) VALUES (:sk_entrada, :fk_ticket, :clave_entrada, :tipo_entrada, :comentario, :fecha_alta, :usuario_alta, :hash_acciones, :carpeta_entrada)");

		$stmt->bindParam(":sk_entrada", $datos["sk_entrada"], PDO::PARAM_STR);
		$stmt->bindParam(":fk_ticket", $datos["fk_ticket"], PDO::PARAM_STR);
		$stmt->bindParam(":clave_entrada", $datos["clave_entrada"], PDO::PARAM_STR);
		$stmt->bindParam(":tipo_entrada", $datos["tipo_entrada"], PDO::PARAM_INT);
		$stmt->bindParam(":comentario", $datos["comentario"], PDO::PARAM_STR);
		$stmt->bindParam(":fecha_alta", $datos["fecha_alta"], PDO::PARAM_STR);
		$stmt->bindParam(":usuario_alta", $datos["usuario_alta"], PDO::PARAM_STR);
		$stmt->bindParam(":hash_acciones", $datos["hash_acciones"], PDO::PARAM_STR);
		$stmt->bindParam(":carpeta_entrada", $datos["carpeta_entrada"], PDO::PARAM_STR);

		if ($stmt->execute()) {

			return "ok";
		} else {

			return "error";
		}

		$stmt = null;
	}

	static public function mdlAltaArchivosEntrada($tabla, $datos)
	{

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (sk_archivo, clave_archivo_entrada, fk_entrada, archivo_entrada, nombre_archivo_original, orden, fecha_alta, hash_acciones) VALUES (:sk_archivo, :clave_archivo_entrada, :fk_entrada, :archivo_entrada, :nombre_archivo_original, :orden, :fecha_alta, :hash_acciones)");

		$stmt->bindParam(":sk_archivo", $datos["sk_archivo"], PDO::PARAM_STR);
		$stmt->bindParam(":clave_archivo_entrada", $datos["clave_archivo_entrada"], PDO::PARAM_STR);
		$stmt->bindParam(":fk_entrada", $datos["fk_entrada"], PDO::PARAM_STR);
		$stmt->bindParam(":archivo_entrada", $datos["archivo_entrada"], PDO::PARAM_STR);
		$stmt->bindParam(":nombre_archivo_original", $datos["nombre_archivo_original"], PDO::PARAM_STR);
		$stmt->bindParam(":orden", $datos["orden"], PDO::PARAM_INT);
		$stmt->bindParam(":fecha_alta", $datos["fecha_alta"], PDO::PARAM_STR);
		$stmt->bindParam(":hash_acciones", $datos["hash_acciones"], PDO::PARAM_STR);

		if ($stmt->execute()) {

			return "ok";
		} else {

			return "error";
		}

		$stmt = null;
	}

	static public function mdlConsulta($tabla, $condicion)
	{

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla $condicion");

		$stmt->execute();

		return $stmt->fetchAll();

		$stmt = null;
	}

	static public function mdlUUID()
	{

		$stmt = Conexion::conectar()->prepare(" SELECT UUID() AS id ");

		$stmt->execute();

		return $stmt->fetch();

		$stmt = null;
	}

	static public function mdlConsultaMotivo($tabla, $campo, $valor)
	{

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $campo = :$campo");

		$stmt->bindParam(":$campo", $valor, PDO::PARAM_STR);

		$stmt->execute();

		return $stmt->fetch();

		$stmt = null;
	}

	static public function mdlConsultaTotalTickets()
	{

		$stmt = Conexion::conectar()->prepare("SELECT count(*) AS total FROM tickets_47aaef15");

		$stmt->execute();

		return $stmt->fetch();

		$stmt = null;
	}

	static public function mdlBorrarTodo($tabla, $campo, $valor)
	{

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE $campo = '$valor'");

		if ($stmt->execute()) {

			return 'ok';
		} else {

			return 'error';
		}

		$stmt = null;
	}

	static public function mdlConsultaEntrada($tabla, $campo, $valor, $extra)
	{

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $campo = :$campo $extra");

		$stmt->bindParam(":$campo", $valor, PDO::PARAM_STR);

		$stmt->execute();

		return $stmt->fetch();

		$stmt = null;
	}

	static public function mdlConsultaCantidadTickets()
	{

		$stmt = Conexion::conectar()->prepare("SELECT count(*) AS total, count(case when calificacion = 1 then 1 else null end) AS positivos, count(case when calificacion = 2 then 1 else null end) AS negativos, count(case when estatus != 4 then 1 else null end) AS abiertos,count(case when estatus = 4 then 1 else null end) AS cerrados FROM tickets_47aaef15");

		$stmt->execute();

		return $stmt->fetch();

		$stmt = null;
	}

	static public function mdlConsultaTicketExistente($no_ticket)
	{

		$stmt = Conexion::conectar()->prepare("SELECT * FROM tickets_47aaef15 WHERE clave_ticket = '$no_ticket'");

		$stmt->execute();

		return $stmt->fetch();

		$stmt = null;
	}

	static public function mdlConsultaArchivos($tabla, $campo, $valor, $extra)
	{

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $campo = :$campo $extra");

		$stmt->bindParam(":$campo", $valor, PDO::PARAM_STR);

		$stmt->execute();

		return $stmt->fetchAll();

		$stmt = null;
	}

	static public function mdlActualiza($tabla, $query)
	{			

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $query");

		if ($stmt->execute()) {

			return 'ok';
		} else {

			return 'error';
		}

		$stmt = null;
	}
}
