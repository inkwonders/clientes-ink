<?php

require_once "conexion.php";

class ModeloProyectos
{

	static public function mdlMostrarProyectos($tabla, $condicion){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla $condicion ORDER BY fecha_alta ASC");
  
		$stmt -> execute();
  
		return $stmt -> fetchAll();
  
		$stmt = null;
  
	  }

	static public function mdlAltaProyecto($tabla, $datos)
	{

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (sk_proyecto, clave_proyecto, fk_cliente, estatus, titulo, descripcion, fecha_alta, usuario_alta, fecha_finalizacion, usuario_finalizacion, hash_acciones) VALUES (:sk_proyecto, :clave_proyecto, :fk_cliente, :estatus, :titulo, :descripcion, :fecha_alta, :usuario_alta, :fecha_finalizacion, :usuario_finalizacion, :hash_acciones)");

		$stmt->bindParam(":sk_proyecto", $datos["sk_proyecto"], PDO::PARAM_STR);
		$stmt->bindParam(":clave_proyecto", $datos["clave_proyecto"], PDO::PARAM_STR);
		$stmt->bindParam(":fk_cliente", $datos["fk_cliente"], PDO::PARAM_STR);
		$stmt->bindParam(":estatus", $datos["estatus"], PDO::PARAM_INT);
		$stmt->bindParam(":titulo", $datos["titulo"], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", $datos["descripcion"], PDO::PARAM_STR);
		$stmt->bindParam(":fecha_alta", $datos["fecha_alta"], PDO::PARAM_STR);
		$stmt->bindParam(":usuario_alta", $datos["usuario_alta"], PDO::PARAM_STR);
		$stmt->bindParam(":fecha_finalizacion", $datos["fecha_finalizacion"], PDO::PARAM_STR);
		$stmt->bindParam(":usuario_finalizacion", $datos["usuario_finalizacion"], PDO::PARAM_STR);
		$stmt->bindParam(":hash_acciones", $datos["hash_acciones"], PDO::PARAM_STR);


		if ($stmt->execute()) {

			return "ok";
		} else {

			$stmt->debugDumpParams();
		}

		$stmt = null;
	}

	static public function mdlAltaArchivosProyecto($tabla, $datos)
	{

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (sk_archivo, clave_archivo, fk_proyecto, archivo, nombre_archivo_original, orden, fecha_alta, hash_acciones) VALUES (:sk_archivo, :clave_archivo, :fk_proyecto, :archivo, :nombre_archivo_original, :orden, :fecha_alta, :hash_acciones)");

		$stmt->bindParam(":sk_archivo", $datos["sk_archivo"], PDO::PARAM_STR);
		$stmt->bindParam(":clave_archivo", $datos["clave_archivo"], PDO::PARAM_STR);
		$stmt->bindParam(":fk_proyecto", $datos["fk_proyecto"], PDO::PARAM_STR);
		$stmt->bindParam(":archivo", $datos["archivo"], PDO::PARAM_STR);
		$stmt->bindParam(":nombre_archivo_original", $datos["nombre_archivo_original"], PDO::PARAM_STR);
		$stmt->bindParam(":orden", $datos["orden"], PDO::PARAM_INT);
		$stmt->bindParam(":fecha_alta", $datos["fecha_alta"], PDO::PARAM_STR);
		$stmt->bindParam(":hash_acciones", $datos["hash_acciones"], PDO::PARAM_STR);

		if ($stmt->execute()) {

			return "ok";
		} else {

			return "error";
		}

		$stmt = null;
	}
	static public function mdlConsultaArchivos($tabla, $campo, $valor, $extra)
	{

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $campo = :$campo $extra");

		$stmt->bindParam(":$campo", $valor, PDO::PARAM_STR);

		$stmt->execute();

		return $stmt->fetchAll();

		$stmt = null;
	}

	static public function mdlConsultaProyecto($tabla, $campo1, $campo2, $valor1, $valor2)
	{
		
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $campo1 = :$campo1 AND $campo2 = :$campo2");

		$stmt->bindParam(":$campo1", $valor1, PDO::PARAM_STR);
		$stmt->bindParam(":$campo2", $valor2, PDO::PARAM_STR);

		$stmt->execute();

		return $stmt->fetch();

		$stmt = null;

	}


}
