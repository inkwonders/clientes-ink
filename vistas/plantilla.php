<?php

session_start();

$ruta_global = Rutas::ctrRuta();

require_once "lib/funciones.php";

?>

<!DOCTYPE html>
<html lang="es">

<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Last-Modified" content="0">
	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta http-equiv="Pragma" content="no-cache">

	<title>INK Wonders | Soporte</title>

	<!-- favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $ruta_global; ?>vistas/assets/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $ruta_global; ?>vistas/assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $ruta_global; ?>vistas/assets/favicon/favicon-16x16.png">
	<link rel="mask-icon" href="<?php echo $ruta_global; ?>vistas/assets/favicon/safari-pinned-tab.svg" color="#000000">
	<meta name="msapplication-TileColor" content="#fffcfb">
	<meta name="theme-color" content="#ffffff">

	<!-- librerias -->
	<script src="<?php echo $ruta_global; ?>vistas/assets/js/jquery-3.5.1.min.js"></script>
	<!--<link type="text/css" rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/materialize.min.css"  media="screen,projection"/>-->
	<link rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/AdminLTE-3.0.5/plugins/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/AdminLTE-3.0.5/dist/css/adminlte.min.css">
	<link rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/AdminLTE-3.0.5/plugins/select2/css/select2.css">
	<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
	<!-- fin lebrerias -->

	<!-- cdn's -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css">
	<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> -->
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<!-- fin cdn's -->
	<!-- web manifest-->
	<link rel="manifest" href="<?php echo $ruta_global; ?>manifest.webmanifest">
	<!-- estilos -->
	<link rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/estilos_generales.css?v=<?php echo time(); ?>">
	<link rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/estilos_login.css?v=<?php echo time(); ?>">
	<link rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/estilos_aside.css?v=<?php echo time(); ?>">
	<link rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/estilos_tickets.css?v=<?php echo time(); ?>">
	<link rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/estilos_nuevo_caso.css?v=<?php echo time(); ?>">
	<link rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/estilos_ticket.css?v=<?php echo time(); ?>">
	<link rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/estilos_clientes.css?v=<?php echo time(); ?>">
	<link rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/estilos_404.css?v=<?php echo time(); ?>">
	<link rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/estilos_proyectos.css?v=<?php echo time(); ?>">
	<!-- fin estilos -->

	<!-- librerias js -->
	<script src="<?php echo $ruta_global; ?>vistas/assets/AdminLTE-3.0.5/plugins/jquery/jquery.min.js"></script>
	<script src="<?php echo $ruta_global; ?>vistas/assets/AdminLTE-3.0.5/plugins/select2/js/select2.js"></script>

	<script src="<?php echo $ruta_global; ?>vistas/assets/AdminLTE-3.0.5/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo $ruta_global; ?>vistas/assets/AdminLTE-3.0.5/dist/js/adminlte.min.js"></script>
	<script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
	<!-- fin librerias js -->

	<!-- include summernote css/js-->	
	<link href="<?php echo $ruta_global; ?>vistas/assets/AdminLTE-3.0.5/plugins/summernote/summernote.css" rel="stylesheet">
	<script src="<?php echo $ruta_global; ?>vistas/assets/AdminLTE-3.0.5/plugins/summernote/summernote.js"></script>
	<script src="<?php echo $ruta_global; ?>vistas/assets/AdminLTE-3.0.5/plugins/summernote/lang/summernote-es-ES.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/pace-js@latest/pace-theme-default.min.css">

</head>

<body class="hold-transition sidebar-mini" ondragstart="return false;" ondrop="return false;">

	<?php

	if (isset($_GET["pagina"])) {

		$rutas = explode("/", $_GET["pagina"]);
		$tam_rutas = sizeof($rutas);

		if (isset($_SESSION["sesion_activa"]) && $_SESSION["sesion_activa"] == 1) {

			include "vistas/secciones/header.php";
			include "vistas/secciones/aside.php";
			if ($rutas[0] == "tickets"){
				include "vistas/modulos/" . $rutas[0] . ".php";
			}else if($rutas[0] == "nuevo-ticket"){
				if(isset($_SESSION["tipo_usuario"]) && $_SESSION["tipo_usuario"] == 2){
					include "vistas/modulos/" . $rutas[0] . ".php";
				}else{
					include "vistas/secciones/404.php";
				}
			}else if($rutas[0] == "ticket"){
				include "vistas/modulos/" . $rutas[0] . ".php";
			}else if($rutas[0] == "clientes"){
				if(isset($_SESSION["tipo_usuario"]) && $_SESSION["tipo_usuario"] == 1){
					include "vistas/modulos/" . $rutas[0] . ".php";
				}else{
					include "vistas/secciones/404.php";
				}
			}else if($rutas[0] == "usuarios"){
				if(isset($_SESSION["tipo_usuario"]) && $_SESSION["tipo_usuario"] == 1){
					include "vistas/modulos/" . $rutas[0] . ".php";
				}else{
					include "vistas/secciones/404.php";
				}
			}else if($rutas[0] == "proyectos"){
				include "vistas/modulos/" . $rutas[0] . ".php";
			} else if ($rutas[0] === "salir") {
				include "vistas/modulos/salir.php";
			} else if ($rutas[0] === "login") {
				echo '<script type="text/javascript">
					 		location.href = "' . $ruta_global . 'tickets";
						</script>';
			} else if ($rutas[0] === "404") {
				include "vistas/secciones/404.php";
			} else {
				include "vistas/secciones/404.php";
			}
		} else if ($rutas[0] != 'login') { // no tiene sesion y no existe la ruta

			echo '<script type="text/javascript">
					 location.href = "' . $ruta_global . 'login";
				</script>';
		} else if ($rutas[0] === 'login') { // se fue al login
			if ($tam_rutas > 1) {
				echo '<script type="text/javascript">
							location.href = "' . $ruta_global . 'login";
							 </script>';
			} else {
				include "vistas/modulos/login.php";
			}
		}
	} else {
		echo '<script type="text/javascript">
						location.href = "' . $ruta_global . 'login";
				</script>';
	}

	?>

</body>

</html>

<!-- script's -->

<script>
	var ruta_global = "<?php echo $ruta_global; ?>";
</script>

<script src="<?php echo $ruta_global; ?>vistas/assets/js/script.js"></script>
<script src="<?php echo $ruta_global; ?>vistas/assets/js/aside_script.js"></script>
<scrip src="<?php echo $ruta_global; ?>pwabuilder-sw-register.js"></script>
<scrip src="<?php echo $ruta_global; ?>pwabuilder-sw.js"></script>

		<!-- fin script's -->