<div class="wrapper main-sidebar sidebar-dark-primary elevation-4">
    <aside class="main-sidebar sidebar-dark-primary elevation-4">

        <!-- Brand Logo -->
        <?php if($_SESSION['tipo_usuario']==1) { ?>
        <a href="" class="brand-link">
            <img src="<?php echo $ruta_global; ?>vistas/assets/img/logo.png"
            alt="INKWONDERS"
            class="brand-image img-circle elevation-3"
            style="opacity: .8">
            <span class="brand-text font-weight-light">INKWONDERS</span>
        </a>
        <?php }else{ 
            $id_cliente = $_SESSION['id_cliente'];
            $consulta_cliente = ControladorUsuarios::ctrConsultaUsuario('clientes_30f39860','sk_cliente',$id_cliente);
            ?>
            <div class="brand-link">
                <span class="brand-text font-weight-light" title="<?php echo $consulta_cliente["nombre"]; ?>"><?php echo $consulta_cliente["nombre"]; ?></span>
        </div>

        <?php } ?>

        <div class="sidebar">

            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- <li class="nav-item has-treeview menu-open"> -->

                        <?php
                        if($_SESSION['tipo_usuario']==1) { 
                            ?>                            
                            <li class="nav-item has-treeview menu-open">
                            <ul class="nav nav-treeview">
                                <li class="nav-item" id="asside_dashboard">
                                    <a href="<?php echo $ruta_global;?>tickets" class="nav-link">
                                        <i class="nav-icon fas fa-tachometer-alt"></i>
                                        <p><?php echo ($_SESSION['tipo_usuario'] == 1) ? "Tickets" : " Mis Tickets"; ?></p>
                                        <?php
                                        $fecha = date("Y-m-d");
                                        $condicion_admin = " estatus != 4 AND estatus != 2";
                                        $consulta_tickets_admin = ControladorNotificaciones::ctrTicketNotificacion('tickets_47aaef15', $condicion_admin);
                                        if($consulta_tickets_admin['total']>0){
                                        ?>
                                        <span class="right badge badge-danger"><?php echo $consulta_tickets_admin['total']; ?></span>
                                        <?php } ?>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item" id="asside_clientes">
                                    <a href="<?php echo $ruta_global;?>clientes" class="nav-link">
                                        <i class="nav-icon fas fas fa-user-friends"></i>
                                        <p>Clientes</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item" id="asside_usuarios">
                                    <a href="<?php echo $ruta_global;?>usuarios" class="nav-link">
                                        <i class="nav-icon fas fas fa-user-friends"></i>
                                        <p>Usuarios</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item" id="asside_proyectos">
                                    <a href="<?php echo $ruta_global;?>proyectos" class="nav-link">
                                        <i class="nav-icon fas fas fa-file"></i>
                                        <p>Proyectos</p>
                                    </a>
                                </li>
                            </ul>

                </ul>
                <?php
            }else if($_SESSION['tipo_usuario']==2){ ?>
            <li class="nav-item has-treeview menu-open">                
                        <ul class="nav nav-treeview">
                            <li class="nav-item" id="asside_dashboard">
                                <a href="<?php echo $ruta_global;?>tickets" class="nav-link">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p><?php echo ($_SESSION['tipo_usuario'] == 1) ? "Tickets" : " Mis Tickets"; ?></p>
                                    <?php
                                        $fecha = date("Y-m-d");
                                        $condicion_cliente = "fk_cliente = '".$_SESSION['id_cliente']."' AND estatus = 2";
                                        $consulta_tickets_cliente = ControladorNotificaciones::ctrTicketNotificacion('tickets_47aaef15', $condicion_cliente);
                                        if($consulta_tickets_cliente['total']>0){
                                        ?>
                                        <span class="right badge badge-danger"><?php echo $consulta_tickets_cliente['total']; ?></span>
                                        <?php } ?>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav nav-treeview">
                            <li class="nav-item" id="asside_nuevo_ticket">
                                <a href="<?php echo $ruta_global;?>nuevo-ticket" class="nav-link">
                                    <i class="nav-icon fas fa-ticket-alt"></i>
                                    <p>Nuevo ticket</p>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav nav-treeview">
                            <li class="nav-item" id="asside_proyectos">
                                <a href="<?php echo $ruta_global;?>proyectos" class="nav-link">
                                    <i class="nav-icon fas fas fa-file"></i>
                                    <p>Proyectos</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <?php
                }
                ?>



        </li>
    </ul>
</nav>

</div>
<div class="version">
    <p>V 1.0.0</p>
</div>

</aside>
</div>
