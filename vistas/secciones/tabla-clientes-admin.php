<table class="tabla_casos2">
    <thead>
        <tr class="tabla_head">
            <th class="fila_1 oculto">
                #
            </th>
            <th class="fila_2">
                Nombre de cliente
            </th>
            <th class="fila_3">
                Número de usuarios
            </th>
            <th class="fila_4">
                Fecha de alta
            </th>
            <th class="fila_5">
                Activo
            </th>
            <th class="fila_6">
                Acciones
            </th>
        </tr>
    </thead>
    <tbody id="lista_casos" class="tabla_body scroll_cumbres">
        <?php
        $contador_clientes = 0;
        $consulta_clientes = Clientes::ctrConsultaClientes('clientes_30f39860', 'usuario_clientes_91caadc1', '');   
        foreach ($consulta_clientes as $key => $clientes) {   
            $contador_clientes++;  
        ?>
        <tr class="tabla_row odd" role="row">
            <td class="fila_1 oculto">
                <span><?php echo $contador_clientes; ?></span>
            </td>
            <td class="fila_2">
                <span class="tipo_nuevo"><?php echo $clientes['nombre']; ?></span>
            </td>
            <td class="fila_3">
                <span><?php echo $clientes['numero_clientes']; ?></span>
            </td>
            <td class="fila_4">
                <span><?php echo mostrar_fecha_formato($clientes['fecha_alta']); ?></span>
            </td>
            <td class="fila_5">
            <?php
            if($clientes['activo'] == 1){
                echo "<input type='checkbox' class='btn_status_cliente' key='".base64_encode($clientes['sk_cliente'])."' key2='".base64_encode($_SESSION['id_usuario'])."' checked data-toggle='toggle' data-on='Activo' data-off='Inactivo' data-onstyle='success' data-offstyle='danger'>";
            }else{
                echo "<input type='checkbox' class='btn_status_cliente' key='".base64_encode($clientes['sk_cliente'])."' key2='".base64_encode($_SESSION['id_usuario'])."' data-toggle='toggle' data-on='Activo' data-off='Inactivo' data-onstyle='success' data-offstyle='danger'>";
            }
            ?>
            
            </td>
            <td class="fila_6 ec_acciones_usuarios">
            <!--boton para agregar usuario-->
                &nbsp;&nbsp;
            <?php if($clientes['numero_clientes']==0){ ?>
            <button type="button" class="btn btn-info ec_btnacciones c_agregar_usuario" key="<?php echo base64_encode($clientes['sk_cliente']);?>" data-toggle="modal" data-target="#agregarUsuario"><i class="fas fa-user-plus"></i></button>
            <?php } ?>
            <!--boton para editar cliente-->
            <button type="button" class="btn btn-secondary ec_btnacciones c_editar_cliente" data-toggle="modal" data-target="#editarCliente" nombreCliente="<?php echo $clientes["nombre"]; ?>" key="<?php echo base64_encode($clientes['sk_cliente']);?>" key2="<?php echo base64_encode($_SESSION['id_usuario']); ?>"><i class="fas fa-user-edit"></i></button>
            <!--boton para eliminar-->
            
            </td>
        </tr>
        <?php } ?> 

    </tbody>
</table>
