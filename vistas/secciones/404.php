    <div class="content-wrapper">
      <div class="content">
        <div class="contenido404">
          <span class="titulo404">404</span><br />
          <span class="texto404">Lo sentimos, no se encontraron resultados...</span><br /><br />
          <div class="button404" onclick="window.history.back();">
            <!-- <i class="fas fa-long-arrow-alt-left"></i> -->
            <i class="fas fa-angle-left fa-2x mr_10_404"></i>
            <span>REGRESAR</span>
          </div>
        </div>
      </div>
    </div>
