<table class="tabla_casos">
    <thead>
        <tr class="tabla_head">
            <th class="fila_1">
                #
            </th>
            <th class="fila_2">
                Nombre del proyecto
            </th>
            <th class="fila_3">
                Cliente
            </th>
            <th class="fila_4">
                Fecha de creación 
            </th>
            <th class="fila_6">
                Acciones
            </th>
        </tr>
    </thead>
    <tbody id="lista_casos" class="tabla_body scroll_cumbres">

    <?php
    if ($_SESSION['tipo_usuario'] == 1) {
    $consulta_proyectos = ControladorProyectos::ctrMostrarProyectos('proyectos_f60e66cc', '');
    }else{
        $id_cliente = $_SESSION['id_cliente'];
        $consulta_proyectos = ControladorProyectos::ctrMostrarProyectos("proyectos_f60e66cc", "WHERE fk_cliente = '$id_cliente'");
    }
    
    $contador = 0;
    foreach ($consulta_proyectos as $key => $proyectos) {
        $contador++;
        $fk_cliente = $proyectos["fk_cliente"];
        $cliente = Clientes::ctrConsultaCliente('clientes_30f39860', $fk_cliente);
    ?>
            <tr class="tabla_row odd" role="row">
                <td class="fila_1 sorting_1">
                    <span><?php echo $contador; ?></span>
                </td>
                <td class="fila_2">
                    <span><?php echo $proyectos["titulo"]; ?></span>
                </td>
                <td class="fila_3">
                    <span><?php echo $cliente["nombre"]; ?></span>
                </td>
                <td class="fila_4">
                    <span><?php echo mostrar_fecha_formato($proyectos["fecha_alta"]); ?></span>
                </td>
                <td class="fila_6">
                    <div class="cont_centrado">
                        <?php
                        $id_proyecto = $proyectos['sk_proyecto'];
                        $consulta_archivos_entrada = ControladorProyectos::ctrConsultaArchivos('proyectos_archivos_49bc6db7', 'fk_proyecto', $id_proyecto, '');
                        $numero_archivos = sizeof($consulta_archivos_entrada);
                        foreach ($consulta_archivos_entrada as $key => $valueArchivosEntrada) :
                            $nombre[] = $valueArchivosEntrada["archivo"];
                            $nombre_original[] = $valueArchivosEntrada["nombre_archivo_original"];
                        endforeach;
                        ?>
                        <!-- <i class="fas fa-search cont_centrado tamano_iconos c_pointer btn_ver_mas_proyecto" title="Ver más" nombre="<?php echo $proyectos["titulo"]; ?>" cliente="<?php echo $cliente["nombre"]; ?>" descripcion="<?php echo base64_encode($proyectos["descripcion"]); ?>" fecha="<?php echo mostrar_fecha_formato($proyectos["fecha_alta"]); ?>" numero_archivos="<?php echo $numero_archivos; ?>" <?php for($i=0; $i<$numero_archivos; $i++): echo"archivo".$i."="."'".$nombre[$i]."$".$nombre_original[$i]."'"; endfor; unset($nombre); unset($nombre_original);?>></i> -->
                        <i class="fas fa-search cont_centrado tamano_iconos c_pointer btn_ver_mas_proyecto" title="Ver más" no="<?php echo $proyectos["clave_proyecto"]; ?>" hash="<?php echo base64_encode($proyectos["hash_acciones"]); ?>" cliente="<?php echo $cliente["nombre"]; ?>"></i>
                    </div>
                </td>
            </tr>
<?php } ?>
    </tbody>
</table>
