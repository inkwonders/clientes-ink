<?php

$consulta_cantidad_tickets = ControladorTickets::ctrConsultaCantidadTickets();
$positivos = $consulta_cantidad_tickets['positivos'];
$negativos = $consulta_cantidad_tickets['negativos'];
$abiertos = $consulta_cantidad_tickets['abiertos'];
$cerrados = $consulta_cantidad_tickets['cerrados'];
$total_casos = $consulta_cantidad_tickets['total'];
?>
<script type="text/javascript">
    var positivos = <?php echo $positivos; ?>;
    var negativos = <?php echo $negativos; ?>;
    var abiertos = <?php echo $abiertos; ?>;
    var cerrados = <?php echo $cerrados; ?>;
    var total_casos = <?php echo $total_casos; ?>;
</script>
<div class="tabla_estadistica">
    <div class="columna_est">
        <!-- <span class="titulo_tabla">REPORTES</span> -->
        <div class="divisor_container_tabla hide-on-small-only"></div>
        <div class="sub_tabla">
            <div class="row_sup">
                <div class="col_1">
                    <span class="c_cuadro c_positivos">Número de casos calificados positivo</span>
                </div>
                <div class="col_2">
                    <span><?php echo $positivos; ?></span>
                </div>
            </div>
            <div class="row_sup">
                <div class="col_1">
                    <span class="c_cuadro c_negativo">Número de casos calificados negativo</span>
                </div>
                <div class="col_2">
                    <span><?php echo $negativos; ?></span>
                </div>
            </div>
            <div class="row_sup">
                <div class="col_1">
                    <span class="c_cuadro c_abiertos">Número de casos abiertos</span>
                </div>
                <div class="col_2">
                    <span><?php echo $abiertos; ?></span>
                </div>
            </div>
            <div class="row_sup">
                <div class="col_1">
                    <span class="c_cuadro c_cerrados">Número de casos cerrados</span>
                </div>
                <div class="col_2">
                    <span><?php echo $cerrados; ?></span>
                </div>
            </div>
            <div class="row_sup">
                <div class="col_1">
                    <span>Número de casos TOTAL</span>
                </div>
                <div class="col_2">
                    <span><?php echo $total_casos; ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="columna_est">
        <div id="contenido_grafica"></div>
    </div>
</div>

<div class="tickets">
    <table class="tabla_tickets">
        <thead>
            <tr class="tabla_head">
                <th class="fila_1">
                    #
                </th>
                <th class="fila_2">
                    Estatus
                </th>
                <th class="fila_3">
                    Título
                </th>
                <th class="fila_4">
                    Tema
                </th>
                <th class="fila_5">
                    Fecha última respuesta
                </th>
                <th class="fila_6">
                    Usuario última respuesta
                </th>
                <th class="fila_7">
                    Cliente
                </th>
                <th class="fila_8">
                    Ver
                </th>
            </tr>
        </thead>
        <tbody id="lista_casos" class="tabla_body">
            <?php

            $tabla_consulta = 'tickets_47aaef15';
            $condicion_consulta = "ORDER BY folio DESC";

            $consulta_tickets = ControladorTickets::ctrConsulta($tabla_consulta, $condicion_consulta);

            foreach ($consulta_tickets as $key => $valueTicket) {

                $id_ticket = $valueTicket['sk_ticket'];
                $clave_ticket = $valueTicket['clave_ticket'];
                $folio = $valueTicket['folio'];
                $estatus = $valueTicket['estatus'];
                $titulo = $valueTicket['titulo'];
                $sk_motivo = $valueTicket['fk_motivo'];
                $calificacion = $valueTicket['calificacion'];
                $id_usuario_alta = $valueTicket['usuario_alta'];
                $fecha_alta = $valueTicket['fecha_alta'];
                $id_usuario_actualizacion = $valueTicket['usuario_actualizacion'];
                $fecha_actualizacion = $valueTicket['fecha_actualizacion'];
                $hash_ticket =  base64_encode($valueTicket['hash_acciones']);

                $datos_status = datos_estatus($estatus);

                $consulta_motivo = ControladorTickets::ctrConsultaMotivo('motivos_bb9aa773', 'sk_motivo', $sk_motivo);
                $motivo = $consulta_motivo['nombre'];

                if ($estatus == 1) {
                    $consulta_usuario = ControladorUsuarios::ctrConsultaUsuario('v_usuarios_b7c50848', 'id_usuario', $id_usuario_alta);

                    $fecha_ultima_entrada = $fecha_alta;
                    $usuario_ultima_entrada = $consulta_usuario['nombre_completo_usuario'];
                } else {
                    $consulta_usuario = ControladorUsuarios::ctrConsultaUsuario('v_usuarios_b7c50848', 'id_usuario', $id_usuario_actualizacion);

                    $fecha_ultima_entrada = $fecha_actualizacion;
                    $usuario_ultima_entrada = $consulta_usuario['nombre_completo_usuario'];
                }
            ?>
                <tr class="tabla_row odd" role="row">
                    <td class="fila_1 sorting_1">
                        <span><?php echo $folio; ?></span>
                    </td>
                    <td class="fila_2">
                        <span class="<?php echo $datos_status['clase']; ?>"><?php echo $datos_status['texto']; ?></span>
                    </td>
                    <td class="fila_3">
                        <span><?php echo $titulo; ?></span>
                    </td>
                    <td class="fila_4">
                        <span><?php echo $motivo; ?></span>
                    </td>
                    <td class="fila_5">
                        <span>
                            <?php                            
                            if($estatus != 4 && $estatus != 2){
                                echo "<i class='fas fa-circle circulo-rojo'></i>";
                            } 
                            echo mostrar_fecha_formato($fecha_ultima_entrada);
                            ?>
                        </span>
                    </td>
                    <td class="fila_6">
                        <span><?php echo $usuario_ultima_entrada; ?></span>
                    </td>
                    <td class="fila_7">
                     <?php $consulta_cliente = ControladorUsuarios:: ctrConsultaClienteUsuarios('clientes_30f39860', 'usuario_clientes_91caadc1', $id_usuario_alta);?>
                     <span><?php echo $consulta_cliente["nombre"]; ?></span>
                        
                    </td>
                    <td class="fila_8">
                        <div class="cont_centrado">
                            <a href="<?php echo $ruta_global."ticket/".$clave_ticket; ?>" class="et_no_link">
                                <i class="fas fa-search cont_centrado tamano_iconos c_pointer tooltiped" data-toggle="tooltip" data-placement="bottom" title="Ver más"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>
