<table class="tabla_casos">
    <thead>
        <tr class="tabla_head">
            <th class="fila_1">
                #
            </th>
            <th class="fila_2">
                Estatus
            </th>
            <th class="fila_3">
                Título
            </th>
            <th class="fila_4">
                Tema
            </th>
            <th class="fila_5">
                Fecha última respuesta
            </th>
            <th class="fila_6">
                Usuario última respuesta
            </th>
            <th class="fila_7">
                Ver
            </th>
            <th class="fila_8">
                Calificar
            </th>
        </tr>
    </thead>
    <tbody id="lista_casos" class="tabla_body scroll_cumbres">
        <?php

        $tabla_consulta = 'tickets_47aaef15';
        $id_cliente = $_SESSION['id_cliente'];
        $condicion_consulta = "WHERE fk_cliente = '$id_cliente' ORDER BY folio DESC";

        $consulta_tickets = ControladorTickets::ctrConsulta($tabla_consulta, $condicion_consulta);

        foreach ($consulta_tickets as $key => $valueTicket) {

            $id_ticket = $valueTicket['sk_ticket'];
            $clave_ticket = $valueTicket['clave_ticket'];
            $folio = $valueTicket['folio'];
            $estatus = $valueTicket['estatus'];
            $titulo = $valueTicket['titulo'];
            $sk_motivo = $valueTicket['fk_motivo'];
            $calificacion = $valueTicket['calificacion'];
            $id_usuario_alta = $valueTicket['usuario_alta'];
            $fecha_alta = $valueTicket['fecha_alta'];
            $id_usuario_actualizacion = $valueTicket['usuario_actualizacion'];
            $fecha_actualizacion = $valueTicket['fecha_actualizacion'];
            $hash_ticket =  base64_encode($valueTicket['hash_acciones']);

            $datos_status = datos_estatus($estatus);

            $consulta_motivo = ControladorTickets::ctrConsultaMotivo('motivos_bb9aa773', 'sk_motivo', $sk_motivo);
            $motivo = $consulta_motivo['nombre'];

            if ($estatus == 1) {
                $consulta_usuario = ControladorUsuarios::ctrConsultaUsuario('v_usuarios_b7c50848', 'id_usuario', $id_usuario_alta);

                $fecha_ultima_entrada = $fecha_alta;
                $usuario_ultima_entrada = $consulta_usuario['nombre_completo_usuario'];
            } else {
                $consulta_usuario = ControladorUsuarios::ctrConsultaUsuario('v_usuarios_b7c50848', 'id_usuario', $id_usuario_actualizacion);

                $fecha_ultima_entrada = $fecha_actualizacion;
                $usuario_ultima_entrada = $consulta_usuario['nombre_completo_usuario'];
            }


        ?>
            <tr class="tabla_row odd" role="row">
                <td class="fila_1 sorting_1">
                    <span><?php echo $folio; ?></span>
                </td>
                <td class="fila_2">
                    <span class="tipo_nuevo <?php echo $datos_status['clase']; ?>"><?php echo $datos_status['texto']; ?></span>
                </td>
                <td class="fila_3">
                    <span><?php echo $titulo; ?></span>
                </td>
                <td class="fila_4">
                    <span><?php echo $motivo; ?></span>
                </td>
                <td class="fila_5">
                    <span><?php
                    $fecha = date("Y-m-d");
                    if( $estatus == 2 ){
                        echo "<i class='fas fa-circle circulo-rojo'></i>";
                    }
                    echo mostrar_fecha_formato($fecha_ultima_entrada); ?></span>
                </td>
                <td class="fila_6">
                    <span><?php echo $usuario_ultima_entrada; ?></span>
                </td>
                <td class="fila_7">
                    <a href="<?php echo $ruta_global . "ticket/" . $clave_ticket; ?>" class="et_no_link">
                        <i class="fas fa-search cont_centrado tamano_iconos c_pointer tooltiped" data-toggle="tooltip" data-placement="bottom" title="Ver más"></i>
                    </a>
                </td>
                <td class="fila_8">
                    <div class="cont_centrado">
                        <?php if ($estatus != 4) : ?>
                            <i class="fas fa-ban tamano_iconos tooltiped" data-toggle="tooltip" data-placement="bottom" title="No se puede calificar"></i>
                        <?php elseif (empty($valueTicket['calificacion'])) : ?>
                            <div class="c_pointer tooltiped" data-toggle="tooltip" data-placement="bottom" title="Calificar">
                                <button type="button" name="button" class="btn_votar2 grey_vot ev_calificar" key="<?php echo base64_encode($valueTicket['sk_ticket']); ?>" no="<?php echo $valueTicket['clave_ticket']; ?>" hash="<?php echo base64_encode($valueTicket['hash_acciones']); ?>">
                                    <i class="fas fa-thumbs-up"></i>
                                </button>
                            </div>
                        <?php else : ?>

                            <?php if ($valueTicket['calificacion'] == 1) : ?>
                                <div class="tooltiped" data-toggle="tooltip" data-placement="bottom" title="Calificado">
                                    <button type="button" name="button" class="btn_votar2 green_vot" style="cursor: auto;" key="<?php echo base64_encode($valueTicket['sk_ticket']); ?>" no="<?php echo $valueTicket['clave_ticket']; ?>" hash="<?php echo base64_encode($valueTicket['hash_acciones']); ?>">
                                        <i class="fas fa-thumbs-up"></i>
                                    </button>
                                </div>
                            <?php else : ?>
                                <div class="tooltiped" data-toggle="tooltip" data-placement="bottom" title="Calificado">
                                    <button type="button" name="button" class="btn_votar2 red_vot" style="cursor: auto;" key="<?php echo base64_encode($valueTicket['sk_ticket']); ?>" no="<?php echo $valueTicket['clave_ticket']; ?>" hash="<?php echo base64_encode($valueTicket['hash_acciones']); ?>">
                                        <i class="fas fa-thumbs-down"></i>
                                    </button>
                                </div>
                            <?php endif; ?>

                        <?php endif; ?>
                    </div>
                </td>
            </tr>
        <?php
        }
        ?>

    </tbody>
</table>

<form id="form_calificar_ticket" method="post">
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" id="calificar_caso">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-info" id="exampleModalLabel">Califica tu experiencia y la atención que te brindó nuestro equipo de soporte</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="contenido_bts">
                        <button type="button" name="button" class="btn_votar green_vot" id="voto_positivo" val="1">
                            <i class="fas fa-thumbs-up"></i>
                        </button>
                        <button type="button" name="button" class="btn_votar red_vot" id="voto_negativo" val="2">
                            <i class="fas fa-thumbs-down"></i>
                        </button>
                    </div>
                    <div class="form-group">
                        <input type="hidden" id="inp_calificacion" value="0">
                        <input type="hidden" id="inp_key" value="">
                        <input type="hidden" id="inp_no" value="">
                        <input type="hidden" id="inp_hash" value="">
                        <label for="recipient-name" class="col-form-label">Comentario</label>
                        <textarea class="form-control char_count enc_txt_descripcion" id="comentario_calificacion" name="descripcion" rows="6" elem="c_cliente" maxlength="500"></textarea>
                        <span id="c_cliente" class="sp_char_count"></span>
                    </div>
                </div>
                <div class="modal-footer" id="btns_calificar">
                    <button type="button" class="enc_btnCancelar" id="btn_cancelar_calificar" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="enc_btnAgregarNuevoCliente deshabilitado" id="btn_calificar_guardar" value="submit">Aceptar</button>
                </div>
                <div id="cont_loader_tickets" class="eta_cont_loader_ticket oculto">
                    <div class="spinner-border" role="status">
                        <span class="sr-only">Cargando...</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>