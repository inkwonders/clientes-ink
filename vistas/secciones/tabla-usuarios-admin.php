<?php 
    require_once "./lib/funciones.php";
?>
<table class="tabla_usuarios">
    <thead>
        <tr class="tabla_head">
            <th class="fila_1 oculto">
                #
            </th>
            <th class="fila_2">
                Nombre completo
            </th>
            <th class="fila_3">
                Correo
            </th>
            <th class="fila_4">
                Telefono
            </th>
            <th class="fila_5">
                Cliente
            </th>
            <th class="fila_6">
                Fecha de alta
            </th>
            <th class="fila_7">
                Status
            </th>
            <th class="fila_8">
                Acciones
            </th>
        </tr>
    </thead>
    <tbody id="lista_casos" class="tabla_body scroll_cumbres">
        <?php
        $consulta_usuarios = ControladorUsuarios::ctrConsultaUsuarios('usuarios_35d99c1a', 'tipo_usuario', '2');   
        foreach ($consulta_usuarios as $key => $usuarios) {     
        ?>
        <tr class="tabla_row odd" role="row">
            <td class="fila_1 oculto">
                <span><?php echo '1'; ?></span>
            </td>
            <td class="fila_2">
                <span class="tipo_nuevo"><?php echo $usuarios['nombres']." ".$usuarios['paterno']." ".$usuarios['materno']; ?></span>
            </td>
            <td class="fila_3">
                <span><?php echo $usuarios['correo']; ?></span>
            </td>
            <td class="fila_4">
                <span><?php echo $usuarios['telefono']; ?></span>
            </td>
            <?php
             $consulta_cliente_usuarios = ControladorUsuarios::ctrConsultaClienteUsuarios('clientes_30f39860', 'usuario_clientes_91caadc1', $usuarios['sk_usuario']);
            ?>
            <td class="fila_5">
                <span><?php echo $consulta_cliente_usuarios[0]; ?></span>
            </td>
            <td class="fila_6">
                <span><?php echo mostrar_fecha_formato($usuarios['fecha_alta']); ?></span>
            </td>
            <td class="fila_7">
            <?php
            if($usuarios['activo'] == 1){
                echo "<input type='checkbox' class='btn_status_usuario' key='".base64_encode($usuarios['sk_usuario'])."' key2='".base64_encode($_SESSION['id_usuario'])."' checked data-toggle='toggle' data-on='Activo' data-off='Inactivo' data-onstyle='success' data-offstyle='danger'>";
            }else{
                echo "<input type='checkbox' class='btn_status_usuario' key='".base64_encode($usuarios['sk_usuario'])."' key2='".base64_encode($_SESSION['id_usuario'])."' data-toggle='toggle' data-on='Activo' data-off='Inactivo' data-onstyle='success' data-offstyle='danger'>";
            }
            ?>
            </td>
            <td class="fila_8 ec_acciones_usuarios">
            <!--boton ver más-->
            <button type="button" class="btn btn-info ec_btnacciones c_agregar_usuario btn_verMasUsuario" data-toggle="modal" nombre="<?php echo $usuarios['nombres']." ".$usuarios['paterno']." ".$usuarios['materno']; ?>" nacimiento="<?php echo mostrar_fecha_formato($usuarios['fecha_nacimiento']); ?>" correo="<?php echo $usuarios['correo']; ?>" telefono="<?php echo $usuarios['telefono']; ?>" telefono2="<?php echo $usuarios['telefono1']; ?>" telefono3="<?php echo $usuarios['telefono2']; ?>" >
                <i class="fas fa-search-plus"></i>
            </button>
            <!--boton para editar cliente-->
            <button type="button" class="btn btn-secondary ec_btnacciones c_editar_cliente btn_editarUsuario" data-toggle="modal" nombre="<?php echo $usuarios['nombres'];?>" apaterno="<?php echo $usuarios['paterno'];?>" amaterno="<?php echo $usuarios['materno'];?>" nacimiento="<?php echo $usuarios['fecha_nacimiento'];?>" correo="<?php echo $usuarios['correo']; ?>" telefono="<?php echo $usuarios['telefono']; ?>" telefono2="<?php echo $usuarios['telefono1']; ?>" telefono3="<?php echo $usuarios['telefono2']; ?>" key="<?php echo base64_encode($usuarios["sk_usuario"]); ?>">
            <i class="fas fa-user-edit"></i>
            </button>
            <!--boton para eliminar-->
            <button type="button" class="btn btn-danger ec_btnacciones c_eliminar_usuario" key="<?php echo base64_encode($usuarios['sk_usuario']);?>" data-toggle="modal" data-target="#eliminar_usuario"><i class="fas fa-trash-alt"></i></button>
            
            </td>
        </tr>
        <?php } ?> 

    </tbody>
</table>
