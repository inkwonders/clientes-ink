<script type="text/javascript">
  var arreglo_extensiones = [
    "GIF",
    "jpeg",
    "JPG",
    "SVG",
    "pps",
    "gif",
    "png",
    "ppsx",
    "one",
    "svg",
    "psd",
    "jpg",
    "AI",
    "AIT",
    "ai",
    "ait",
    "mov",
    "docx",
    "xlsx",
    "mp4",
    "pptx",
    "doc",
    "txt",
    "xls",
    "ppt",
    "pdf",
    "PNG",
    "JPEG"
  ];
</script>

<div class="content-wrapper contenido_principal">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?php echo ($_SESSION['tipo_usuario'] == 1) ? "Proyectos" : " Mi proyecto"; ?></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <!-- <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Starter Page</li>
                    </ol> -->
          <div class="btn_nuevo_cliente">
            <?php
            if ($_SESSION['tipo_usuario'] == 1) {
              echo '<button class="ep_btnNuevoProyecto" type="button"><i class="fas fa-plus enc_icon-mas"></i>
                                <span>
                                    Nuevo proyecto
                                </span>
                            </button>';
            }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid contenido_secciones">
    <?php
    include('vistas/secciones/tabla_proyectos_admin.php');
    ?>
  </div>
</div>
</div>



<!--############################
        Modal agregar nuevo proyecto
###########################-->
<form id="formAgregarProyecto">
  <div class="modal fade" id="agregarProyecto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title text-info" id="exampleModalLabel">Agregar proyecto</h3>
        </div>
        <input name="usuario_alta" id="usuario_alta" type="hidden" value="<?php echo base64_encode($_SESSION['id_usuario']); ?>">
        <div class="modal-body">
          <div class="form-outline mb-4">
            <label class="form-label" for="form3Example2">Nombre</label>
            <input type="name" name="nombre_proyecto" id="input_nombre_proyecto" class="form-control char_count" elem="c_nombre_proyecto" maxlength="50" required="" />
            <span id="c_nombre_proyecto" class="sp_char_count"></span>
          </div>

          <div class="form-outline mb-4">
            <label class="form-label" for="form3Example2">Cliente</label>
            <select id="select_cliente_proyecto" name="cliente" class="form-control select2" style="width: 100%;">
              <option value="0" disabled selected>Selecciona un cliente.</option>
              <?php
              $consulta_clientes = Clientes::ctrConsultaClientes('clientes_30f39860', 'usuario_clientes_91caadc1', '');
              foreach ($consulta_clientes as $key => $clientes) {
              ?>
                <option value="<?php echo base64_encode($clientes["sk_cliente"]); ?>"><?php echo $clientes["nombre"]; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-outline mb-4">
            <label class="form-label" for="form3Example2">Descripción</label>
            <textarea class="form-control char_count ep_txt_descripcion" id="descripcion_proyecto" name="descripcion" rows="6" elem="car_descripcion" maxlength="1500"></textarea>
            <span id="car_descripcion_proyecto" class="sp_char_count"></span>
          </div>
          <div class="cont_btn_center">
            <div class="contenido_inputs_files" id="contenido_inputs_files_1">
              <input type="file" name="subida_de_archivo_dos[]" id="subida_de_archivo_dos_0" style="display: none;" multiple="multiple">
              <label class="btnEnviar subida_de_archivo_dos_0" id="cargar_archivos_dos" for="subida_de_archivo_dos_0">Adjuntar archivos</label>
            </div>
            <div class="contenedor_archivos"></div>
          </div>
          <br>
        </div>
        <div class="cont_centrado">
          <div id="cont_loader_proyectos" class="oculto">
            <div class="spinner-border" role="status">
              <span class="sr-only">Cargando...</span>
            </div>
          </div>
        </div>
        <div id="respuesta_agregar_usuario" class="cont_centrado"></div>
        <div class="modal-footer" id="btns_agregar_proyecto">
          <button type="button" class="ep_btnCancelar" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="ep_btnAgregarProyecto deshabilitado">Agregar proyecto</button>
        </div>
      </div>
    </div>
  </div>
</form>

<!--############################
        Modal ver más PROYECTO
###########################-->
<div class="modal fade" id="modal_verMasProyecto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal_proyecto" role="document">
    <div class="modal-content" id="contenido_modal_proyecto">
      <!-- aqui van los datos cargados con ajax -->
    </div>
  </div>
</div>

<div class="modal fade" id="error_general_proyectos" tabindex="-1" role="dialog" aria-labelledby="error_title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-danger">¡Error!</h3>
      </div>
      <div class="modal-body text-break" id="msjErrorModal_proyectos">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exito_general_proyectos" tabindex="-1" role="dialog" aria-labelledby="error_title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-success">¡Éxito!</h3>
      </div>
      <div class="modal-body text-break" id="msjExitoModal_proyectos">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn_modal_exito">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/script_proyectos.js"></script>