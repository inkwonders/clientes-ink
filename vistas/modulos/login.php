<div class="contenedor-login">
  <div class="login-box">

      <div class="tarjeta">
          <div class="tarjeta-body login-tarjeta-body">
              <div class="login-logo">
                  <a href="">
                      <img src="<?php echo $ruta_global; ?>vistas/assets/img/logo.svg">
                  </a>
              </div>
              <br>

              <form id="form_ingreso" method="post">
                  <div class="input-field input-group mb-3">
                      <p class="texto">Correo</p>
                      <input type="text" name="email_login" id="txtNombre" class="campoTexto" maxlength="120" required>
                  </div>
                  <div class="input-field input-group mb-3">
                      <p class="texto">Contraseña</p>
                      <input type="password" class="campoTexto" name="contraseña_login" id="password" required>
                      <img src="<?php echo $ruta_global; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar-pass mostrar_password" inp="password" id="mostrar_pass_login" title="Mostrar contraseña">
                  </div>
                  <br>
                  <br>
                  <div id="cont_botones_inicio">
                      <input id="btn_login" type="submit" class="button-inicio" value="INICIAR SESIÓN"> <br>
                  </div>
                  <div id="respuesta_login"class="respuesta_clave">
                  <?php

                    // $ingreso = new ControladorUsuarios();
                    // $ingreso->ctrIngresoUsuario();

                  ?>
                  </div>                  
              </form>

          </div>
      </div>

  </div>
</div>
