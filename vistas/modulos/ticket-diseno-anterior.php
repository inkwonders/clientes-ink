<?php

$ruta_404 = $ruta_global . "404";
if (!empty($rutas[1])) {
    $no_ticket = $rutas[1];
    $consulta_ticket = ControladorTickets::ctrConsultaTicketExistente($no_ticket);
    if (!empty($consulta_ticket)) {
        if ($_SESSION['tipo_usuario'] == 1) { // es admin
        } else if ($_SESSION['tipo_usuario'] == 2 && $_SESSION['id_usuario'] == $consulta_ticket['usuario_alta'] && $_SESSION['id_cliente'] == $consulta_ticket['fk_cliente']) {
        } else {
            echo '<script> location.href = "' . $ruta_404 . '"; </script>';
        }
    } else {
        echo '<script> location.href = "' . $ruta_404 . '"; </script>';
    }
} else {
    echo '<script> location.href = "' . $ruta_404 . '"; </script>';
}

$id_ticket = $consulta_ticket['sk_ticket'];
$carpeta_ticket = $consulta_ticket['carpeta_ticket'];

$datos_status = datos_estatus($consulta_ticket['estatus']);

$consulta_motivo = ControladorTickets::ctrConsultaMotivo('motivos_bb9aa773', 'sk_motivo', $consulta_ticket['fk_motivo']);
$motivo = $consulta_motivo['nombre'];

if ($consulta_ticket['estatus'] == 1) {
    $consulta_usuario = ControladorUsuarios::ctrConsultaUsuario('v_usuarios_b7c50848', 'id_usuario', $consulta_ticket['usuario_alta']);

    $fecha_ultima_entrada = $consulta_ticket['fecha_alta'];
    $usuario_ultima_entrada = $consulta_usuario['nombre_completo_usuario'];
} else {
    $consulta_usuario = ControladorUsuarios::ctrConsultaUsuario('v_usuarios_b7c50848', 'id_usuario', $consulta_ticket['usuario_actualizacion']);

    $fecha_ultima_entrada = $consulta_ticket['fecha_actualizacion'];;
    $usuario_ultima_entrada = $consulta_usuario['nombre_completo_usuario'];
}

?>

<script type="text/javascript">
    var arreglo_extensiones = [
        "GIF",
        "jpeg",
        "JPG",
        "SVG",
        "pps",
        "gif",
        "png",
        "ppsx",
        "one",
        "svg",
        "psd",
        "jpg",
        "AI",
        "AIT",
        "ai",
        "ait",
        "mov",
        "docx",
        "xlsx",
        "mp4",
        "pptx",
        "doc",
        "txt",
        "xls",
        "ppt",
        "pdf",
        "PNG",
        "JPEG"
    ];
</script>
<div class="content-wrapper contenido_principal eta_conten_height">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Detalles del ticket</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <div class="btn_nuevo">
                        <?php if ($consulta_ticket['estatus'] != 4) : ?>
                            <button class="eta_btnNuevoTicket" data-toggle="modal" data-target="#modal_entrada">
                                <i class="fas fa-plus enc_icon-mas"></i>
                                <span>Nueva respuesta</span>
                            </button>
                        <?php endif; ?>
                        <?php if ($_SESSION['tipo_usuario'] == 1 && $consulta_ticket['estatus'] != 4  && $consulta_ticket['estatus'] != 1) : ?>
                            <button class="eta_btnCerrarCaso eta_margen_der" data-toggle="modal" data-target="#cerrar_caso">
                                <i class="fas fa-check enc_icon-mas"></i>
                                <span>Cerrar caso</span>
                            </button>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content cont_ticked">
        <div class="container-fluid contenido_secciones">
            <div class="eta_contenido_detalles">
                <div class="body_caso">

                    <div class="tabla_ticket margen_inf_movil">
                        <div class="eta_tabla_head">
                            <div class="eta_fila_1 eta_fila_movil">
                                <span>#</span>
                            </div>
                            <div class="eta_fila_2 eta_fila_movil">
                                <span>Estatus</span>
                            </div>
                            <div class="eta_fila_3 eta_fila_movil">
                                <span>Título</span>
                            </div>
                            <div class="eta_fila_4 eta_fila_movil">
                                <span>Motivo</span>
                            </div>
                            <div class="eta_fila_5 eta_fila_movil">
                                <span>Fecha última respuesta</span>
                            </div>
                            <div class="eta_fila_6 eta_fila_movil">
                                <span>Usuario última respuesta</span>
                            </div>
                        </div>
                        <div class="eta_tabla_body tabla_2">
                            <div class="eta_tabla_row eta_minimo">
                                <div class="eta_fila_1 eta_fila_movil">
                                    <span><?php echo $consulta_ticket['folio']; ?></span>
                                </div>
                                <div class="eta_fila_2 eta_fila_movil">
                                    <span class="<?php echo $datos_status['clase']; ?>"><?php echo $datos_status['texto']; ?></span>
                                </div>
                                <div class="eta_fila_3 eta_fila_movil">
                                    <span><?php echo $consulta_ticket['titulo']; ?></span>
                                </div>
                                <div class="eta_fila_4 eta_fila_movil">
                                    <span><?php echo $motivo; ?></span>
                                </div>
                                <div class="eta_fila_5 eta_fila_movil">
                                    <span><?php echo mostrar_fecha_formato($fecha_ultima_entrada); ?></span>
                                </div>
                                <div class="eta_fila_6 eta_fila_movil">
                                    <span><?php echo $usuario_ultima_entrada; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="eta_entradas">
                        <div class="eta_caso_detalles">
                            <div class="tabla_ticket margen_inf_movil" id="archivos_mostrar">
                                <div class="eta_tabla_head">
                                    <div class="eta_fila_33 width_100 eta_menos_h">
                                        <span>Archivos</span>
                                    </div>
                                </div>
                                <div class="eta_tabla_body eta_tabla_2">
                                    <div class="eta_tabla_row">
                                        <div class="eta_fila_33 width_100">
                                            <div class="eta_archivos_contenedor">
                                                <?php
                                                $consulta_archivos_ticket = ControladorTickets::ctrConsultaArchivos('tickets_archivos_cb0a4d09', 'fk_ticket', $id_ticket, 'ORDER BY orden ASC');
                                                $tam_archivos_ticket = sizeof($consulta_archivos_ticket);

                                                foreach ($consulta_archivos_ticket as $key => $valueArchivoTicket) :

                                                    $ruta_archivo = $ruta_global . "vistas/assets/archivos/" . $carpeta_ticket . "/" . $valueArchivoTicket['archivo'];
                                                ?>
                                                    <a download="<?php echo $valueArchivoTicket['nombre_archivo_original']; ?>" href="<?php echo $ruta_archivo; ?>" title="Descargar">
                                                        <span><?php echo $valueArchivoTicket['nombre_archivo_original']; ?></span>
                                                    </a>
                                                <?php endforeach; ?>
                                                <?php if ($tam_archivos_ticket == 0) : ?>
                                                    <div>
                                                        <p class="eta_p_no_res">No se adjuntaron archivos.</p>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabla_ticket margen_inf_movil">
                                <div class="eta_tabla_head sin_borde">
                                    <div class="eta_fila_33 width_100 eta_menos_h">
                                        <span>Descripción</span>
                                    </div>
                                </div>
                                <div class="eta_caja_texto_entrada eta_sin_margin_top">
                                    <span>
                                        <?php echo $consulta_ticket['descripcion']; ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($_SESSION['tipo_usuario'] == 1) : ?>
                        <div class="eta_contenido_titulo eta_margen_sup">
                            <span class="eta_titulo eta_titulo_2 eta_titulo_inf">Información del usuario</span>
                        </div>
                        <div class="tabla_ticket margen_inf_movil">
                            <div class="eta_tabla_head">
                                <div class="eta_fila_11 eta_fila_movil">
                                    <span>Nombre completo</span>
                                </div>
                                <div class="eta_fila_12 eta_fila_movil">
                                    <span>Teléfono</span>
                                </div>
                                <div class="eta_fila_13 eta_fila_movil">
                                    <span>Correo</span>
                                </div>
                                <div class="eta_fila_14 eta_fila_movil">
                                    <span>Cliente</span>
                                </div>
                            </div>
                            <?php
                            $consulta_usuario_caso = ControladorUsuarios::ctrConsultaUsuario('v_usuarios_b7c50848', 'id_usuario', $consulta_ticket['usuario_alta']);
                            ?>
                            <div class="eta_tabla_body eta_tabla_2">
                                <div class="eta_tabla_row">
                                    <div class="eta_fila_11 eta_fila_movil">
                                        <span><?php echo $consulta_usuario_caso['nombre_completo_usuario']; ?></span>
                                    </div>
                                    <div class="eta_fila_12 eta_fila_movil">
                                        <span><?php echo $consulta_usuario_caso['telefono_usuario']; ?></span>
                                    </div>
                                    <div class="eta_fila_13 eta_fila_movil">
                                        <span><?php echo $consulta_usuario_caso['correo_usuario']; ?></span>
                                    </div>
                                    <div class="eta_fila_14 eta_fila_movil">
                                        <span><?php echo $consulta_usuario_caso['nombre_cliente']; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($consulta_ticket['estatus'] == 4 && !empty($consulta_ticket['calificacion'])) : ?>
                        <div class="eta_contenido_titulo eta_margen_sup">
                            <span class="eta_titulo eta_titulo_2 eta_titulo_inf">Calificación y comentarios</span>
                        </div>
                        <div class="tabla_ticket margen_inf_movil">
                            <div class="eta_tabla_head">
                                <div class="eta_fila_21">
                                    <span>Calificación</span>
                                </div>
                                <div class="eta_fila_22">
                                    <span>Comentarios</span>
                                </div>
                            </div>
                            <div class="eta_tabla_body eta_tabla_2">
                                <div class="eta_tabla_row">
                                    <div class="eta_fila_21">
                                        <span>
                                            <?php if ($consulta_ticket['calificacion'] == 1) : ?>
                                                <div class="eta_boton_calif">
                                                    <i class="fas fa-thumbs-up"></i>
                                                </div>
                                            <?php else : ?>
                                                <div class="btn_votar2 red_vot">
                                                    <i class="fas fa-thumbs-down"></i>
                                                </div>
                                            <?php endif; ?>
                                        </span>
                                    </div>
                                    <div class="eta_fila_22 eta_break_word">
                                        <span><?php echo $consulta_ticket['comentario_calificacion']; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php
                    $tabla_entradas = 'tickets_entradas_09f26b6a';
                    $condicion_consulta_entrada = "WHERE fk_ticket = '$id_ticket' ORDER BY fecha_alta DESC";
                    $consulta_entradas = ControladorTickets::ctrConsulta($tabla_entradas, $condicion_consulta_entrada);
                    $total_entradas = sizeof($consulta_entradas);
                    ?>
                    <div class="eta_entradas margen_inf_movil">

                        <?php if ($total_entradas > 0) : ?>
                            <div class="eta_contenido_titulo eta_margen_sup">
                                <span class="eta_titulo eta_titulo_2 eta_titulo_inf">Respuestas</span>
                            </div>
                        <?php endif; ?>

                        <?php foreach ($consulta_entradas as $key => $valueEntrada) : ?>
                            <div class="eta_caso_detalles">
                                <div class="tabla_ticket">
                                    <div class="eta_tabla_head">
                                        <div class="eta_fila_31 eta_fila_movil">
                                            <span>Usuario</span>
                                        </div>
                                        <div class="eta_fila_32 eta_fila_movil">
                                            <span>Fecha</span>
                                        </div>
                                        <div class="eta_fila_33 eta_fila_movil">
                                            <span>Archivos</span>
                                        </div>
                                    </div>
                                    <div class="eta_tabla_body tabla_2 scroll_cumbres">
                                        <?php
                                        $consulta_usuario_entrada = ControladorUsuarios::ctrConsultaUsuario('v_usuarios_b7c50848', 'id_usuario', $valueEntrada['usuario_alta']);
                                        ?>
                                        <div class="eta_tabla_row">
                                            <div class="eta_fila_31 eta_fila_movil">
                                                <span><?php echo $consulta_usuario_entrada['nombre_completo_usuario']; ?></span>
                                            </div>
                                            <div class="eta_fila_32 eta_fila_movil">
                                                <span><?php echo mostrar_fecha_formato($valueEntrada['fecha_alta']); ?></span>
                                            </div>
                                            <div class="eta_fila_33 eta_fila_movil">
                                                <?php
                                                $consulta_archivos_entrada = ControladorTickets::ctrConsultaArchivos('tickets_entradas_archivos_c53737c2', 'fk_entrada', $valueEntrada['sk_entrada'], 'ORDER BY orden ASC');
                                                $tam_archivos_entradas = sizeof($consulta_archivos_entrada);
                                                ?>
                                                <div class="eta_archivos_contenedor">
                                                    <?php foreach ($consulta_archivos_entrada as $key => $valueArchivosEntrada) :
                                                        $ruta_archivo = $ruta_global . "vistas/assets/archivos/" . $carpeta_ticket . "/" . $valueEntrada['carpeta_entrada'] . "/" . $valueArchivosEntrada['archivo_entrada'];
                                                    ?>
                                                        <a download="<?php echo $valueArchivosEntrada['nombre_archivo_original']; ?>" href="<?php echo $ruta_archivo; ?>" title="Descargar">
                                                            <span><?php echo $valueArchivosEntrada['nombre_archivo_original']; ?></span>
                                                        </a>
                                                    <?php endforeach; ?>
                                                    <?php if ($tam_archivos_entradas == 0) : ?>
                                                        <div>
                                                            <p class="eta_p_no_res">No se adjuntaron archivos.</p>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="eta_caja_texto_entrada width_movil_100">
                                    <span>
                                        <?php echo $valueEntrada['comentario']; ?>
                                    </span>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="width_100 eta_margen_sup"></div>
                </div>
                <div class="cont_btn">
                    <div class="btnEnviar" id="btnEnviar" onclick="window.history.back();">&lt; Regresar</div>
                </div>
            </div>
        </div>
    </div>
</div>

<form id="form_entrada" method="post">
    <div class="modal fade" id="modal_entrada" tabindex="-1" role="dialog" aria-labelledby="modal_entradaLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-info">Nueva respuesta</h3>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" id="key" rg="<?php echo $ruta_global; ?>" value="<?php echo base64_encode($id_ticket); ?>">
                        <input type="hidden" id="tipo_entrada" value="<?php echo $_SESSION['tipo_usuario']; ?>">
                        <input type="hidden" id="cp" value="<?php echo base64_encode($carpeta_ticket); ?>">
                        <label for="recipient-name" class="col-form-label">Comentario</label>
                        <textarea class="form-control char_count enc_txt_descripcion" id="inp_descripcion_entrada" name="descripcion" rows="6" elem="c_cliente" maxlength="1500"></textarea>
                        <span id="c_cliente" class="sp_char_count"></span>
                    </div>
                    <div class="cont_btn_center">
                        <div class="contenido_inputs_files" id="contenido_inputs_files_1">
                            <input type="file" name="subida_de_archivo[]" id="subida_de_archivo_0" style="display: none;" multiple="multiple">
                            <label class="btnEnviar subida_de_archivo_0" id="cargar_archivos" for="subida_de_archivo_0">Adjuntar archivos</label>
                        </div>
                        <span class="msg_archivos">* Solo se permiten 5 archivos, con un peso máximo de 5 MB cada uno.</span>
                        <div class="contenedor_archivos"></div>
                    </div>
                </div>
                <div class="modal-footer" id="btns_entrada">
                    <button type="button" class="enc_btnCancelar" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="enc_btnAgregarNuevoCliente deshabilitado" id="btn_guardar_entrada" value="submit">Agregar respuesta</button>
                </div>
                <div id="cont_loader_tickets" class="eta_cont_loader_ticket oculto">
                    <div class="spinner-border" role="status">
                        <span class="sr-only">Cargando...</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="modal fade" tabindex="-1" role="dialog" id="cerrar_caso">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cerrar caso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>¿Está seguro de cerrar el caso?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn_aceptar_cerrar_ticket" key="<?php echo base64_encode($consulta_ticket['sk_ticket']); ?>" key2="<?php echo base64_encode($consulta_ticket['fk_cliente']); ?>" no="<?php echo $consulta_ticket['clave_ticket']; ?>" hash="<?php echo base64_encode($consulta_ticket['hash_acciones']); ?>">Aceptar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="error_general_tickets" tabindex="-1" role="dialog" aria-labelledby="error_title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-danger">¡Error!</h3>
            </div>
            <div class="modal-body text-break" id="msjErrorModal_tickets">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exito_general_tickets" tabindex="-1" role="dialog" aria-labelledby="error_title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-success">¡Éxito!</h3>
            </div>
            <div class="modal-body text-break" id="msjExitoModal_tickets">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn_modal_exito">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/script_ticket.js"></script>