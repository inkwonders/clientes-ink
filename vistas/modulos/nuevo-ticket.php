<script type="text/javascript">
    var arreglo_extensiones = [
        "GIF",
        "jpeg",
        "JPG",
        "SVG",
        "pps",
        "gif",
        "png",
        "ppsx",
        "one",
        "svg",
        "psd",
        "jpg",
        "AI",
        "AIT",
        "ai",
        "ait",
        "mov",
        "docx",
        "xlsx",
        "mp4",
        "pptx",
        "doc",
        "txt",
        "xls",
        "ppt",
        "pdf",
        "PNG",
        "JPEG"
    ];
</script>
<div class="content-wrapper contenido_principal">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Nuevo ticket</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                <div class="cont_btn">
                    <div class="enc_btnRegresar" id="btnEnviar" onclick="window.history.back();">&lt; Regresar</div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid contenido_secciones">
            <div class="cont_caso">
                <div class="body_caso">
                    <div class="parte_caso borde_derecho">
                        <img src="<?php echo $ruta_global; ?>vistas/assets/img/receipt.svg" alt="INKWONDERS" class="brand-image" style="opacity: .8">
                    </div>
                    <div class="parte_caso">
                        <form id="form_nuevo_ticket" class="datos_caso" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="inp_titulo">Título*</label>
                                <input type="text" class="form-control char_count" id="inp_titulo" name="titulo" placeholder="Título" elem="c_titulo" maxlength="50">
                                <span id="c_titulo" class="sp_char_count"></span>
                            </div>
                            <br>
                            <div class="form-group">
                                <label>Tema*</label>
                                <select id="select_motivo" name="motivo" class="form-control select2" style="width: 100%;">
                                    <option value="0" disabled selected>Selecciona un motivo.</option>
                                    <?php                                   
                                    $motivos = ControladorTickets::ctrConsulta('motivos_bb9aa773','ORDER BY orden ASC');                                                                                
                                    foreach ($motivos as $key => $value) {                                  
                                        ?>
                                        <option value="<?php echo $value['clave_motivo']; ?>"><?php echo $value['nombre']; ?></option>
                                        <?php
                                    }                                    
                                    ?>
                                </select>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="inp_descripcion">Descripción*</label>
                                <textarea class="form-control char_count enc_txt_descripcion" id="inp_descripcion" name="descripcion" rows="6" elem="car_descripcion" maxlength="1500"></textarea>
                                <span id="car_descripcion" class="sp_char_count"></span>
                            </div>
                            <div class="cont_btn_center">
                                <div class="contenido_inputs_files" id="contenido_inputs_files_1">
                                    <input type="file" name="subida_de_archivo[]" id="subida_de_archivo_0" style="display: none;" multiple="multiple">
                                    <label class="btnEnviar subida_de_archivo_0" id="cargar_archivos" for="subida_de_archivo_0">Adjuntar archivos</label>
                                </div>
                                <span class="msg_archivos">* Solo se permiten 5 archivos, con un peso máximo de 5 MB cada uno.</span>
                                <div class="contenedor_archivos"></div>
                            </div>
                            <br>
                            <div class="cont_centrado">
                                <div id="cont_loader_tickets" class="oculto">
                                    <div class="spinner-border" role="status">
                                        <span class="sr-only">Cargando...</span>
                                    </div>
                                </div>
                                <button id="btn_guardar" class="enc_btnGuardar deshabilitado" type="submit">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="error_general_tickets" tabindex="-1" role="dialog" aria-labelledby="error_title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-danger">¡Error!</h3>        
      </div>
      <div class="modal-body text-break" id="msjErrorModal_tickets">        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>        
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exito_general_tickets" tabindex="-1" role="dialog" aria-labelledby="error_title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-success">¡Éxito!</h3>        
      </div>
      <div class="modal-body text-break" id="msjExitoModal_tickets">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn_modal_exito">Aceptar</button>        
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="vistas/assets/js/script_nuevo_ticket.js"></script>