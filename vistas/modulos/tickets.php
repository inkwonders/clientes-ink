<div class="content-wrapper contenido_principal">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?php echo ($_SESSION['tipo_usuario'] == 1) ? "Tickets" : " Mis Tickets"; ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <!-- <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Starter Page</li>
                    </ol> -->
                    <div class="btn_nuevo">
                        <?php if ($_SESSION['tipo_usuario'] == 2): ?>
                           <a class="enc_btnNuevoTicket" href="nuevo-ticket"><i class="fas fa-plus enc_icon-mas"></i>
                                <span>
                                    Nuevo ticket
                                </span>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid contenido_secciones">
            <?php
            if ($_SESSION['tipo_usuario'] == 1) {
                include('vistas/secciones/tabla_tickets_admin.php');
            } elseif ($_SESSION['tipo_usuario'] == 2) {
                include('vistas/secciones/tabla_tickets_clientes.php');
            }
            ?>
        </div>
    </div>

</div>

<div class="modal fade" id="error_general_tickets" tabindex="-1" role="dialog" aria-labelledby="error_title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-danger">¡Error!</h3>
            </div>
            <div class="modal-body text-break" id="msjErrorModal_tickets">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exito_general_tickets" tabindex="-1" role="dialog" aria-labelledby="exito_title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-success">¡Éxito!</h3>
            </div>
            <div class="modal-body text-break" id="msjExitoModal_tickets">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn_modal_exito">Aceptar</button>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/script_tickets.js"></script>