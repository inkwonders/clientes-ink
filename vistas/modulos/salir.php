<?php  

  unset($_SESSION["sesion_activa"]);
  unset($_SESSION["tipo_usuario"]);
  unset($_SESSION["id_usuario"]);
  unset($_SESSION["correo"]);  

  session_destroy();


  echo '<script>

  	location.href = "'.$ruta_global.'login";

  </script>';
