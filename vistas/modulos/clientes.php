<div class="content-wrapper contenido_principal">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Clientes</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <!-- <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Starter Page</li>
                    </ol> -->
          <div class="btn_nuevo_cliente">
            <?php
            if ($_SESSION['tipo_usuario'] == 1) {
              echo '<button class="enc_btnNuevoCliente" type="button" data-toggle="modal" data-target="#agregar_cliente"><i class="fas fa-plus enc_icon-mas"></i>
                                <span>
                                    Nuevo cliente
                                </span>
                            </button>';
            }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid contenido_secciones">
    <?php
    include('vistas/secciones/tabla-clientes-admin.php');
    ?>
  </div>
</div>
</div>

<!--############################
        Modal agregar CLIENTE
###########################-->
<form id="form_agregar_cliente" method="post">
  <div class="modal fade" id="agregar_cliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title text-info" id="exampleModalLabel">Agregar nuevo cliente</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input name="usuario_alta" type="hidden" value="<?php echo $_SESSION['id_usuario']; ?>">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Cliente *</label>
            <input type="text" class="form-control char_count" name="nombre_cliente" id="input_agregar_cliente" elem="c_cliente" maxlength="40">
            <span id="c_cliente" class="sp_char_count"></span>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="enc_btnCancelar" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="enc_btnAgregarNuevoCliente" value="submit">Agregar cliente</button>
        </div>
      </div>
    </div>
  </div>
</form>

<!--############################
        Modal agregar USUARIO
###########################-->
<form id="formAgregarUsuario">
  <div class="modal fade" id="agregarUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title text-info" id="exampleModalLabel">Agregar nuevo usuario</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <input name="usuario_alta" type="hidden" value="<?php echo $_SESSION['id_usuario']; ?>">
        <div class="modal-body">
          <div class="form-outline mb-4">
            <label class="form-label" for="form3Example2">Nombre *</label>
            <input type="name" name="u_nombre" id="input_nombre_usuario" class="form-control char_count" elem="c_nombre" maxlength="30" required="" />
            <span id="c_nombre" class="sp_char_count"></span>
          </div>
          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form3Example2">Apellido Paterno *</label>
                <input type="text" name="apellidoP" id="input_apellido_paterno" class="form-control char_count" elem="c_aPaterno" maxlength="30" required="" />
                <span id="c_aPaterno" class="sp_char_count"></span>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form3Example2">Apellido Materno </label>
                <input type="text" name="apellidoM" id="input_apellido_materno" class="form-control char_count" elem="c_aMaterno" maxlength="30" />
                <span id="c_aMaterno" class="sp_char_count"></span>
              </div>
            </div>
          </div>
          <div class="form-outline mb-4">
            <label class="form-label" for="form3Example2">Correo electrónico *</label>
            <input type="email" name="correo" class="form-control char_count" id="input_correo" elem="c_correo" maxlength="100" required="" />
            <span id="c_correo" class="sp_char_count"></span>
          </div>
          <div class="form-outline mb-4">
            <label class="form-label" for="form3Example2">Teléfono *</label>
            <input onkeydown="validateNumber();" type="text" name="telefono" class="form-control char_count" id="input_telefono" elem="c_telefono" maxlength="10" required="" />
            <span id="c_telefono" class="sp_char_count"></span>
          </div>
          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form3Example2">Telefono 2</label>
                <input onkeydown="validateNumber();" type="text" name="telefono2" class="form-control char_count" elem="c_telefono2" maxlength="10" />
                <span id="c_telefono2" class="sp_char_count"></span>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form3Example2">Telefono 3</label>
                <input onkeydown="validateNumber();" type="text" name="telefono3" class="form-control char_count" elem="c_telefono3" maxlength="10" />
                <span id="c_telefono3" class="sp_char_count"></span>
              </div>
            </div>
          </div>
          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form3Example2">Contraseña *</label>
                <input type="password" name="contrasena" class="form-control char_count" id="input_contrasena" elem="c_contrasena" maxlength="30" required="" />
                <span id="c_contrasena" class="sp_char_count"></span>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form3Example2">Repetir contraseña *</label>
                <input type="password" name="contrasena_repetir" class="form-control char_count" id="input_repetir_contrasena" elem="c_repetir_contrasena" maxlength="30" required="" />
                <span id="c_repetir_contrasena" class="sp_char_count"></span>
              </div>
            </div>
          </div>
          <div id="respuesta_agregar_usuario" class="cont_centrado"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="enc_btnCancelar" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="enc_btnAgregarNuevoUsuario">Agregar usuario</button>
        </div>
      </div>
    </div>
  </div>
</form>

<!--############################
        Modal editar CLIENTE
###########################-->
<form id="form_editar_cliente">
  <div class="modal fade" id="editarCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title text-info">Editar cliente</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nombre cliente *</label>
            <input type="text" name="nombre_cliente_editado" class="form-control char_count" id="c_input_nombre_cliente" elem="c_cliente" maxlength="40">
            <span id="c_cliente" class="sp_char_count"></span>
          </div>
        </div>
        <div id="respuesta_editar_cliente" class="cont_centrado"></div>
        <div class="modal-footer">
          <button type="button" class="enc_btnCancelar" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="enc_btnAgregarNuevoCliente">Editar</button>
        </div>
      </div>
    </div>
  </div>
</form>


<!--############################
        Modal eliminar CLIENTE  
###########################-->
<div class="modal fade" id="eliminar_cliente" tabindex="-1" role="dialog" aria-labelledby="eliminar_cliente" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-danger">Eliminar cliente</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ¿Está seguro de eliminar este cliente?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btn_seguro_eliminar" class="btn btn-danger">Eliminar</button>
      </div>
    </div>
  </div>
</div>

<!--############################
        Modal aviso status de cliente
###########################-->
<div class="modal fade" id="status_cliente_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-success" id="exampleModalLabel">Status</h3>
      </div>
      <div class="modal-body">
        El status ha sido modificado con éxito.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="error_general_clientes" tabindex="-1" role="dialog" aria-labelledby="error_title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-danger">¡Error!</h3>
      </div>
      <div class="modal-body text-break" id="msjErrorModal_clientes">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exito_general_clientes" tabindex="-1" role="dialog" aria-labelledby="error_title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-success">¡Éxito!</h3>
      </div>
      <div class="modal-body text-break" id="msjExitoModal_clientes">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn_modal_exito">Aceptar</button>
      </div>
    </div>
  </div>
</div>


<script src="<?php echo $ruta_global; ?>vistas/assets/js/script_cliente.js"></script>