<div class="content-wrapper contenido_principal">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Usuarios</h1>
        </div>
        <div class="col-sm-6">
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid contenido_secciones">
    <?php
    include('vistas/secciones/tabla-usuarios-admin.php');
    ?>
  </div>
</div>
</div>

<!--############################
        Modal ver más USUARIO
###########################-->
<div class="modal fade" id="modal_verMasUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-info" id="exampleModalLabel">Datos del usuario</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-outline mb-4">
          <label class="form-label" for="form3Example2">Nombre: </label>
          <span class="label" id="vm_nombre"></span>
        </div>
        <div class="form-outline mb-4">
          <label class="form-label" for="form3Example2">Correo electrónico: </label>
          <span class="label" id="vm_correo"></span>
        </div>
        <div class="form-outline mb-4">
          <label class="form-label" for="form3Example2">Teléfono: </label>
          <span id="vm_telefono"></span>
        </div>
        <div class="row mb-4">
          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form3Example2">Telefono 2: </label>
              <span class="label" id="vm_telefono2"></span>
            </div>
          </div>
          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form3Example2">Telefono 3: </label>
              <span class="label" id="vm_telefono3"></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!--############################
        Modal editar USUARIO
###########################-->
<form id="formEditarUsuario">
  <div class="modal fade" id="modal_editarUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title text-info" id="exampleModalLabel">Editar usuario</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <input name="usuario_actualizacion" type="hidden" value="<?php echo $_SESSION['id_usuario']; ?>">
        <div class="modal-body">
          <div class="form-outline mb-4">
            <label class="form-label" for="form3Example2">Nombre *</label>
            <input type="name" name="u_nombre" id="input_nombre_usuario_edit" class="form-control char_count" elem="c_nombre" maxlength="30" required="" />
            <span id="c_nombre" class="sp_char_count"></span>
          </div>
          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form3Example2">Apellido Paterno *</label>
                <input type="text" name="apellidoP" id="input_apellido_paterno_edit" class="form-control char_count" elem="c_aPaterno" maxlength="30" required="" />
                <span id="c_aPaterno" class="sp_char_count"></span>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form3Example2">Apellido Materno</label>
                <input type="text" name="apellidoM" id="input_apellido_materno_edit" class="form-control char_count" elem="c_aMaterno" maxlength="30" />
                <span id="c_aMaterno" class="sp_char_count"></span>
              </div>
            </div>
          </div>
          <div class="form-outline mb-4">
            <label class="form-label" for="form3Example2">Correo electrónico *</label>
            <input type="email" name="correo" class="form-control char_count" id="input_correo_edit" elem="c_correo" maxlength="100" required="" />
            <span id="c_correo" class="sp_char_count"></span>
          </div>
          <div class="form-outline mb-4">
            <label class="form-label" for="form3Example2">Teléfono *</label>
            <input onkeydown="validateNumber();" type="text" name="telefono" class="form-control char_count" id="input_telefono_edit" elem="c_telefono" maxlength="10" required="" />
            <span id="c_telefono" class="sp_char_count"></span>
          </div>
          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form3Example2">Telefono 2</label>
                <input onkeydown="validateNumber();" type="text" name="telefono2" id="input_telefono2_edit" class="form-control char_count" elem="c_telefono2" maxlength="10" />
                <span id="c_telefono2" class="sp_char_count"></span>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form3Example2">Telefono 3</label>
                <input onkeydown="validateNumber();" type="text" name="telefono3" id="input_telefono3_edit" class="form-control char_count" elem="c_telefono3" maxlength="10" />
                <span id="c_telefono3" class="sp_char_count"></span>
              </div>
            </div>
          </div>
          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form3Example2">Contraseña</label>
                <input type="password" name="contrasena" class="form-control char_count" id="input_contrasena_edit" elem="c_contrasena" maxlength="30"/>
                <span id="c_contrasena" class="sp_char_count"></span>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form3Example2">Repetir contraseña</label>
                <input type="password" name="contrasena_repetir" class="form-control char_count" id="input_repetir_contrasena_edit" elem="c_repetir_contrasena" maxlength="30"/>
                <span id="c_repetir_contrasena" class="sp_char_count"></span>
              </div>
            </div>
          </div>
        </div>
        <div id="respuesta_editar_usuario" class="cont_centrado"></div>
        <div class="modal-footer">
          <button type="button" class="enc_btnCancelar" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="enc_btnAgregarNuevoUsuario_edit deshabilitado">Editar</button>
        </div>
      </div>
    </div>
  </div>
</form>


<!--############################
        Modal status USUARIO
###########################-->

<div class="modal fade" id="status_usuario_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-success" id="exampleModalLabel">Status</h3>
      </div>
      <div class="modal-body">
        El status ha sido modificado con éxito.
      </div>
      <div class="modal-footer">
        <button type="button" class="enc_btnCancelar" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>


<!--############################
        Modal eliminar USUARIO 
###########################-->
<div class="modal fade" id="eliminar_usuario" tabindex="-1" role="dialog" aria-labelledby="eliminar_cliente" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-danger">Eliminar usuario</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ¿Está seguro de eliminar este usuario?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btn_seguro_eliminar" class="btn btn-danger">Eliminar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="error_general_usuarios" tabindex="-1" role="dialog" aria-labelledby="error_title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-danger">¡Error!</h3>
      </div>
      <div class="modal-body text-break" id="msjErrorModal_usuarios">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exito_general_usuarios" tabindex="-1" role="dialog" aria-labelledby="error_title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title text-success">¡Éxito!</h3>
      </div>
      <div class="modal-body text-break" id="msjExitoModal_usuarios">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn_modal_exito">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo $ruta_global; ?>vistas/assets/js/script_usuarios.js"></script>