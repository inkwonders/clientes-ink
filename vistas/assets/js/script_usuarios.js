//funcion para ver más de un usuario
$(".btn_verMasUsuario").on("click", function(){
    nombre=$(this).attr("nombre");
    correo=$(this).attr("correo");
    telefono=$(this).attr("telefono");
    telefono2=$(this).attr("telefono2");
    if(telefono2 == ""){
        telefono2 = "Sin datos";
    }
    telefono3=$(this).attr("telefono3");
    if(telefono3 == ""){
        telefono3 = "Sin datos";
    }
    $('#modal_verMasUsuario').modal('show');
    $('#vm_nombre').html(nombre);
    $('#vm_correo').html(correo);
    $('#vm_telefono').html(telefono);
    $('#vm_telefono2').html(telefono2);
    $('#vm_telefono3').html(telefono3);
});

//funcion para editar un usuario

$(".btn_editarUsuario").on("click", function(){
    $('#modal_editarUsuario').modal('show');
    id_usuario = $(this).attr("key");
    nombre=$(this).attr("nombre");
    paterno=$(this).attr("apaterno");
    materno=$(this).attr("amaterno");
    correo=$(this).attr("correo");
    telefono=$(this).attr("telefono");
    telefono2=$(this).attr("telefono2");
    telefono3=$(this).attr("telefono3");

    $('#input_nombre_usuario_edit').val(nombre);
    $('#input_apellido_paterno_edit').val(paterno);
    $('#input_apellido_materno_edit').val(materno);
    $('#input_correo_edit').val(correo);
    $('#input_telefono_edit').val(telefono);
    $('#input_telefono2_edit').val(telefono2);
    $('#input_telefono3_edit').val(telefono3);

    $("#formEditarUsuario").submit(function(e) {
        e.preventDefault(); 
            var form = $(this);
            var datos = form.serialize()+"&id_usuario="+id_usuario;
            
            
            $.ajax({
                type: "POST",
                url: "ajax/usuarios.ajax.php",
                data: datos, 
                success: function(response)
                {   
                    // console.log(response);
                    $("#modal_editarUsuario").modal('hide');
                    if(response =="ok"){
                        $("#btn_modal_exito").attr("onclick","location.reload()");  
                        $("#msjExitoModal_usuarios").html("El usuario ha sido editado correctamente");
                        $("#exito_general_usuarios").modal("show");                        
                    }else{
                        $("#msjErrorModal_usuarios").html("Ocurrió un error, favor de intentarlo más tarde.");
                        $("#error_general_usuarios").modal("show");
                    }
                }
            });
            
        
    });
});


//funcion para validar campos requeridos al agregar usuario


$('#input_nombre_usuario_edit, #input_apellido_paterno_edit, #input_correo_edit, #input_telefono_edit, #input_contrasena_edit, #input_repetir_contrasena_edit').keyup(function() {
    if($('#input_nombre_usuario_edit').val() != '' && $('#input_apellido_paterno_edit').val() != ''  && $('#input_correo_edit').val() != '' && $('#input_telefono_edit').val() != ''){

        if($('#input_repetir_contrasena_edit').val() != $('#input_contrasena_edit').val()){

          $('.enc_btnAgregarNuevoUsuario_edit').addClass('deshabilitado');

        }else{

          $('.enc_btnAgregarNuevoUsuario_edit').removeClass('deshabilitado');

        }
  
    }else{

        $('.enc_btnAgregarNuevoUsuario_edit').addClass('deshabilitado');

    }

});


//funcion para forzar campos numéricos
function validateNumber(evt) {
    var e = evt || window.event;
    var key = e.keyCode || e.which;

    if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
    // numbers   
    key >= 48 && key <= 57 ||
    // Numeric keypad
    key >= 96 && key <= 105 ||
    // Backspace and Tab and Enter
    key == 8 || key == 9 || key == 13 ||
    // Home and End
    key == 35 || key == 36 ||
    // left and right arrows
    key == 37 || key == 39 ||
    // Del and Ins
    key == 46 || key == 45) {
        // input is VALID
    }
    else {
        // input is INVALID
        e.returnValue = false;
        if (e.preventDefault) e.preventDefault();
    }
}


//función para validar status del cliente

$('.btn_status_usuario').change(function(){
    status = $(this).prop('checked');
    id_usuario = $(this).attr("key");
    id_admin = $(this).attr("key2");
    datos = {
        "sk_usuario":  id_usuario,
        "status": status,
        "operacion": "status",
        "id_admin": id_admin
    }
    
    $.ajax({
           type: "POST",
           url: "ajax/usuarios.ajax.php",
           data: datos, 
           success: function(response)
           {
               if(response =="ok"){
                $('#status_usuario_modal').modal('show');
               }else{
                   console.log("error");
               }
           }
         });
});

//funcion para eliminar cliente
$(".c_eliminar_usuario").on("click", function(){
    id_usuario = $(this).attr("key");
   
    $("#btn_seguro_eliminar").on("click", function(){
        datos = {
            "sk_usuario":  id_usuario,
            "operacion": "eliminar"
        }
        
        $.ajax({
               type: "POST",
               url: "ajax/usuarios.ajax.php",
               data: datos, 
               success: function(response)
               {
                    $('#eliminar_usuario').modal('hide');
                    if(response =="ok"){
                        $("#btn_modal_exito").attr("onclick","location.reload()");  
                        $("#msjExitoModal_usuarios").html("El usuario ha sido eliminado correctamente");
                        $("#exito_general_usuarios").modal("show");                        
                    }else{
                        $("#msjErrorModal_usuarios").html("Ocurrió un error, favor de intentarlo más tarde.");
                        $("#error_general_usuarios").modal("show");
                    }
               }
             });
    
            });
});