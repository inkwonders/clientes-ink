function valida_votacion() {
  let calificacion = $("#inp_calificacion").val();
  let comentario = $("#comentario_calificacion").val();
  let tam_comentario = comentario.length;

  if (
    calificacion != 0 &&
    calificacion != undefined &&
    calificacion != "" &&
    tam_comentario > 0 &&
    tam_comentario <= 500
  ) {
    $("#btn_calificar_guardar").removeClass("deshabilitado");
  } else {
    $("#btn_calificar_guardar").addClass("deshabilitado");
  }
}

$("#comentario_calificacion").keyup(function () {
  valida_votacion();
});

$("#form_calificar_ticket").submit(function (e) {
  e.preventDefault();

  let key = $("#inp_key").val();
  let no = $("#inp_no").val();
  let hash = $("#inp_hash").val();
  let calificacion = $("#inp_calificacion").val();
  let comentario = $("#comentario_calificacion").val();
  let accion = "calificar";

  let datos = {
    key: key,
    no: no,
    hash: hash,
    calificacion: calificacion,
    comentario: comentario,
    accion: accion,
  };

  $.ajax({
    url: "ajax/tickets.ajax.php",
    type: "POST",
    data: datos,
    beforeSend: function () {
      $("#btns_calificar").addClass("oculto");
      $("#cont_loader_tickets").removeClass("oculto");
    },
    success: function (response) {
        // console.log(response);
      $("#calificar_caso").modal('hide');
      $("#cont_loader_tickets").addClass("oculto");
      $("#btns_calificar").removeClass("oculto");

      if (response == "error") {
        $("#msjErrorModal_tickets").html(
          "Ocurrió un error, favor de intentarlo más tarde."
        );
        $("#error_general_tickets").modal("show");
      } else if (response == "ok") {
        $("#btn_modal_exito").attr("onclick", "location.reload()");
        $("#msjExitoModal_tickets").html(
          "El ticket se calificó correctamente, gracias por tu retroalimentación."
        );
        $("#exito_general_tickets").modal("show");
      }
    },
  });
});

$(".btn_votar").click(function () {
  let valor = $(this).attr("val");

  if (valor == 1) {
    $("#voto_positivo").removeClass("grey_vot");
    $("#voto_positivo").addClass("green_vot");
    $("#voto_negativo").removeClass("red_vot");
    $("#voto_negativo").addClass("grey_vot");
    $("#inp_calificacion").val("1");
  } else {
    $("#voto_negativo").removeClass("grey_vot");
    $("#voto_negativo").addClass("red_vot");
    $("#voto_positivo").removeClass("green_vot");
    $("#voto_positivo").addClass("grey_vot");
    $("#inp_calificacion").val("2");
  }

  valida_votacion();
});

$(".ev_calificar").click(function () {
  let key = $(this).attr("key");
  let no = $(this).attr("no");
  let hash = $(this).attr("hash");

  $("#inp_key").val(key);
  $("#inp_no").val(no);
  $("#inp_hash").val(hash);

  $("#calificar_caso").modal("show");
});

$("#btn_cancelar_calificar").click(function () {
  $("#inp_calificacion").val(0);
  $("#inp_key").val("");
  $("#inp_no").val("");
  $("#inp_hash").val("");
});
