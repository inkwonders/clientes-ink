//función para agregar un nuevo cliente (nombre)
$("#form_agregar_cliente").submit(function(e) {

    e.preventDefault(); 

    var form = $(this);
    var datos = form.serialize();
    
    $.ajax({
            type: "POST",
            url: "ajax/cliente.ajax.php",
            data: datos, 
            success: function(response)
            {
                $("#agregar_cliente").modal('hide');
                if(response =="ok"){
                $("#btn_modal_exito").attr("onclick","location.reload()");  
                $("#msjExitoModal_clientes").html("El cliente se ha dado de alta correctamente");
                $("#exito_general_clientes").modal("show");                        
                }else{
                $("#msjErrorModal_clientes").html("Ocurrió un error, favor de intentarlo más tarde.");
                $("#error_general_clientes").modal("show");
                }
            }
         });

    
});

//Función para editar datos del cliente(nombre)
$(".c_editar_cliente").on("click", function(){

    nombre_cliente = $(this).attr("nombrecliente");
    $("#c_input_nombre_cliente").val(nombre_cliente);
    id_cliente = $(this).attr("key");
    id_admin = $(this).attr("key2");

    $("#form_editar_cliente").submit(function(e) {
        e.preventDefault(); 

        if(nombre_cliente ==  $("#c_input_nombre_cliente").val()){
            $("#respuesta_editar_cliente").html("El nombre es igual al anterior");
        }else{
        
        nombre_cliente_editado = $("#c_input_nombre_cliente").val();
        datos = {
            "sk_cliente":  id_cliente,
            "nombre_cliente_editado": nombre_cliente_editado,
            "id_admin": id_admin
        }
        
        $.ajax({
               type: "POST",
               url: "ajax/cliente.ajax.php",
               data: datos, 
               success: function(response)
               {
                    $("#editarCliente").modal('hide');
                    if(response =="ok"){
                        $("#btn_modal_exito").attr("onclick","location.reload()");  
                        $("#msjExitoModal_clientes").html("El cliente se ha editado correctamente");
                        $("#exito_general_clientes").modal("show");                        
                    }else{
                        $("#msjErrorModal_clientes").html("Ocurrió un error, favor de intentarlo más tarde.");
                        $("#error_general_clientes").modal("show");
                    }
               }
             });
    
            }
    });

});

//funcion para eliminar cliente
$(".c_eliminar_cliente").on("click", function(){
    id_cliente = $(this).attr("key");
   
    $("#btn_seguro_eliminar").on("click", function(){
        datos = {
            "sk_cliente":  id_cliente,
            "operacion": "eliminar"
        }
        
        $.ajax({
               type: "POST",
               url: "ajax/cliente.ajax.php",
               data: datos, 
               success: function(response)
               {
                    $("#eliminar_cliente").modal('hide');
                    if(response =="ok"){
                        $("#btn_modal_exito").attr("onclick","location.reload()");  
                        $("#msjExitoModal_clientes").html("El cliente se ha eliminado correctamente");
                        $("#exito_general_clientes").modal("show");                        
                    }else{
                        $("#msjErrorModal_clientes").html("Ocurrió un error, favor de intentarlo más tarde.");
                        $("#error_general_clientes").modal("show");
                    }
               }
             });
    
            });
});

//funcion para validar campos requeridos al agregar usuario

$('.enc_btnAgregarNuevoUsuario').addClass("deshabilitado");

$('#input_nombre_usuario, #input_apellido_paterno, #input_correo, #input_telefono, #input_contrasena, #input_repetir_contrasena').keyup(function() {
    if($('#input_nombre_usuario').val() != '' && $('#input_apellido_paterno').val() != ''  && $('#input_correo').val() != '' && $('#input_telefono').val() != '' && $('#input_contrasena').val() != '' && $('#input_repetir_contrasena').val() != ''){
        
        if($('#input_repetir_contrasena').val() != $('#input_contrasena').val()){

          $('.enc_btnAgregarNuevoUsuario').addClass('deshabilitado');

        }else{

          $('.enc_btnAgregarNuevoUsuario').removeClass('deshabilitado');

        }
    }else{
        $('.enc_btnAgregarNuevoUsuario').addClass('deshabilitado');
    }

});

//función para validar status del cliente

$('.btn_status_cliente').change(function(){
        status = $(this).prop('checked');
        id_cliente = $(this).attr("key");
        id_admin = $(this).attr("key2");
        datos = {
            "sk_cliente":  id_cliente,
            "status": status,
            "operacion": "status",
            "id_admin": id_admin
        }
        
        $.ajax({
               type: "POST",
               url: "ajax/cliente.ajax.php",
               data: datos, 
               success: function(response)
               {
                   if(response =="ok"){
                    $('#status_cliente_modal').modal('show');
                   }else{
                       console.log("error");
                   }
               }
             });
});



//funcion para forzar campos numéricos
function validateNumber(evt) {
    var e = evt || window.event;
    var key = e.keyCode || e.which;

    if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
    // numbers   
    key >= 48 && key <= 57 ||
    // Numeric keypad
    key >= 96 && key <= 105 ||
    // Backspace and Tab and Enter
    key == 8 || key == 9 || key == 13 ||
    // Home and End
    key == 35 || key == 36 ||
    // left and right arrows
    key == 37 || key == 39 ||
    // Del and Ins
    key == 46 || key == 45) {
        // input is VALID
    }
    else {
        // input is INVALID
        e.returnValue = false;
        if (e.preventDefault) e.preventDefault();
    }
}



//función para agregar nuevo usuario 
$(".c_agregar_usuario").on("click", function(){
    id_cliente = $(this).attr("key");
    agregar = "agregar";
    $("#formAgregarUsuario").submit(function(e) {
        e.preventDefault(); 
            var form = $(this);
            var datos = form.serialize()+"&operacion="+agregar+"&id_cliente="+id_cliente;
            
            $.ajax({
                type: "POST",
                url: "ajax/cliente.ajax.php",
                data: datos, 
                success: function(response)
                {
                    //console.log(response);
                    if(response=="ok"){
                        $("#agregarUsuario").modal('hide');                
                        $("#btn_modal_exito").attr("onclick","location.reload()");  
                        $("#msjExitoModal_clientes").html("El usuario se ha registrado correctamente");
                        $("#exito_general_clientes").modal("show");                                        
                    }else if(response=="correo"){
                        $('#respuesta_agregar_usuario').html("Este correo ya está registrado");
                        $('#input_correo').addClass('is-invalid');
                    }else{
                        $("#msjErrorModal_clientes").html("Ocurrió un error, favor de intentarlo más tarde.");
                        $("#error_general_clientes").modal("show");
                    }
                }
            });
             
    });

});

//funcion para limpiar todos los modales 
$(".close").on("click", function(){
    $("#respuesta_editar_cliente").html("");
    $("#respuesta_agregar_usuario").html("");
    $('#input_correo').removeClass('is-invalid');
});

$('#input_correo').keyup(function(){
    $('#input_correo').removeClass('is-invalid');
    $("#respuesta_agregar_usuario").html("");
});