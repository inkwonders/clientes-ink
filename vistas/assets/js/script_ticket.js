var bool_descripcion = false;
var limiteCaracteres = 1500;

$(".card-entradas_0").removeClass('collapsed-card');
$(".btn_ocultar_0").removeClass('fa-plus');
$(".btn_ocultar_0").addClass('fa-minus');

//$('#inp_descripcion_entrada').summernote();



$("#inp_descripcion_entrada").summernote({
    height: "200",
    lang: "es-ES",
    disableDragAndDrop: true,
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['table', ['table']]
      // ,
      // ['insert', ['link']]
    ],
    callbacks: {
      onKeydown: function(e) {   
          
        var caracteres = $("#inp_descripcion_entrada").val().replace(/(<([^>]+)>)/ig,"");
        var totalCaracteres = caracteres.length;                  

        var key = e.keyCode;
        allowed_keys = [8, 37, 38, 39, 40, 46]

        if($.inArray(key, allowed_keys) != -1){            
          return true;

        }else if(totalCaracteres >= limiteCaracteres){           
          e.preventDefault();
          e.stopPropagation();
          return false;
         
        }else{}

      },
      onChange: function(contents) {
      
        var caracteres = contents.replace(/(<([^>]+)>)/ig,"");
        var totalCaracteres = caracteres.length;   
        
        $("#c_cliente").html("Caracteres " + totalCaracteres + " / "+limiteCaracteres); 

        if(totalCaracteres > limiteCaracteres){           
         
         $("#c_cliente").addClass("text-danger");

         bool_descripcion = false;

        }else if(totalCaracteres == 0){

          $("#c_cliente").addClass("text-danger");

          bool_descripcion = false;

        }else{

          bool_descripcion = true;

          $("#c_cliente").removeClass("text-danger");

        }   

        valida_boton(); 
      },
      onImageUpload: function (data) {
          data.pop();
      }
    }           
  });

  

$("#form_entrada").submit(function (e) {
  e.preventDefault();

  let ruta_global = $("#key").attr('rg');

  var datos_entrada = new FormData();
  
  datos_entrada.append("key", $("#key").val());
  datos_entrada.append("tipo_entrada", $("#tipo_entrada").val());
  datos_entrada.append("cp", $("#cp").val());
  datos_entrada.append("descripcion", $("#inp_descripcion_entrada").val());

  let cont_archivos = 0;

  for (var entradas of datos_validados.entries()) {
    cont_archivos++;
    let nombre = "archivo_" + cont_archivos;
    let archivo = entradas[1];
    datos_entrada.append(nombre, archivo);
  }

  datos_entrada.append("total_archivos", cont_archivos);
  datos_entrada.append("accion", "alta");

  let valida = valida_campos();

  if (valida) {    

    // for (var entradas of datos_entrada.entries()) {      
    //   console.log(entradas);
    // }

    $.ajax({
      url: ruta_global+"ajax/entradas.ajax.php",
      type: "POST",
      data: datos_entrada,
      contentType: false,
      processData: false,
      cache: false,
      beforeSend: function () {
        $("#btns_entrada").addClass("oculto");
        $("#cont_loader_tickets").removeClass("oculto");
      },
      success: function (response) {
        $("#cont_loader_tickets").addClass("oculto");
        $("#btns_entrada").removeClass("oculto");
        console.log('resp: '+response);
        if (response == "error") {
          $("#msjErrorModal_tickets").html(
            "Ocurrió un error, favor de intentarlo más tarde."
          );
          $("#error_general_tickets").modal("show");
        } else if (response == "ok") {
          $("#modal_entrada").modal("hide");
          $("#btn_modal_exito").attr("onclick", "location.reload()");
          $("#msjExitoModal_tickets").html(
            "La entrada ha sido enviada correctamente."
          );
          $("#exito_general_tickets").modal("show");
        }
      },
    });

  }

});


function valida_campos(){    
  
    if(bool_descripcion){
        return true;
    }else{
        return false;
    }


}


function valida_boton(){

    let valida = valida_campos();
    
    if(valida){
        $("#btn_guardar_entrada").removeClass('deshabilitado');
    }else{
        $("#btn_guardar_entrada").addClass('deshabilitado');
    }

}

function go_tickets(){
  location.href = "tickets";
}

$("#btn_aceptar_cerrar_ticket").click(function () {  

  let key = $(this).attr('key');
  let key2 = $(this).attr('key2');
  let no = $(this).attr('no');
  let hash = $(this).attr('hash');  
  let accion = "cerrar";

  let datos = {
    key: key,
    key2: key2,
    no: no,
    hash: hash,
    accion: accion
  };

  $.ajax({
    url: ruta_global+"ajax/tickets.ajax.php",
    type: "POST",
    data: datos,
    beforeSend: function () {      
    },
    success: function (response) {
        console.log(response);              
      $('#cerrar_caso').modal('hide');
      if (response == "error") {
        $("#msjErrorModal_tickets").html(
          "Ocurrió un error, favor de intentarlo más tarde."
        );
        $("#error_general_tickets").modal("show");
      } else if (response == "ok") {
        $("#btn_modal_exito").attr("onclick", "location.reload()");
        $("#msjExitoModal_tickets").html(
          "El ticket ha sido cerrado correctamente."
        );
        $("#exito_general_tickets").modal("show");
      }
    },
  });
});