var bool_descripcion = false;
var limiteCaracteres = 1500;

$(".ep_btnNuevoProyecto").on("click",function(){
    $('#agregarProyecto').modal("show");
});

$("#descripcion_proyecto").summernote({
    height: "200",
    lang: "es-ES",  
    disableDragAndDrop: true,
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['table', ['table']]
      // ,
      // ['insert', ['link']]
    ],
    callbacks: {
      onKeydown: function(e) {   
          
        var caracteres = $("#descripcion_proyecto").val().replace(/(<([^>]+)>)/ig,"");
        var totalCaracteres = caracteres.length;                  

        var key = e.keyCode;
        allowed_keys = [8, 37, 38, 39, 40, 46]

        if($.inArray(key, allowed_keys) != -1){            
          return true;

        }else if(totalCaracteres >= limiteCaracteres){           
          e.preventDefault();
          e.stopPropagation();
          return false;
         
        }else{}

      },
      onChange: function(contents) {
      
        var caracteres = contents.replace(/(<([^>]+)>)/ig,"");
        var totalCaracteres = caracteres.length;   
        
        $("#car_descripcion_proyecto").html("Caracteres " + totalCaracteres + " / "+limiteCaracteres); 

        if(totalCaracteres > limiteCaracteres){           
         
         $("#car_descripcion_proyecto").addClass("text-danger");

         bool_descripcion = false;

        }else if(totalCaracteres == 0){

          $("#car_descripcion_proyecto").addClass("text-danger");

          bool_descripcion = false;

        }else{

          bool_descripcion = true;

          $("#car_descripcion_proyecto").removeClass("text-danger");

        }   

        valida_boton(); 
      },
      onImageUpload: function (data) {
          data.pop();
      }
    }           
  });

$("#formAgregarProyecto").submit(function (e) {
    e.preventDefault();
  
    var datos_proyecto= new FormData();
  
    datos_proyecto.append('nombre', $("#input_nombre_proyecto").val());
    datos_proyecto.append('cliente', $("#select_cliente_proyecto").val());
    datos_proyecto.append('descripcion', $("#descripcion_proyecto").val());
    datos_proyecto.append('usuario_alta', $("#usuario_alta").val());
  
    let cont_archivos = 0; 
  
    
    for (var entradas of datos_validados.entries()) {   

      
      cont_archivos++;
      let nombre = 'proyecto_'+cont_archivos;
      let archivo = entradas[1];

      datos_proyecto.append(nombre, archivo);
  
    } 
  
    datos_proyecto.append('total_archivos', cont_archivos);
    datos_proyecto.append('accion', 'alta');
    
    let valida = valida_campos();
    
    if(valida){
      $.ajax({
          url: "ajax/proyectos.ajax.php",
          type: "POST",
          data: datos_proyecto,
          contentType: false,
          processData: false,
          cache: false,
          beforeSend : function(){
              // for (var entradas of datos_proyecto.entries()) {      
              //   console.log(entradas);
              // }
              //console.log(cont_archivos);                                 
              $('#btns_agregar_proyecto').addClass('oculto');
              $('#cont_loader_proyectos').removeClass('oculto');
          },
          success: function(response){
            // console.log(response);            
            $("#agregarProyecto").modal('hide');
            if(response =="ok"){
              $("#btn_modal_exito").attr("onclick","location.reload()");  
              $("#msjExitoModal_proyectos").html("El proyecto se ha creado correctamente");
              $("#exito_general_proyectos").modal("show");                        
            }else{
              $('#cont_loader_proyectos').addClass('oculto');
              $('#btns_agregar_proyecto').removeClass('oculto');
              $("#msjErrorModal_proyectos").html("Ocurrió un error, favor de intentarlo más tarde.");
              $("#error_general_proyectos").modal("show");
            }
          }
        });
    }
  
  });


function valida_campos(){
    let nombre_proyecto = $("#input_nombre_proyecto").val();    
    let cliente_proyecto = $("#select_cliente_proyecto").val();

    let bool_nombre_proyecto = false, bool_cliente_proyecto = false;

    if(nombre_proyecto.length > 1 && nombre_proyecto.length <= 50){
        bool_nombre_proyecto = true;
    }    

    if(cliente_proyecto != '' && cliente_proyecto != '0' && cliente_proyecto != null){
        bool_cliente_proyecto = true;
    }


    if(bool_nombre_proyecto && bool_descripcion && bool_cliente_proyecto){                
        return true;
    }else{      
        return false;
    }

}

function valida_boton(){

    let valida = valida_campos();
    
    if(valida){
        $(".ep_btnAgregarProyecto").removeClass('deshabilitado');
    }else{
        $(".ep_btnAgregarProyecto").addClass('deshabilitado');
    }

}

$("#input_nombre_proyecto").keyup(function(){    
    valida_boton();
});

$("#descripcion_proyecto, .note-editable").keyup(function(){    
    valida_boton();
});

$("#select_cliente_proyecto").select2();  

$('.select2').on('select2:close', function (e) { 
    valida_boton();
});

$(".btn_ver_mas_proyecto").on("click", function(){

  let cliente = $(this).attr("cliente");
  let no = $(this).attr("no");
  let hash = $(this).attr("hash");

  let datos = {
    "cliente": cliente,
    "no" : no,
    "hash" : hash
  }

  $.ajax({
    url: 'ajax/modalProyectos.ajax.php',
    data: datos,
    type: "POST",
    beforeSend: function(){
      $('#contenido_modal_proyecto').html('');
    },
    success: function(respuesta) {
      // console.log(respuesta);
      $('#contenido_modal_proyecto').html(respuesta);           
      $("#modal_verMasProyecto").modal("show");
    }
  });
  
});
