var bool_descripcion = false;
var limiteCaracteres = 1500;

$("#inp_descripcion").summernote({
    height: "200",
    lang: "es-ES",
    disableDragAndDrop: true,
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['table', ['table']]
      // ,
      // ['insert', ['link']]
    ],
    callbacks: {
        onKeydown: function(e) {   
          
          var caracteres = $("#inp_descripcion").val().replace(/(<([^>]+)>)/ig,"");
          var totalCaracteres = caracteres.length;                  

          var key = e.keyCode;
          allowed_keys = [8, 37, 38, 39, 40, 46]

          if($.inArray(key, allowed_keys) != -1){            
            return true;

          }else if(totalCaracteres >= limiteCaracteres){           
            e.preventDefault();
            e.stopPropagation();
            return false;
           
          }else{}

        },
        onChange: function(contents) {
        
          var caracteres = contents.replace(/(<([^>]+)>)/ig,"");
          var totalCaracteres = caracteres.length;   
          
          $("#car_descripcion").html("Caracteres " + totalCaracteres + " / "+limiteCaracteres); 

          if(totalCaracteres > limiteCaracteres){           
           
           $("#car_descripcion").addClass("text-danger");

           bool_descripcion = false;

          }else if(totalCaracteres == 0){

            $("#car_descripcion").addClass("text-danger");

            bool_descripcion = false;

          }else{

            bool_descripcion = true;

            $("#car_descripcion").removeClass("text-danger");

          }   

          valida_campos(); 

        },
        onImageUpload: function (data) {
            data.pop();
        }
      
    }   

  });

$("#form_nuevo_ticket").submit(function (e) {
  e.preventDefault();

  var datos_ticket = new FormData();

  datos_ticket.append('titulo', $("#inp_titulo").val());
  datos_ticket.append('motivo', $("#select_motivo").val());
  datos_ticket.append('descripcion', $("#inp_descripcion").val());

  let cont_archivos = 0; 

  
  for (var entradas of datos_validados.entries()) {   

    cont_archivos++;
    let nombre = 'archivo_'+cont_archivos;
    let archivo = entradas[1];
    datos_ticket.append(nombre, archivo);

  } 

  datos_ticket.append('total_archivos', cont_archivos);
  datos_ticket.append('accion', 'alta');
  
  let valida = valida_campos();
  
  if(valida){
    $.ajax({
        url: "ajax/tickets.ajax.php",
        type: "POST",
        data: datos_ticket,
        contentType: false,
        processData: false,
        cache: false,
        beforeSend : function(){                                 
            $('#btn_guardar').addClass('oculto');
            $('#cont_loader_tickets').removeClass('oculto');
        },
        success: function(response){
            $('#cont_loader_tickets').addClass('oculto');
            $('#btn_guardar').removeClass('oculto');
             
            if(response == 'error'){
                $("#msjErrorModal_tickets").html("Ocurrió un error, favor de intentarlo más tarde.");
                $("#error_general_tickets").modal("show");
            }else if(response == 'ok'){
                $("#btn_modal_exito").attr("onclick","go_tickets()");  
                $("#msjExitoModal_tickets").html("El ticket se generó y envió correctamente, en breve recibirás respuesta por parte de muestro equipo de soporte.");
                $("#exito_general_tickets").modal("show");
            }
        }
      });
  }

});

function valida_campos(){
    let titulo = $("#inp_titulo").val();
    let motivo = $("#select_motivo").val();
  

    let bool_titulo = false, bool_motivo = false;

    if(titulo.length > 1 && titulo.length <= 50){
        bool_titulo = true;
    }

    if(motivo != '' && motivo != '0' && motivo != null){
        bool_motivo = true;
    }
  

    if(bool_titulo && bool_motivo && bool_descripcion){                
        return true;
    }else{      
        return false;
    }

}

function valida_boton(){

    let valida = valida_campos();
    
    if(valida){
        $("#btn_guardar").removeClass('deshabilitado');
    }else{
        $("#btn_guardar").addClass('deshabilitado');
    }

}

$("#inp_titulo").keyup(function(){    
    valida_boton();
});

$("#inp_descripcion, .note-editable").keyup(function(){    
    valida_boton();
});

$("#select_motivo").select2();  

$('.select2').on('select2:close', function (e) { 
    valida_boton();
});


function go_tickets(){
    location.href = "tickets";
}