paceOptions = {
    elements: false,
    //restartOnRequestAfter: false

    
    restartOnRequestAfter: true,
    restartOnPushState: true
}

Pace.on("done", function(){
    $(".pace").fadeOut(0);
});

$(".char_count").keyup(function () {
  let max = $(this).attr("maxlength");
  let elemento = $(this).attr("elem");
  let actual = $(this).val().length;

  if (actual == 0) {
    $("#" + elemento).html("");
  } else {
    $("#" + elemento).html(actual + "/" + max);
  }
});

$("#mostrar_pass_login").click(function () {
  let input = $(this).attr("inp");
  let tipo = $("#" + input).attr("type");

  if (tipo == "password") {
    $("#" + input).attr("type", "text");
    $(this).attr("src", ruta_global + "vistas/assets/img/icon-ocultar.svg");
    $(this).attr("title", "Ocultar contraseña");
  } else {
    $("#" + input).attr("type", "password");
    $(this).attr("src", ruta_global + "vistas/assets/img/icon-mostrar.svg");
    $(this).attr("title", "Mostrar contraseña");
  }
});

$("#form_ingreso").submit(function (e) {
  e.preventDefault();
  let datos = $(this).serialize();

  $.ajax({
    url: ruta_global + "ajax/ingreso.ajax.php",
    type: "POST",
    data: datos,
    beforeSend: function () {
      $("#btn_login").addClass("deshabilitado");
    },
    success: function (response) {
      $("#btn_login").removeClass("deshabilitado");
      $("#respuesta_login").html(response);
    },
  });
});

$(document).ready(function () {

  // $('#btnQuejas').addClass('active');
  ver_sugerencias();

  if (
    typeof positivos !== "undefined" &&
    typeof negativos !== "undefined" &&
    typeof abiertos !== "undefined" &&
    typeof cerrados !== "undefined"
  ) {
    $("#btnQuejas").addClass("active");
    
    google.charts.load("current", { packages: ["corechart"] });
    google.charts.setOnLoadCallback(drawStuff);
    function drawStuff() {
      var data = google.visualization.arrayToDataTable([
        ["Elemento", "Densidad", { role: "style" }],
        ["" + positivos, positivos, "color: #109618"],
        ["" + negativos, negativos, "color: #ff5733"],
        ["" + abiertos, abiertos, "color: #FF9900"],
        ["" + cerrados, cerrados, "color: #3366CC"],
      ]);
      var view = new google.visualization.DataView(data);
      view.setColumns([
        0,
        1,
        {
          calc: "stringify",
          sourceColumn: 1,
          type: "string",
          role: "annotation",
        },
        2,
      ]);
      var options = {
        width: "100%",
        legend: { position: "none" },
        axes: {
          x: {
            0: { side: "left", label: "" }, // Top x-axis.
          },
        },
        vAxis: { format: "decimal" },
        bar: { groupWidth: "50%" },
        backgroundColor: '#f4f6f9',
        sliceVisibilityThreshold: 0,
      };
      var chart = new google.visualization.ColumnChart(
        document.getElementById("contenido_grafica")
      );
      chart.draw(data, options);
    }
  }
});

function ver_sugerencias() {
  $(".tabla_casos").DataTable({
    order: [[0, "desc"]],
    // "ordering": false,
    scrollY: "65vh",
    responsive: true,
    columnDefs: [
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 3, targets: -2 },
        { responsivePriority: 2, targets: -1 }
    ],
    scrollCollapse: true,
    paging: true,
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, "Todos"],
    ],
    pagingType: "full_numbers",
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "No se encontraron resultados",
      info: "Página _PAGE_ de _PAGES_",
      scrollX: false,
      infoEmpty: "No se encontraron resultados",
      infoFiltered: "(Filtrado de un total de _MAX_ registros)",
      search: "Buscar:",
      paginate: {
        first: "Primero",
        previous: "Atrás",
        next: "Siguiente",
        last: "Último",
      },
    }
  });
  $(".tabla_casos2").DataTable({
    order: [[3, "desc"]],
    // "ordering": false,
    scrollY: "65vh",
    responsive: true,
    columnDefs: [
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 3, targets: -2 },
        { responsivePriority: 2, targets: -1 }
    ],
    scrollCollapse: true,
    paging: true,
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, "Todos"],
    ],
    pagingType: "full_numbers",
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "No se encontraron resultados",
      info: "Página _PAGE_ de _PAGES_",
      scrollX: false,
      infoEmpty: "No se encontraron resultados",
      infoFiltered: "(Filtrado de un total de _MAX_ registros)",
      search: "Buscar:",
      paginate: {
        first: "Primero",
        previous: "Atrás",
        next: "Siguiente",
        last: "Último",
      },
    }
  });
  $(".tabla_usuarios").DataTable({
    order: [[5, "desc"]],
    // "ordering": false,
    scrollY: "65vh",
    responsive: true,
    columnDefs: [
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 3, targets: -2 },
        { responsivePriority: 2, targets: -1 }
    ],
    scrollCollapse: true,
    paging: true,
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, "Todos"],
    ],
    pagingType: "full_numbers",
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "No se encontraron resultados",
      info: "Página _PAGE_ de _PAGES_",
      scrollX: false,
      infoEmpty: "No se encontraron resultados",
      infoFiltered: "(Filtrado de un total de _MAX_ registros)",
      search: "Buscar:",
      paginate: {
        first: "Primero",
        previous: "Atrás",
        next: "Siguiente",
        last: "Último",
      },
    }
  });
  $(".tabla_tickets").DataTable({
    order: [[0, "desc"]],
    // "ordering": false,
    scrollY: "65vh",
    responsive: true,
    columnDefs: [
        { responsivePriority: 1, targets: 0 },
        // { responsivePriority: 3, targets: -2 },
        { responsivePriority: 2, targets: -1 }
    ],
    scrollCollapse: true,
    paging: true,
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, "Todos"],
    ],
    pagingType: "full_numbers",
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "No se encontraron resultados",
      info: "Página _PAGE_ de _PAGES_",
      scrollX: false,
      infoEmpty: "No se encontraron resultados",
      infoFiltered: "(Filtrado de un total de _MAX_ registros)",
      search: "Buscar:",
      paginate: {
        first: "Primero",
        previous: "Atrás",
        next: "Siguiente",
        last: "Último",
      },
    }
  });
  //     }
  // });
}

$(".select2").select2({
  language: {

    noResults: function() {

      return "No se encontraron resultados";        
    }

  }
});

var array_archivos = [];
var archivos_validos = [];
var cont_i = 0;
var tam_archivo = true;
var tipo_archivo = true;
var cont_cantidad;
var datos_validados = new FormData();
function valida_archivo() {
  array_archivos[cont_i] = [];
  archivos_validos[cont_i] = [];
  array_archivos[cont_i] = $("#subida_de_archivo_" + cont_i)[0].files;
  for (
    let i = 0;
    i < array_archivos[cont_i].length && tam_archivo && tipo_archivo;
    i++
  ) {
    tam_archivo = array_archivos[cont_i][i].size <= 5000000;
    nombre_archivo = array_archivos[cont_i][i].name;
    let extension = nombre_archivo.split(".").pop();
    extension = extension.toLowerCase();
    tipo_archivo = arreglo_extensiones.includes(extension, 0);
    archivos_validos[cont_i][i] = true;
  }
  if (tam_archivo && tipo_archivo) {
    $(".subida_de_archivo_" + cont_i).css("display", "none");
    $(".contenido_inputs_files").append(
      '<input type="file" name="subida_de_archivo[]" value="" id="subida_de_archivo_' +
        (cont_i + 1) +
        '" style="display: none;" multiple="multiple">' +
        '<label class="btnEnviar subida_de_archivo_' +
        (cont_i + 1) +
        '" id="cargar_archivos" for="subida_de_archivo_' +
        (cont_i + 1) +
        '">Adjuntar archivos</label>'
    );
    $("#subida_de_archivo_" + (cont_i + 1)).change(function () {
      valida_archivo();
    });
    render_archivos();
    cont_i++;
  } else {
    // alert("Error");
    $("#subida_de_archivo_" + cont_i).val("");
    if (!tipo_archivo) {
      let extensiones = "";
      let tam_extensiones = arreglo_extensiones.length;

      for (let index = 0; index < tam_extensiones; index++) {
        let valor = arreglo_extensiones[index];
        extensiones = extensiones +'<b>'+valor+'</b>';
        if (index != tam_extensiones - 1) {
          extensiones = extensiones + ", ";
        }
      }
      $("#msjErrorModal_tickets").html("El archivo debe tener alguna de las siguientes extensiones: "+extensiones+".");
      $("#error_general_tickets").modal("show");
      // console.log(arreglo_extensiones);
    //   arreglo_extensiones.forEach((exten, i) => {
    //     $("#extenciones_permitidas").append((i > 0 ? ", " : "") + exten);
    //   });
    } else {
      $("#msjErrorModal_tickets").html("El peso máximo de los archivos es de <b>5MB</b>.");
      $("#error_general_tickets").modal("show");
    }
    tam_archivo = true;
    tipo_archivo = true;
  }
}


//validar archivos dos
var array_archivos_dos = [];
var archivos_validos_dos = [];
var cont_i_dos = 0;
var tipo_archivo_dos = true;
var datos_validados_dos = new FormData();

function valida_archivo_dos() {  

  array_archivos_dos[cont_i_dos] = [];
  archivos_validos_dos[cont_i_dos] = [];
  array_archivos_dos[cont_i_dos] = $("#subida_de_archivo_dos_" + cont_i_dos)[0].files;
  

  for (let i = 0; i < array_archivos_dos[cont_i_dos].length; i++) 
  {    
    archivos_validos_dos[cont_i_dos][i] = true;
  }


  $(".subida_de_archivo_dos_" + cont_i_dos).css("display", "none");
  $(".contenido_inputs_files").append('<input type="file" name="subida_de_archivo_dos[]" value="" id="subida_de_archivo_dos_' +(cont_i_dos + 1) +'" style="display: none;" multiple="multiple">' +'<label class="btnEnviar subida_de_archivo_dos_' +(cont_i_dos + 1) +'" id="cargar_archivos_dos" for="subida_de_archivo_dos_' +(cont_i_dos + 1) +'">Adjuntar archivos</label>');

  $("#subida_de_archivo_dos_" + (cont_i_dos + 1)).change(function () {
      valida_archivo_dos();
  });

  render_archivos_dos();
  cont_i_dos++;
  
}



// let input_files = document.querySelector('#subida_de_archivo_' + cont_i);
$("#subida_de_archivo_" + cont_i).change(function () {
  valida_archivo();
});

$("#subida_de_archivo_dos_" + cont_i_dos).change(function () {
  valida_archivo_dos();
});

// $(".cont_btn_center").on("dragover drop", function(e) {
//     e.preventDefault();
// }).on("drop", function(e) {
//     $("#subida_de_archivo_" + cont_i).prop("files", e.originalEvent.dataTransfer.files);
//     valida_archivo();
// });
function render_archivos() {
  $(".contenedor_archivos").html("");
  let contador_interno = 0;
  datos_validados = new FormData();
  let total_archivos = 0;
  array_archivos.forEach((archivos, i) => {
    for (let j = 0; j < archivos.length; j++) {
      if (archivos_validos[i][j]) {
        if (total_archivos < 5) {
          $(".contenedor_archivos").append('<div class="archivo_subir" id="archivo_'+i+"_"+j+'">'+"<span>"+archivos[j].name+"</span>"+'<div class="cancel_archivo" onclick="cancel_archivo('+i+", "+j+');"></div>'+"</div>");
          datos_validados.append("archivo_"+j, archivos[j]);
        } else {
          archivos_validos[i][j] = false;
        }
        total_archivos++;
      }
    }
  });
  cont_cantidad = 0;
  // for (var value of datos_validados.values()) {
  //    cont_cantidad++;
  // }
  // datos_validados.append('cont_cantidad', cont_cantidad);
  // console.log(cont_cantidad);
  if (total_archivos > 5) {
    $("#msjErrorModal_tickets").html("Sólo se permite subir <b>5</b> archivos.");
    $("#error_general_tickets").modal("show");
  }
  if (total_archivos >= 5) {
    $("#contenido_inputs_files_1").addClass("inhabilitado");
  } else {
    $("#contenido_inputs_files_1").removeClass("inhabilitado");
  }
}


function render_archivos_dos() {    

  $(".contenedor_archivos").html(""); 
  datos_validados = new FormData();
  array_archivos_dos.forEach((archivos, i) => {
    for (let j = 0; j < archivos.length; j++) {        
      if (archivos_validos_dos[i][j]) {
        $(".contenedor_archivos").append('<div class="archivo_subir" id="archivo_'+i+"_"+j+'">'+"<span>"+archivos[j].name+"</span>"+'<div class="cancel_archivo" onclick="cancel_archivo_dos('+i+", "+j+');"></div>'+"</div>");
        datos_validados.append("archivo_"+j, archivos[j]);             
      }
    }
  });  
 
}


function cancel_archivo(i, j) {
  archivos_validos[i][j] = false;
  render_archivos();
}
function cancel_archivo_dos(i, j) {  
  archivos_validos_dos[i][j] = false;
  render_archivos_dos();
}



$(".modal").modal({
  backdrop: "static",
  keyboard: false,
  show: false,
});
