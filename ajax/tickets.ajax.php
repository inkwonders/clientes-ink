<?php

require_once "../lib/funciones.php";

require_once "../controladores/rutas.controlador.php";

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";

require_once "../controladores/tickets.controlador.php";
require_once "../modelos/tickets.modelo.php";


class Tickets
{

  public function altaTicket()
  {

    $id_ticket = $this->id_ticket;
    $clave_ticket = $this->clave_ticket;
    $id_cliente = $this->id_cliente;
    $folio = $this->folio;
    $titulo = $this->titulo;
    $id_motivo = $this->id_motivo;
    $descripcion = $this->descripcion;
    $fecha_hoy = $this->fecha_hoy;
    $id_usuario = $this->id_usuario;
    $hash_ticket = $this->hash_ticket;
    $carpeta_ticket = $this->carpeta_ticket;

    $datos = array(
      "sk_ticket" => $id_ticket,
      "clave_ticket" => $clave_ticket,
      "fk_cliente" => $id_cliente,
      "folio" => $folio,
      "estatus" => "1",
      "titulo" => $titulo,
      "fk_motivo" => $id_motivo,
      "descripcion" => $descripcion,
      "calificacion" => NULL,
      "comentario_calificacion" => NULL,
      "fecha_calificacion" => NULL,
      "fecha_alta" => $fecha_hoy,
      "usuario_alta" => $id_usuario,
      "fecha_actualizacion" => NULL,
      "usuario_actualizacion" => NULL,
      "hash_acciones" => $hash_ticket,
      "carpeta_ticket" => $carpeta_ticket
    );

    $inserta_ticket = ControladorTickets::ctrAltaTicket('tickets_47aaef15', $datos);
    if ($inserta_ticket == 'ok') {
      return true;
    } else {
      return false;
    }
  }

  public function altaArchivosTicket()
  {

    $id_archivo = $this->id_archivo;
    $clave_archivo = $this->clave_archivo;
    $id_ticket = $this->id_ticket;
    $archivo = $this->archivo;
    $nombre_archivo_original = $this->nombre_archivo_original;
    $orden = $this->orden;
    $fecha_hoy = $this->fecha_hoy;
    $hash_archivo = $this->hash_archivo;

    $datos = array(
      "sk_archivo" => $id_archivo,
      "clave_archivo" => $clave_archivo,
      "fk_ticket" => $id_ticket,
      "archivo" => $archivo,
      "nombre_archivo_original" => $nombre_archivo_original,
      "orden" => $orden,
      "fecha_alta" => $fecha_hoy,
      "hash_acciones" => $hash_archivo
    );

    $inserta_archivo_ticket = ControladorTickets::ctrAltaArchivosTicket('tickets_archivos_cb0a4d09', $datos);
    if ($inserta_archivo_ticket == 'ok') {
      return true;
    } else {
      return false;
    }
  }

  public function calificaTicket()
  {

    $id_ticket = $this->id_ticket;
    $clave_ticket = $this->clave_ticket;
    $id_cliente = $this->id_cliente;
    $calificacion = $this->calificacion;
    $comentario = $this->comentario;
    $fecha_hoy = $this->fecha_hoy;
    $hash_ticket = $this->hash_ticket;

    $query = " calificacion = $calificacion, comentario_calificacion = '$comentario', fecha_calificacion = '$fecha_hoy' WHERE sk_ticket = '$id_ticket' AND clave_ticket = '$clave_ticket' AND hash_acciones = '$hash_ticket' AND fk_cliente = '$id_cliente'";

    $actualiza_ticket = ControladorTickets::ctrActualiza('tickets_47aaef15', $query);
    echo $actualiza_ticket;
  }

  public function cerrarTicket()
  {

    $id_cliente = $this->id_cliente;
    $id_ticket = $this->id_ticket;
    $clave_ticket = $this->clave_ticket;
    $hash_ticket = $this->hash_ticket;

    $tabla = "tickets_47aaef15";
    $query = " estatus = 4 WHERE sk_ticket = '$id_ticket' AND clave_ticket = '$clave_ticket' AND hash_acciones = '$hash_ticket' AND fk_cliente = '$id_cliente'";

    $cierra_ticket = ControladorTickets::ctrActualiza($tabla, $query);
    return $cierra_ticket;
  }

  public function enviarCorreoAdm()
  {
    $id_cliente = $this->id_cliente;
    $folio = $this->folio;
    $titulo = $this->titulo;
    $motivo = $this->nombre_motivo;
    $fecha_hoy = $this->fecha_hoy;
    $id_usuario = $this->id_usuario;
    $clave_ticket = $this->clave_ticket;

    $fecha_mostrar = mostrar_fecha_formato($fecha_hoy);
    $hora_mostrar = substr($fecha_hoy, 11, 5);

    $ruta_global = Rutas::ctrRuta();

    $datos_usuario = ControladorUsuarios::ctrConsultaUsuario('usuarios_35d99c1a', 'sk_usuario', $id_usuario);
    $datos_cliente = ControladorUsuarios::ctrConsultaUsuario('clientes_30f39860', 'sk_cliente', $id_cliente);
    $datos_ink = ControladorUsuarios::ctrConsultaUsuario('datos_ae1a8cc2', 'sk_datos', '4b69df73-dbf7-4e2b-8e83-17eb11799823');

    if (!empty($datos_ink['correo_secundario'])) {
      $correos_enviar_ink = $datos_ink['correo'] . "," . $datos_ink['correo_secundario'];
      $correos_ink = "<a style='text-decoration:none; color:#0174e0;' href='mailto:" . $datos_ink['correo'] . "'>" . $datos_ink['correo'] . "</a> y <a style='text-decoration:none; color:#0174e0;' href='mailto:" . $datos_ink['correo_secundario'] . "'>" . $datos_ink['correo_secundario'] . "</a>";
    } else {
      $correos_enviar_ink = $datos_ink['correo'];
      $correos_ink = "<a style='text-decoration:none; color:#0174e0;' href='mailto:" . $datos_ink['correo'] . "'>" . $datos_ink['correo'] . "</a>";
    }

    if (!empty($datos_ink['telefono_secundario'])) {
      $telefonos_ink = "<a style='text-decoration:none; color:#0174e0;' href='tel:" . $datos_ink['telefono'] . "'>" . $datos_ink['telefono'] . "</a> y <a style='text-decoration:none; color:#0174e0;' href='tel:" . $datos_ink['telefono_secundario'] . "'>" . $datos_ink['telefono_secundario'] . "</a>";
    } else {
      $telefonos_ink = "<a style='text-decoration:none; color:#0174e0;' href='tel:" . $datos_ink['telefono'] . "'>" . $datos_ink['telefono'] . "</a>";
    }

    $nombre_usuario = $datos_usuario['nombres'] . " " . $datos_usuario['paterno'] . " " . $datos_usuario['materno'];
    $nombre_cliente = $datos_cliente['nombre'];

    $img_logo = $ruta_global . "vistas/assets/img/logo2.png";
    $img_icono = $ruta_global . "vistas/assets/img/receipt.png";
    $ruta_btn = $ruta_global . "ticket/" . $clave_ticket;

    $to = $correos_enviar_ink;

    $subject = "Nuevo Ticket || Folio ".$folio." 🧾";

    $message = "
    <html>
    <head>
      <meta charset='utf-8'>
      <title></title>
      <style>
      * {
        box-sizing: border-box;
      }
      body {
        padding: 0;
        margin: 0;
      }
      .padre {
        width: 100%;
        font-family: arial;
        background-color: #fff;
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 0;
      }
      .contenedor {
        width: 600px;
        background-color: #efefef;
        margin-top: 50px;
        padding: 17px;
      }
      .mensaje {
        width: 100%;
        background-color: #fff;
        display: inline-block;
        margin: 0;
        padding: 14px;
    
      }
      .linea_h {
        width: 100%;
        height: 2px;
        background-color: #ebebeb;
      }
      .mensaje_sub_1 {
        width: 100%;
        font-size: 14px;
        padding: 7px 0 0 7px;
        color: #808080;
      }
      .mensaje_sub_2 {
        width: 100%;
        padding: 22px 14px 5px 14px;
        font-size: 13px;
        color: #999999;
        display: table;
      }
      .cont_boton {
        width: 100%;
        display: inline-block;
        text-align: center;
        align-items: center;
        justify-content: center;
        padding: 25px 0 14px 0;
      }
      .boton_pagar {
        background-color: #000;
        color: #fff;
        /* width: 70px; */
        height: 40px;
        padding: 2%;
        display: inline-block;
        text-align: center;
        font-size: 15px;
        cursor: pointer;
        margin: 0 auto !important;
        position: relative;
      }
      .cont_image {
        width: 100%;
        display: inline-block;
        padding-top: 25px;
        text-align: center;
      }
      .imagen {
        width: auto;    
        max-height: 200px;
      }
      .cont_txt {
        padding: 30px 0 16px 0;
        color: #808080;
      }
      .cont_image_logo {
        width: 100%;
        display: inline-block;
        padding: 20px 0 34px 0;
        text-align: center;
      }
      .logo_escuela {
        max-height: 80px;
      }
      .titulo{
        font-size: 18px;
        text-transform: uppercase;
      }      
      .footer1{
        padding: 10px 0 0 0;
        margin: 0 0 0 auto;
        display: table-cell;
        /* word-break: break-all; */
        width: 40%;
        line-height: 1.5;
      }
      .footer2{
        padding: 10px 0 0 0;
        line-height: 1.5;
        margin: 0 auto 0 0;
        display: table-cell;
        text-align: right;
        word-break: break-all;
        width: 40%;
      }
      </style>
    </head>
    <body>
      <div class='padre'>
        <div class='contenedor'>
          <div class='mensaje'>
            <div class='cont_image_logo'>
              <img class='logo_escuela' src='$img_logo'>
            </div>
            <div class='linea_h'></div>
            <div class='cont_image'>
              <img class='imagen' src='$img_icono'>
              <div class='cont_txt'>
                <span class='titulo'>Nuevo Ticket</span>
              </div>
            </div>
            <div class='linea_h'></div>
            <br>
            <div class='mensaje_sub_1'>
            Se ha generado un nuevo ticket de soporte: <br />
              <ul>
                <li>Cliente: <b>" . $nombre_cliente . "</b></li>
                <li>Usuario: <b>" . $nombre_usuario . "</b></li>
                <li>Título: <b>" . $titulo . "</b></li>
                <li>Motivo: <b>" . $motivo . "</b></li>
                <li>Fecha: <b>" . $fecha_mostrar . "</b></li>
                <li>Hora: <b>" . $hora_mostrar . " hrs</b></li>
              </ul>          
            </div>
            <div class='cont_boton'>
            <a href='$ruta_btn'>
              <div class='boton_pagar'>
                <span>Ver Ticket</span>
              </div>
            </a>
            </div>
          </div>
          <div class='mensaje_sub_2'>
            <div class='footer1'>
              " . $datos_ink['nombre'] . "<br />
              " . $datos_ink['direccion'] . ".<br />
              " . $correos_ink . "<br/>              
            </div>
          </div>
        </div>
      </div>
    </body>
    </html>
    ";

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: INK WONDERS<noreply@inkwonders.com>' . "\r\n";

    

    if (mail($to, $subject, $message, $headers)) {

      $msj = "ok";
    } else {

      $msj = "error";
    }

    return $msj;
  }

  public function enviarCorreoCliente()
  {
    $id_usuario = $this->id_usuario;
    $folio = $this->folio;
    $titulo = $this->titulo;
    $motivo = $this->nombre_motivo;
    $fecha_hoy = $this->fecha_hoy;
    $clave_ticket = $this->clave_ticket;

    $fecha_mostrar = mostrar_fecha_formato($fecha_hoy);
    $hora_mostrar = substr($fecha_hoy, 11, 5);

    $ruta_global = Rutas::ctrRuta();

    $datos_usuario = ControladorUsuarios::ctrConsultaUsuario('usuarios_35d99c1a', 'sk_usuario', $id_usuario);
    $datos_ink = ControladorUsuarios::ctrConsultaUsuario('datos_ae1a8cc2', 'sk_datos', '4b69df73-dbf7-4e2b-8e83-17eb11799823');

    if (!empty($datos_ink['correo_secundario'])) {
      $correos_ink = "<a style='text-decoration:none; color:#0174e0;' href='mailto:" . $datos_ink['correo'] . "'>" . $datos_ink['correo'] . "</a> y <a style='text-decoration:none; color:#0174e0;' href='mailto:" . $datos_ink['correo_secundario'] . "'>" . $datos_ink['correo_secundario'] . "</a>";
    } else {
      $correos_ink = "<a style='text-decoration:none; color:#0174e0;' href='mailto:" . $datos_ink['correo'] . "'>" . $datos_ink['correo'] . "</a>";
    }

    if (!empty($datos_ink['telefono_secundario'])) {
      $telefonos_ink = "<a style='text-decoration:none; color:#0174e0;' href='tel:" . $datos_ink['telefono'] . "'>" . $datos_ink['telefono'] . "</a> y <a style='text-decoration:none; color:#0174e0;' href='tel:" . $datos_ink['telefono_secundario'] . "'>" . $datos_ink['telefono_secundario'] . "</a>";
    } else {
      $telefonos_ink = "<a style='text-decoration:none; color:#0174e0;' href='tel:" . $datos_ink['telefono'] . "'>" . $datos_ink['telefono'] . "</a>";
    }

    $img_logo = $ruta_global . "vistas/assets/img/logo2.png";
    $img_icono = $ruta_global . "vistas/assets/img/receipt.png";
    $ruta_btn = $ruta_global . "ticket/" . $clave_ticket;

    $to = $datos_usuario['correo'];

    $subject = "Nuevo Ticket || Folio ".$folio." 🧾";

    $message = "
    <html>
    <head>
      <meta charset='utf-8'>
      <title></title>
      <style>
      * {
        box-sizing: border-box;
      }
      body {
        padding: 0;
        margin: 0;
      }
      .padre {
        width: 100%;
        font-family: arial;
        background-color: #fff;
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 0;
      }
      .contenedor {
        width: 600px;
        background-color: #efefef;
        margin-top: 50px;
        padding: 17px;
      }
      .mensaje {
        width: 100%;
        background-color: #fff;
        display: inline-block;
        margin: 0;
        padding: 14px;
    
      }
      .linea_h {
        width: 100%;
        height: 2px;
        background-color: #ebebeb;
      }
      .mensaje_sub_1 {
        width: 100%;
        font-size: 14px;
        padding: 7px 0 0 7px;
        color: #808080;
      }
      .mensaje_sub_2 {
        width: 100%;
        padding: 22px 14px 5px 14px;
        font-size: 13px;
        color: #999999;
        display: table;
      }
      .cont_boton {
        width: 100%;
        display: inline-block;
        text-align: center;
        align-items: center;
        justify-content: center;
        padding: 25px 0 14px 0;
      }
      .boton_pagar {
        background-color: #000;
        color: #fff;
        /* width: 70px; */
        height: 40px;
        padding: 2%;
        display: inline-block;
        text-align: center;
        font-size: 15px;
        cursor: pointer;
        margin: 0 auto !important;
        position: relative;
      }
      .cont_image {
        width: 100%;
        display: inline-block;
        padding-top: 25px;
        text-align: center;
      }
      .imagen {
        width: auto;    
        max-height: 200px;
      }
      .cont_txt {
        padding: 30px 0 16px 0;
        color: #808080;
      }
      .cont_image_logo {
        width: 100%;
        display: inline-block;
        padding: 20px 0 34px 0;
        text-align: center;
      }
      .logo_escuela {
        max-height: 80px;
      }
      .titulo{
        font-size: 18px;
        text-transform: uppercase;
      }      
      .footer1{
        padding: 10px 0 0 0;
        margin: 0 0 0 auto;
        display: table-cell;
        /* word-break: break-all; */
        width: 40%;
        line-height: 1.5;
      }
      .footer2{
        padding: 10px 0 0 0;
        line-height: 1.5;
        margin: 0 auto 0 0;
        display: table-cell;
        text-align: right;
        word-break: break-all;
        width: 40%;
      }
      </style>
    </head>
    <body>
      <div class='padre'>
        <div class='contenedor'>
          <div class='mensaje'>
            <div class='cont_image_logo'>
              <img class='logo_escuela' src='$img_logo'>
            </div>
            <div class='linea_h'></div>
            <div class='cont_image'>
              <img class='imagen' src='$img_icono'>
              <div class='cont_txt'>
                <span class='titulo'>Nuevo Ticket</span>
              </div>
            </div>
            <div class='linea_h'></div>
            <br>
            <div class='mensaje_sub_1'>
            Generaste un nuevo ticket de soporte: <br />
              <ul>   
                <li>Título: <b>" . $titulo . "</b></li>             
                <li>Motivo: <b>" . $motivo . "</b></li>                     
                <li>Fecha: <b>" . $fecha_mostrar . "</b></li>
                <li>Hora: <b>" . $hora_mostrar . " hrs</b></li>
              </ul>          
            </div>
            <div class='cont_boton'>
            <a href='$ruta_btn'>
              <div class='boton_pagar'>
                <span>Ver Ticket</span>
              </div>
            </a>
            </div>
          </div>
          <div class='mensaje_sub_2'>
            <div class='footer1'>
              " . $datos_ink['nombre'] . "<br />
              " . $datos_ink['direccion'] . ".<br />
              " . $correos_ink . "<br/>              
            </div>
          </div>
        </div>
      </div>
    </body>
    </html>
    ";

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: INK WONDERS<noreply@inkwonders.com>' . "\r\n";

    

    if (mail($to, $subject, $message, $headers)) {

      $msj = "ok";
    } else {

      $msj = "error";
    }

    return $msj;
  }

  public function enviarCorreoClienteCasoCerrado()
  {
    $id_ticket = $this->id_ticket;        
    $fecha_hoy = $this->fecha_hoy;
    $clave_ticket = $this->clave_ticket;

    $datos_ticket = ControladorUsuarios::ctrConsultaUsuario('tickets_47aaef15','sk_ticket',$id_ticket);
    $id_usuario = $datos_ticket['usuario_alta'];

    $titulo = $datos_ticket['folio'];
    $id_motivo = $datos_ticket['fk_motivo'];

    $datos_motivo = ControladorUsuarios::ctrConsultaUsuario('motivos_bb9aa773','sk_motivo',$id_motivo);
    $motivo = $datos_motivo['nombre'];    
    $folio = $datos_ticket['folio'];       

    $fecha_mostrar = mostrar_fecha_formato($fecha_hoy);
    $hora_mostrar = substr($fecha_hoy, 11, 5);

    $ruta_global = Rutas::ctrRuta();

    $datos_usuario = ControladorUsuarios::ctrConsultaUsuario('usuarios_35d99c1a', 'sk_usuario', $id_usuario);
    $datos_ink = ControladorUsuarios::ctrConsultaUsuario('datos_ae1a8cc2', 'sk_datos', '4b69df73-dbf7-4e2b-8e83-17eb11799823');

    if (!empty($datos_ink['correo_secundario'])) {
      $correos_ink = "<a style='text-decoration:none; color:#0174e0;' href='mailto:" . $datos_ink['correo'] . "'>" . $datos_ink['correo'] . "</a> y <a style='text-decoration:none; color:#0174e0;' href='mailto:" . $datos_ink['correo_secundario'] . "'>" . $datos_ink['correo_secundario'] . "</a>";
    } else {
      $correos_ink = "<a style='text-decoration:none; color:#0174e0;' href='mailto:" . $datos_ink['correo'] . "'>" . $datos_ink['correo'] . "</a>";
    }

    if (!empty($datos_ink['telefono_secundario'])) {
      $telefonos_ink = "<a style='text-decoration:none; color:#0174e0;' href='tel:" . $datos_ink['telefono'] . "'>" . $datos_ink['telefono'] . "</a> y <a style='text-decoration:none; color:#0174e0;' href='tel:" . $datos_ink['telefono_secundario'] . "'>" . $datos_ink['telefono_secundario'] . "</a>";
    } else {
      $telefonos_ink = "<a style='text-decoration:none; color:#0174e0;' href='tel:" . $datos_ink['telefono'] . "'>" . $datos_ink['telefono'] . "</a>";
    }

    $img_logo = $ruta_global . "vistas/assets/img/logo2.png";
    $img_icono = $ruta_global . "vistas/assets/img/receipt.png";
    $ruta_btn = $ruta_global . "ticket/" . $clave_ticket;

    $to = $datos_usuario['correo'];

    $subject = "Ticket cerrado || Folio $folio 🧾";

    $message = "
    <html>
    <head>
      <meta charset='utf-8'>
      <title></title>
      <style>
      * {
        box-sizing: border-box;
      }
      body {
        padding: 0;
        margin: 0;
      }
      .padre {
        width: 100%;
        font-family: arial;
        background-color: #fff;
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 0;
      }
      .contenedor {
        width: 600px;
        background-color: #efefef;
        margin-top: 50px;
        padding: 17px;
      }
      .mensaje {
        width: 100%;
        background-color: #fff;
        display: inline-block;
        margin: 0;
        padding: 14px;
    
      }
      .linea_h {
        width: 100%;
        height: 2px;
        background-color: #ebebeb;
      }
      .mensaje_sub_1 {
        width: 100%;
        font-size: 14px;
        padding: 7px 0 0 7px;
        color: #808080;
      }
      .mensaje_sub_2 {
        width: 100%;
        padding: 22px 14px 5px 14px;
        font-size: 13px;
        color: #999999;
        display: table;
      }
      .cont_boton {
        width: 100%;
        display: inline-block;
        text-align: center;
        align-items: center;
        justify-content: center;
        padding: 25px 0 14px 0;
      }
      .boton_pagar {
        background-color: #000;
        color: #fff;
        /* width: 70px; */
        height: 40px;
        padding: 2%;
        display: inline-block;
        text-align: center;
        font-size: 15px;
        cursor: pointer;
        margin: 0 auto !important;
        position: relative;
      }
      .cont_image {
        width: 100%;
        display: inline-block;
        padding-top: 25px;
        text-align: center;
      }
      .imagen {
        width: auto;    
        max-height: 200px;
      }
      .cont_txt {
        padding: 30px 0 16px 0;
        color: #808080;
      }
      .cont_image_logo {
        width: 100%;
        display: inline-block;
        padding: 20px 0 34px 0;
        text-align: center;
      }
      .logo_escuela {
        max-height: 80px;
      }
      .titulo{
        font-size: 18px;
        text-transform: uppercase;
      }      
      .footer1{
        padding: 10px 0 0 0;
        margin: 0 0 0 auto;
        display: table-cell;
        /* word-break: break-all; */
        width: 40%;
        line-height: 1.5;
      }
      .footer2{
        padding: 10px 0 0 0;
        line-height: 1.5;
        margin: 0 auto 0 0;
        display: table-cell;
        text-align: right;
        word-break: break-all;
        width: 40%;
      }
      </style>
    </head>
    <body>
      <div class='padre'>
        <div class='contenedor'>
          <div class='mensaje'>
            <div class='cont_image_logo'>
              <img class='logo_escuela' src='$img_logo'>
            </div>
            <div class='linea_h'></div>
            <div class='cont_image'>
              <img class='imagen' src='$img_icono'>
              <div class='cont_txt'>
                <span class='titulo'>TICKET CERRADO</span>
              </div>
            </div>
            <div class='linea_h'></div>
            <br>
            <div class='mensaje_sub_1'>
              Dimos por finalizado el ticket generado:<br />
              <ul>   
                <li>Título: <b>" . $titulo . "</b></li>             
                <li>Motivo: <b>" . $motivo . "</b></li>                     
                <li>Fecha del cierre: <b>" . $fecha_mostrar . "</b></li>
                <li>Hora del cierre: <b>" . $hora_mostrar . " hrs</b></li>
              </ul>          
            </div>
            <div class='cont_boton'>
            <a href='$ruta_btn'>
              <div class='boton_pagar'>
                <span>Calificar Soporte</span>
              </div>
            </a>
            </div>
          </div>
          <div class='mensaje_sub_2'>
            <div class='footer1'>
              " . $datos_ink['nombre'] . "<br />
              " . $datos_ink['direccion'] . ".<br />
              " . $correos_ink . "<br/>              
            </div>
          </div>
        </div>
      </div>
    </body>
    </html>
    ";

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: INK WONDERS<noreply@inkwonders.com>' . "\r\n";

    

    if (mail($to, $subject, $message, $headers)) {

      $msj = "ok";
    } else {

      $msj = "error";
    }

    return $msj;
  }

}

$ticket = new Tickets();

if (!empty($_POST["titulo"]) && !empty($_POST["motivo"]) && !empty($_POST["descripcion"]) && $_POST['accion'] == 'alta') {

  session_start();

  $id_usuario = $_SESSION['id_usuario'];
  $id_cliente = $_SESSION['id_cliente'];
  $titulo = $_POST["titulo"];
  $no_motivo = $_POST["motivo"];
  $descripcion = $_POST["descripcion"];
  $total_archivos = $_POST['total_archivos'];

  if (mb_strlen($titulo) > 50 || mb_strlen($descripcion) > 5000 ) {
    echo 'error';
    exit;
  }

  date_default_timezone_set('America/Mexico_City');
  $fecha_hoy = date("Y-m-d H:i:s");
  $fecha_hoy_corta = date("Y-m-d_H-i-s");

  $consulta_total_tickets = ControladorTickets::ctrConsultaTotalTickets();
  $total_tickets = $consulta_total_tickets['total'];

  $folio = $total_tickets + 1;

  $consulta_id_motivo = ControladorTickets::ctrConsultaMotivo('motivos_bb9aa773', 'clave_motivo', $no_motivo);
  $id_motivo = $consulta_id_motivo['sk_motivo'];
  $nombre_motivo = $consulta_id_motivo['nombre'];

  $consulta_uuid = ControladorTickets::ctrUUID();
  $id_ticket = $consulta_uuid['id'];
  $clave_ticket = genera_clave_unica();
  $hash_ticket = genera_hash();
  $carpeta_ticket = "ticket_" . $folio . "_" . $clave_ticket;

  $ticket->id_ticket = $id_ticket;
  $ticket->clave_ticket = $clave_ticket;
  $ticket->id_usuario = $id_usuario;
  $ticket->id_cliente = $id_cliente;
  $ticket->folio = $folio;
  $ticket->titulo = $titulo;
  $ticket->id_motivo = $id_motivo;
  $ticket->nombre_motivo = $nombre_motivo;
  $ticket->descripcion = $descripcion;
  $ticket->fecha_hoy = $fecha_hoy;
  $ticket->hash_ticket = $hash_ticket;
  $ticket->carpeta_ticket = $carpeta_ticket;

  $inserta_ticket = $ticket->altaTicket();

  $ruta =  "../vistas/assets/archivos/";
  $ruta_carpeta = $ruta . $carpeta_ticket;

  if (!mkdir($ruta_carpeta, 0777)) {
    $bool_carpeta = false;
  } else {
    $bool_carpeta = true;
  }

  if ($inserta_ticket && $bool_carpeta) {

    for ($index_archivos = 1; $index_archivos <= $total_archivos; $index_archivos++) {

      $name = $_FILES['archivo_' . $index_archivos]['name'];
      $tipo = $_FILES['archivo_' . $index_archivos]['type'];
      $tmp = $_FILES['archivo_' . $index_archivos]['tmp_name'];
      $tam = $_FILES['archivo_' . $index_archivos]['size'];

      $validacion_archivo = validarArchivo($name, $tam);

      if ($validacion_archivo) {

        $consulta_uuid_archivo = ControladorTickets::ctrUUID();
        $id_archivo = $consulta_uuid_archivo['id'];
        $clave_archivo = genera_clave_unica();
        $hash_archivo = genera_hash();

        $arr_extension = explode(".", $name);
        $extension = end($arr_extension);

        $nombre_archivo = "ticket_" . uniqid() . "_" . $clave_ticket . "_" . $fecha_hoy_corta . "." . $extension;

        $ticket->id_archivo = $id_archivo;
        $ticket->clave_archivo = $clave_archivo;
        $ticket->archivo = $nombre_archivo;
        $ticket->nombre_archivo_original = $name;
        $ticket->orden = $index_archivos;
        $ticket->hash_archivo = $hash_archivo;

        $ruta_archivo = $ruta_carpeta . "/" . $nombre_archivo;

        if (move_uploaded_file($tmp, $ruta_archivo)) {
          $inserta_archivo = $ticket->altaArchivosTicket();
        } else {
          $borraArchivosTicket = ControladorTickets::ctrBorrarTodo('tickets_archivos_cb0a4d09', 'fk_ticket', $id_ticket);
          $borrTicket = ControladorTickets::ctrBorrarTodo('tickets_47aaef15', 'sk_ticket', $id_ticket);
          rrmdir($ruta_carpeta);
          echo "error";
          exit;
        }
      } else {
        $borraArchivosTicket = ControladorTickets::ctrBorrarTodo('tickets_archivos_cb0a4d09', 'fk_ticket', $id_ticket);
        $borrTicket = ControladorTickets::ctrBorrarTodo('tickets_47aaef15', 'sk_ticket', $id_ticket);
        rrmdir($ruta_carpeta);
        echo 'error';
        exit;
      }
    }
    $correo_adm = $ticket->enviarCorreoAdm();
    $correo_cliente = $ticket->enviarCorreoCliente();
    echo 'ok';
    exit;
  } else {

    echo 'error';
    exit;
  }
} else if (!empty($_POST["key"]) && !empty($_POST["no"]) && !empty($_POST["hash"]) && !empty($_POST["calificacion"]) && !empty($_POST["comentario"]) && $_POST['accion'] == 'calificar') {

  session_start();

  $id_usuario = $_SESSION['id_usuario'];
  $id_cliente = $_SESSION['id_cliente'];
  $id_ticket = base64_decode($_POST["key"]);
  $clave_ticket = $_POST["no"];
  $hash_ticket = base64_decode($_POST["hash"]);
  $calificacion = $_POST["calificacion"];
  $comentario = $_POST["comentario"];

  if (mb_strlen($comentario) > 5000) {
    echo 'error';
    exit;
  }

  if ($calificacion != 1 && $calificacion != 2) {
    echo 'error';
    exit;
  }

  date_default_timezone_set('America/Mexico_City');
  $fecha_hoy = date("Y-m-d H:i:s");

  $ticket->id_ticket = $id_ticket;
  $ticket->clave_ticket = $clave_ticket;
  $ticket->id_cliente = $id_cliente;
  $ticket->calificacion = $calificacion;
  $ticket->comentario = $comentario;
  $ticket->fecha_hoy = $fecha_hoy;
  $ticket->hash_ticket = $hash_ticket;

  $ticket->calificaTicket();
} else if (!empty($_POST["key"]) && !empty($_POST["key2"]) && !empty($_POST["no"]) && !empty($_POST["hash"]) && $_POST['accion'] == 'cerrar') {

  $id_ticket = base64_decode($_POST["key"]);
  $id_cliente = base64_decode($_POST["key2"]);
  $clave_ticket = $_POST["no"];
  $hash_ticket = base64_decode($_POST["hash"]);

  date_default_timezone_set('America/Mexico_City');
  $fecha_hoy = date("Y-m-d H:i:s");

  $ticket->id_ticket = $id_ticket;
  $ticket->clave_ticket = $clave_ticket;
  $ticket->id_cliente = $id_cliente;
  $ticket->fecha_hoy = $fecha_hoy;
  $ticket->hash_ticket = $hash_ticket;

  $cierra_ticket = $ticket->cerrarTicket();

  if($cierra_ticket == 'ok'){
    $envia_correo_cierra_ticket = $ticket->enviarCorreoClienteCasoCerrado();
    echo $envia_correo_cierra_ticket;
  }else{
    echo $cierra_ticket;
  }
  
} else {
  echo 'error';
}

function genera_hash()
{
  $consulta_uuid = ControladorTickets::ctrUUID();
  $hash = base64_encode($consulta_uuid['id']);
  return $hash;
}
