<?php

require_once "../lib/funciones.php";
require_once "../controladores/rutas.controlador.php";
require_once "../controladores/proyectos.controlador.php";
require_once "../modelos/proyectos.modelo.php";

class modalProyectos
{

    public function datos_modal()
    {   
        $ruta = Rutas::ctrRuta();

        $no_proyecto = $this->no_proyecto;
        $hash_proyecto = $this->hash_proyecto;
        $nombre_cliente = $this->nombre_cliente;

        $consulta_proyecto = ControladorProyectos::ctrConsultaProyecto("proyectos_f60e66cc", "clave_proyecto", "hash_acciones", $no_proyecto, $hash_proyecto);

?>
        <div class="modal-header">
            <h3 class="modal-title text-info" id="exampleModalLabel">Detalles del proyecto</h3>
            <button type="button" class="close" id="cerrar_modal_proyecto" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="form-outline mb-4">
                <label class="form-label" for="form3Example2">Nombre del proyecto: </label>
                <span class="label" id="vm_nombre_proyecto"><?php echo $consulta_proyecto['titulo']; ?></span>
            </div>
            <div class="form-outline mb-4">
                <label class="form-label" for="form3Example2">Cliente: </label>
                <span class="label" id="vm_cliente"><?php echo $nombre_cliente; ?></span>
            </div>
            <div class="form-outline mb-4">
                <label class="form-label" for="form3Example2">Fecha de creación: </label>
                <span id="vm_fecha_proyecto"><?php echo mostrar_fecha_formato($consulta_proyecto['fecha_alta']); ?></span>
            </div>
            <div class="form-outline mb-4">
                <label class="form-label" for="form3Example2">Descripción: </label>
                <div class="label" id="vm_descripcion"><?php echo $consulta_proyecto['descripcion']; ?></div>
            </div>
            <div class="form-outline mb-4">
                <label class="form-label" for="form3Example2">Archivos: </label>
                <?php
                $id_proyecto = $consulta_proyecto['sk_proyecto'];
                $consulta_archivos_proyecto = ControladorProyectos::ctrConsultaArchivos('proyectos_archivos_49bc6db7', 'fk_proyecto', $id_proyecto, '');
                $numero_archivos = sizeof($consulta_archivos_proyecto);
                ?>
                <?php if ($numero_archivos > 0) : ?>
                    <div class="eta_archivos_contenedor">
                        <?php foreach ($consulta_archivos_proyecto as $key => $valueArchivoProyecto) :
                            $ruta_archivo = $ruta . "vistas/assets/archivos/proyectos/" . $valueArchivoProyecto['archivo'];
                            ?>
                            <a download="<?php echo $valueArchivoProyecto['nombre_archivo_original']; ?>" href="<?php echo $ruta_archivo; ?>" title="Descargar">
                                <span><?php echo $valueArchivoProyecto['nombre_archivo_original']; ?></span>
                            </a>
                        <?php endforeach; ?>
                    </div>
                <?php else : ?>
                    <span>No se adjuntaron archivos</span>
                <?php endif; ?>
            </div>
        </div>
<?php
    }
}


$datos = new modalProyectos();

if (!empty($_POST['no']) && !empty($_POST['hash']) && !empty($_POST['cliente'])) {

    $datos->no_proyecto = $_POST["no"];
    $datos->hash_proyecto = base64_decode($_POST["hash"]);
    $datos->nombre_cliente = $_POST["cliente"];

    $datos->datos_modal();
} else {
    echo "error";
}
?>