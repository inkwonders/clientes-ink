<?php

require_once "../controladores/clientes.controlador.php";
require_once "../modelos/clientes.modelo.php";

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";

require_once "../lib/funciones.php";


class IngresoCliente{

    public function altaCliente(){
      
      $nombre_cliente = $this->nombre_cliente;
      $usuario_alta = $this->usuario_alta;

      $consulta_uuid = Clientes::ctrUUID();
      $id_usuario = $consulta_uuid['id'];
      $activo = "1";
      $fecha = date("Y-m-d H:i:s"); 
      $code = genera_clave_unica();
      $hash = base64_encode($id_usuario);
      $alta_cliente = Clientes::ctrIngresoCliente('clientes_30f39860', $id_usuario, $nombre_cliente, $activo, $fecha, $usuario_alta, $code, $hash);
      echo $alta_cliente;
    }

    public function editarCliente(){
      
        $nombre_cliente_editado = $this->nombre_cliente_editado;
        $id_cliente = $this->sk_cliente;
        $id_admin = $this->id_admin;
        $fecha = date("Y-m-d H:i:s");
        
        $editar_cliente = Clientes::ctrEditarCliente('clientes_30f39860', $nombre_cliente_editado, $id_cliente, $id_admin, $fecha);
        echo $editar_cliente;
      }
    
      public function altaUsuario(){
        $consulta_uuid = Clientes::ctrUUID();
      
        $id_usuario = $consulta_uuid['id'];
        $u_nombre = $this->u_nombre;
        $apellidoP = $this->apellidoP;
        $apellidoM = $this->apellidoM;
        $fecha_nacimiento = $this->fecha_nacimiento;
        $telefono = $this->telefono;
        $telefono1 = $this->telefono2;
        $telefono2 = $this->telefono3;
        $correo = $this->correo;
        $contrasena = $this->contrasena;
        $contrasena = crypt($contrasena, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
        $usuario_alta = $this->usuario_alta;
        $activo = "1";
        $clave = genera_clave_unica();
        $fecha = date("Y-m-d H:i:s");
        $tipo_usuario = "2";
        $hash = base64_encode($id_usuario);



        $consulta_sk_relacion = Clientes::ctrUUID();
        $sk_usuario_cliente = $consulta_sk_relacion['id'];
        $id_cliente = $this->id_cliente;
        $clave_relacion = genera_clave_unica();
        $hash_relacion = base64_encode($sk_usuario_cliente);
        
        $consulta_correo = ControladorUsuarios::ctrConsultaUsuario('usuarios_35d99c1a','correo' ,$correo);

        if(empty($consulta_correo)){
        
        $alta_usuario = ControladorUsuarios::ctrAltaUsuario('usuarios_35d99c1a', $id_usuario, $u_nombre, $apellidoP, $apellidoM, $fecha_nacimiento, $telefono, $telefono1, $telefono2, $correo, $contrasena, $activo, $clave, $fecha, $tipo_usuario, $hash, $usuario_alta);
        if($alta_usuario=="ok"){
         $relacion_usuario_cliente = ControladorUsuarios::ctrRelacionUsuarioCliente('usuario_clientes_91caadc1', $sk_usuario_cliente, $id_usuario, $id_cliente, $clave_relacion, $hash_relacion);
         echo $relacion_usuario_cliente;
       }else{
         echo "error";
       }
      }else{
        echo "correo";
      }
      }

      public function editarStatusCliente(){
      
        $id_cliente = $this->sk_cliente;
        $status_cliente = $this->status;
        if($status_cliente == "false"){
          $status_cliente = 0;
        }else{
          $status_cliente = 1;
        }
        $fecha = date("Y-m-d H:i:s");
        $id_admin = $this->fk_admin;
        $editar_status_cliente = Clientes::ctrStatusCliente('clientes_30f39860', $status_cliente, $id_cliente, $fecha, $id_admin);
        echo $editar_status_cliente;
      }

      public function eliminarCliente(){
      
        $id_cliente = $this->sk_cliente;
        $eliminar_cliente = Clientes::ctrEliminarCliente('clientes_30f39860', $id_cliente);
        echo $eliminar_cliente;
      }

  }
  

  $clientes = new IngresoCliente(); 

  if(!empty($_POST['nombre_cliente'])){

    $clientes -> nombre_cliente = $_POST['nombre_cliente'];
    $clientes -> usuario_alta = $_POST['usuario_alta'];

    $clientes -> altaCliente();

  }else if(!empty($_POST['nombre_cliente_editado'])){
    $clientes -> nombre_cliente_editado = $_POST['nombre_cliente_editado'];
    $clientes -> sk_cliente = base64_decode($_POST['sk_cliente']);
    $clientes -> id_admin = base64_decode($_POST['id_admin']);
    $clientes -> editarCliente();

  }else if(!empty($_POST['u_nombre']) && $_POST['operacion']=="agregar"){
    $clientes -> u_nombre = $_POST['u_nombre'];
    $clientes -> apellidoP = $_POST['apellidoP'];
    $clientes -> apellidoM = $_POST['apellidoM'];
    $clientes -> fecha_nacimiento = $_POST['fecha_nacimiento'];
    $clientes -> correo = $_POST['correo'];
    $clientes -> telefono = $_POST['telefono'];
    $clientes -> telefono2 = $_POST['telefono2'];
    $clientes -> telefono3 = $_POST['telefono3'];
    $clientes -> contrasena = $_POST['contrasena'];
    $clientes -> usuario_alta = $_POST['usuario_alta'];
    $clientes -> contrasena_repetir = $_POST['contrasena_repetir'];
    $clientes -> id_cliente = base64_decode($_POST['id_cliente']);
    $clientes -> altaUsuario();

  }else if(!empty($_POST['sk_cliente']) && $_POST['operacion']=="status"){
    $clientes -> sk_cliente = base64_decode($_POST['sk_cliente']);
    $clientes -> status = $_POST['status'];
    $clientes -> fk_admin = base64_decode($_POST['id_admin']);
    $clientes -> editarStatusCliente();
  }else if(!empty($_POST['sk_cliente']) && $_POST['operacion']=="eliminar"){
    $clientes -> sk_cliente = base64_decode($_POST['sk_cliente']);
    $clientes -> eliminarCliente();
  }else{
      echo "error";
  }