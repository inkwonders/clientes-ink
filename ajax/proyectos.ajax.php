<?php

require_once "../lib/funciones.php";

require_once "../controladores/proyectos.controlador.php";
require_once "../modelos/proyectos.modelo.php";

require_once "../controladores/tickets.controlador.php";
require_once "../modelos/tickets.modelo.php";

class Proyecto
{

  public function altaProyecto()
  {
    $id_proyecto = $this->id_proyecto;
    $clave_proyecto = $this->clave_proyecto;
    $id_cliente = $this->id_cliente;
    $titulo = $this->titulo;
    $descripcion = $this->descripcion;
    $fecha_hoy = $this->fecha_hoy;
    $id_usuario = $this->id_usuario;
    $hash_proyecto = $this->hash_proyecto;

    $datos = array(
      "sk_proyecto" => $id_proyecto,
      "clave_proyecto" => $clave_proyecto,
      "fk_cliente" => $id_cliente,
      "estatus" => "1",
      "titulo" => $titulo,
      "descripcion" => $descripcion,
      "fecha_alta" => $fecha_hoy,
      "usuario_alta" => $id_usuario,
      "fecha_finalizacion" => NULL,
      "usuario_finalizacion" => NULL,
      "hash_acciones" => $hash_proyecto,
    );

    $inserta_proyecto = ControladorProyectos::ctrAltaProyecto('proyectos_f60e66cc', $datos);
    if ($inserta_proyecto == 'ok') {
      return true;
    } else {
      return false;
    }
  }

  public function altaArchivosProyecto()
  {

    $id_archivo = $this->id_archivo;
    $clave_archivo = $this->clave_archivo;
    $id_proyecto = $this->id_proyecto;
    $archivo = $this->archivo;
    $nombre_archivo_original = $this->nombre_archivo_original;
    $orden = $this->orden;
    $fecha_hoy = $this->fecha_hoy;
    $hash_archivo = $this->hash_archivo;

    $datos = array(
      "sk_archivo" => $id_archivo,
      "clave_archivo" => $clave_archivo,
      "fk_proyecto" => $id_proyecto,
      "archivo" => $archivo,
      "nombre_archivo_original" => $nombre_archivo_original,
      "orden" => $orden,
      "fecha_alta" => $fecha_hoy,
      "hash_acciones" => $hash_archivo
    );

    $inserta_archivo_proyecto = ControladorProyectos::ctrAltaArchivosProyecto('proyectos_archivos_49bc6db7', $datos);
    if ($inserta_archivo_proyecto == 'ok') {
      return true;
    } else {
      return false;
    }
  }
}

$proyecto = new Proyecto();

if (!empty($_POST["nombre"]) && !empty($_POST["cliente"]) && !empty($_POST["descripcion"]) && $_POST['accion'] == 'alta') {
  $consulta_uuid = ControladorTickets::ctrUUID();
  date_default_timezone_set('America/Mexico_City');
  $clave_proyecto = genera_clave_unica();
  $id_proyecto =  $consulta_uuid['id'];
  $total_archivos = $_POST["total_archivos"];

  if (mb_strlen($_POST["nombre"]) > 50 || mb_strlen($_POST["descripcion"]) > 5000) {
    echo 'error';
    exit;
  }

  $proyecto->id_proyecto = $id_proyecto;
  $proyecto->clave_proyecto = $clave_proyecto;
  $proyecto->id_cliente = base64_decode($_POST["cliente"]);
  $proyecto->titulo = $_POST["nombre"];
  $proyecto->descripcion = $_POST["descripcion"];
  $proyecto->fecha_hoy = date("Y-m-d H:i:s");
  $proyecto->id_usuario = base64_decode($_POST["usuario_alta"]);
  $proyecto->hash_proyecto = genera_hash();
  $proyecto->total_archivos = $total_archivos;


  $inserta_proyecto = $proyecto->altaProyecto();

  if ($inserta_proyecto) {

    $ruta =  "../vistas/assets/archivos/proyectos";   

    if($total_archivos > 0){

      for ($index_archivos = 1; $index_archivos <= $total_archivos; $index_archivos++) {

        $name = $_FILES['proyecto_' . $index_archivos]['name'];
        $tipo = $_FILES['proyecto_' . $index_archivos]['type'];
        $tmp = $_FILES['proyecto_' . $index_archivos]['tmp_name'];
        $tam = $_FILES['proyecto_' . $index_archivos]['size'];
  
        $consulta_uuid_archivo = ControladorTickets::ctrUUID();
        $id_archivo = $consulta_uuid_archivo['id'];
        $clave_archivo = genera_clave_unica();
        $hash_archivo = genera_hash();
  
        $arr_extension = explode(".", $name);
        $extension = end($arr_extension);
        $fecha_hoy_corta = date("Y-m-d_H-i-s");
  
        $nombre_archivo = "proyecto_" . uniqid() . "_" . $clave_proyecto . "_" . $fecha_hoy_corta . "." . $extension;
  
        $proyecto->id_archivo = $id_archivo;
        $proyecto->clave_archivo = $clave_archivo;
        $proyecto->archivo = $nombre_archivo;
        $proyecto->nombre_archivo_original = $name;
        $proyecto->orden = $index_archivos;
        $proyecto->hash_archivo = $hash_archivo;
  
        $ruta_archivo = $ruta . "/" . $nombre_archivo;
  
        if (move_uploaded_file($tmp, $ruta_archivo)) {
          $inserta_archivo = $proyecto->altaArchivosProyecto();
          $bool_archivos = true;
        } else {
          $bool_archivos = false;
        }        
  
      }

    }else{
      $bool_archivos = true;
    }    

    if( $bool_archivos ){
      echo "ok";
    }else{
      echo "error";
    }

  }

}

function genera_hash()
{
  $consulta_uuid = ControladorTickets::ctrUUID();
  $hash = base64_encode($consulta_uuid['id']);
  return $hash;
}
