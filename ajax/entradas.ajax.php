<?php

require_once "../lib/funciones.php";

require_once "../controladores/rutas.controlador.php";

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";

require_once "../controladores/tickets.controlador.php";
require_once "../modelos/tickets.modelo.php";



class Entradas
{

  public function altaEntrada()
  {

    $id_entrada = $this->id_entrada;
    $id_ticket = $this->id_ticket;
    $clave_entrada = $this->clave_entrada;
    $tipo_entrada = $this->tipo_entrada;
    $descripcion = $this->descripcion;
    $fecha_hoy = $this->fecha_hoy;
    $id_usuario = $this->id_usuario;
    $hash_entrada = $this->hash_entrada;
    $carpeta_entrada = $this->carpeta_entrada;


    $datos = array(
      "sk_entrada" => $id_entrada,
      "fk_ticket" => $id_ticket,
      "clave_entrada" => $clave_entrada,
      "tipo_entrada" => $tipo_entrada,
      "comentario" => $descripcion,
      "fecha_alta" => $fecha_hoy,
      "usuario_alta" => $id_usuario,
      "hash_acciones" => $hash_entrada,
      "carpeta_entrada" => $carpeta_entrada
    );

    if($tipo_entrada == 1){
      $estatus = 2;
    }else if($tipo_entrada == 2){
      $estatus = 3;
    }


    $inserta_entrada = ControladorTickets::ctrAltaEntradaTicket('tickets_entradas_09f26b6a', $datos);
    if ($inserta_entrada == 'ok') {


      $query = "fecha_actualizacion = '$fecha_hoy', usuario_actualizacion = '$id_usuario',  estatus = $estatus WHERE sk_ticket = '$id_ticket'";
      $actualiza_ticket = ControladorTickets::ctrActualiza('tickets_47aaef15',$query);
      if ($actualiza_ticket == 'ok') {
        return true;
      } else {
        return false;
      }

    } else {
      return false;
    }
  }

  public function altaArchivosEntrada()
  {

    $id_archivo = $this->id_archivo;
    $clave_archivo = $this->clave_archivo;
    $id_entrada = $this->id_entrada;
    $archivo = $this->archivo;
    $nombre_archivo_original = $this->nombre_archivo_original;
    $orden = $this->orden;
    $fecha_hoy = $this->fecha_hoy;
    $hash_archivo = $this->hash_archivo;

    $datos = array(
      "sk_archivo" => $id_archivo,
      "clave_archivo_entrada" => $clave_archivo,
      "fk_entrada" => $id_entrada,
      "archivo_entrada" => $archivo,
      "nombre_archivo_original" => $nombre_archivo_original,
      "orden" => $orden,
      "fecha_alta" => $fecha_hoy,
      "hash_acciones" => $hash_archivo
    );

    $inserta_archivo_entrada = ControladorTickets::ctrAltaArchivosEntrada('tickets_entradas_archivos_c53737c2', $datos);

    if ($inserta_archivo_entrada == 'ok') {
      return true;
    } else {
      return false;
    }
  }

  public function enviarCorreoAdm()
  {
    $id_ticket = $this->id_ticket;    
    $fecha_hoy = $this->fecha_hoy;

    $datos_ticket = ControladorUsuarios::ctrConsultaUsuario('tickets_47aaef15','sk_ticket',$id_ticket);
    $id_usuario = $datos_ticket['usuario_alta'];

    $titulo = $datos_ticket['folio'];
    $id_motivo = $datos_ticket['fk_motivo'];

    $datos_motivo = ControladorUsuarios::ctrConsultaUsuario('motivos_bb9aa773','sk_motivo',$id_motivo);
    $motivo = $datos_motivo['nombre'];

    $id_cliente = $datos_ticket['fk_cliente'];
    $folio = $datos_ticket['folio'];       
    $clave_ticket = $datos_ticket['clave_ticket'];

    $fecha_mostrar = mostrar_fecha_formato($fecha_hoy);
    $hora_mostrar = substr($fecha_hoy,11,5);

    $ruta_global = Rutas::ctrRuta();

    $datos_usuario = ControladorUsuarios::ctrConsultaUsuario('usuarios_35d99c1a','sk_usuario',$id_usuario);
    $datos_cliente = ControladorUsuarios::ctrConsultaUsuario('clientes_30f39860','sk_cliente',$id_cliente);
    $datos_ink = ControladorUsuarios::ctrConsultaUsuario('datos_ae1a8cc2','sk_datos','4b69df73-dbf7-4e2b-8e83-17eb11799823');
    
    if(!empty($datos_ink['correo_secundario'])){
      $correos_enviar_ink = $datos_ink['correo'].",".$datos_ink['correo_secundario'];
      $correos_ink = "<a style='text-decoration:none; color:#0174e0;' href='mailto:".$datos_ink['correo']."'>".$datos_ink['correo']."</a> y <a style='text-decoration:none; color:#0174e0;' href='mailto:".$datos_ink['correo_secundario']."'>".$datos_ink['correo_secundario']."</a>";
    }else{
      $correos_enviar_ink = $datos_ink['correo'];
      $correos_ink = "<a style='text-decoration:none; color:#0174e0;' href='mailto:".$datos_ink['correo']."'>".$datos_ink['correo']."</a>";
    }

    if(!empty($datos_ink['telefono_secundario'])){
      $telefonos_ink = "<a style='text-decoration:none; color:#0174e0;' href='tel:".$datos_ink['telefono']."'>".$datos_ink['telefono']."</a> y <a style='text-decoration:none; color:#0174e0;' href='tel:".$datos_ink['telefono_secundario']."'>".$datos_ink['telefono_secundario']."</a>";
    }else{
      $telefonos_ink = "<a style='text-decoration:none; color:#0174e0;' href='tel:".$datos_ink['telefono']."'>".$datos_ink['telefono']."</a>";
    }

    $nombre_usuario = $datos_usuario['nombres']." ".$datos_usuario['paterno']." ".$datos_usuario['materno'];
    $nombre_cliente = $datos_cliente['nombre'];

    $img_logo = $ruta_global."vistas/assets/img/logo2.png";
    $img_icono = $ruta_global."vistas/assets/img/receipt.png";
    $ruta_btn = $ruta_global."ticket/".$clave_ticket;    

    $to = $correos_enviar_ink;    
    
    $subject = "Recibiste una respuesta para el ticket de soporte || Folio $folio 🧾";

    $message = "
    <html>
    <head>
      <meta charset='utf-8'>
      <title></title>
      <style>
      * {
        box-sizing: border-box;
      }
      body {
        padding: 0;
        margin: 0;
      }
      .padre {
        width: 100%;
        font-family: arial;
        background-color: #fff;
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 0;
      }
      .contenedor {
        width: 600px;
        background-color: #efefef;
        margin-top: 50px;
        padding: 17px;
      }
      .mensaje {
        width: 100%;
        background-color: #fff;
        display: inline-block;
        margin: 0;
        padding: 14px;
    
      }
      .linea_h {
        width: 100%;
        height: 2px;
        background-color: #ebebeb;
      }
      .mensaje_sub_1 {
        width: 100%;
        font-size: 14px;
        padding: 7px 0 0 7px;
        color: #808080;
      }
      .mensaje_sub_2 {
        width: 100%;
        padding: 22px 14px 5px 14px;
        font-size: 13px;
        color: #999999;
        display: table;
      }
      .cont_boton {
        width: 100%;
        display: inline-block;
        text-align: center;
        align-items: center;
        justify-content: center;
        padding: 25px 0 14px 0;
      }
      .boton_pagar {
        background-color: #000;
        color: #fff;
        /* width: 70px; */
        height: 40px;
        padding: 2%;
        display: inline-block;
        text-align: center;
        font-size: 15px;
        cursor: pointer;
        margin: 0 auto !important;
        position: relative;
      }
      .cont_image {
        width: 100%;
        display: inline-block;
        padding-top: 25px;
        text-align: center;
      }
      .imagen {
        width: auto;    
        max-height: 200px;
      }
      .cont_txt {
        padding: 30px 0 16px 0;
        color: #808080;
      }
      .cont_image_logo {
        width: 100%;
        display: inline-block;
        padding: 20px 0 34px 0;
        text-align: center;
      }
      .logo_escuela {
        max-height: 80px;
      }
      .titulo{
        font-size: 18px;
        text-transform: uppercase;
      }      
      .footer1{
        padding: 10px 0 0 0;
        margin: 0 0 0 auto;
        display: table-cell;
        /* word-break: break-all; */
        width: 40%;
        line-height: 1.5;
      }
      .footer2{
        padding: 10px 0 0 0;
        line-height: 1.5;
        margin: 0 auto 0 0;
        display: table-cell;
        text-align: right;
        word-break: break-all;
        width: 40%;
      }
      </style>
    </head>
    <body>
      <div class='padre'>
        <div class='contenedor'>
          <div class='mensaje'>
            <div class='cont_image_logo'>
              <img class='logo_escuela' src='$img_logo'>
            </div>
            <div class='linea_h'></div>
            <div class='cont_image'>
              <img class='imagen' src='$img_icono'>
              <div class='cont_txt'>
                <span class='titulo'>Nueva Entrada</span>
              </div>
            </div>
            <div class='linea_h'></div>
            <br>
            <div class='mensaje_sub_1'>
              Recibiste una respuesta para tu ticket de soporte: <br />
              <ul>
                <li>Título: <b>".$titulo."</b></li>
                <li>Motivo: <b>".$motivo."</b></li>
                <li>Fecha de la nueva respuesta: <b>".$fecha_mostrar."</b></li>
                <li>Hora de la nueva respuesta: <b>".$hora_mostrar." hrs</b></li>
              </ul>          
            </div>
            <div class='cont_boton'>
            <a href='$ruta_btn'>
              <div class='boton_pagar'>
                <span>Ver Respuesta</span>
              </div>
            </a>
            </div>
          </div>
          <div class='mensaje_sub_2'>
            <div class='footer1'>
              ".$datos_ink['nombre']."<br />
              ".$datos_ink['direccion'].".<br />
              ".$correos_ink."<br/>              
            </div>
          </div>
        </div>
      </div>
    </body>
    </html>
    ";

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: INK WONDERS<noreply@inkwonders.com>' . "\r\n";
    
    

    if (mail($to, $subject, $message, $headers)) {

      $msj = "ok";
    } else {

      $msj = "error";
    }

    return $msj;

  }

  public function enviarCorreoCliente()
  {
    $id_ticket = $this->id_ticket;
    $fecha_hoy = $this->fecha_hoy;
    $tipo_entrada = $this->tipo_entrada;

    $datos_ticket = ControladorUsuarios::ctrConsultaUsuario('tickets_47aaef15','sk_ticket',$id_ticket);
    $id_usuario = $datos_ticket['usuario_alta'];    
    $titulo = $datos_ticket['folio'];
    $id_motivo = $datos_ticket['fk_motivo'];

    $datos_motivo = ControladorUsuarios::ctrConsultaUsuario('motivos_bb9aa773','sk_motivo',$id_motivo);
    $motivo = $datos_motivo['nombre'];


    $folio = $datos_ticket['folio'];       
    $clave_ticket = $datos_ticket['clave_ticket'];

    $fecha_mostrar = mostrar_fecha_formato($fecha_hoy);
    $hora_mostrar = substr($fecha_hoy,11,5);

    $ruta_global = Rutas::ctrRuta();

    $datos_usuario = ControladorUsuarios::ctrConsultaUsuario('usuarios_35d99c1a','sk_usuario',$id_usuario);
    $datos_ink = ControladorUsuarios::ctrConsultaUsuario('datos_ae1a8cc2','sk_datos','4b69df73-dbf7-4e2b-8e83-17eb11799823');
    
    if(!empty($datos_ink['correo_secundario'])){      
      $correos_ink = "<a style='text-decoration:none; color:#0174e0;' href='mailto:".$datos_ink['correo']."'>".$datos_ink['correo']."</a> y <a style='text-decoration:none; color:#0174e0;' href='mailto:".$datos_ink['correo_secundario']."'>".$datos_ink['correo_secundario']."</a>";
    }else{
      $correos_ink = "<a style='text-decoration:none; color:#0174e0;' href='mailto:".$datos_ink['correo']."'>".$datos_ink['correo']."</a>";
    }

    if(!empty($datos_ink['telefono_secundario'])){
      $telefonos_ink = "<a style='text-decoration:none; color:#0174e0;' href='tel:".$datos_ink['telefono']."'>".$datos_ink['telefono']."</a> y <a style='text-decoration:none; color:#0174e0;' href='tel:".$datos_ink['telefono_secundario']."'>".$datos_ink['telefono_secundario']."</a>";
    }else{
      $telefonos_ink = "<a style='text-decoration:none; color:#0174e0;' href='tel:".$datos_ink['telefono']."'>".$datos_ink['telefono']."</a>";
    }    

    $img_logo = $ruta_global."vistas/assets/img/logo2.png";
    $img_icono = $ruta_global."vistas/assets/img/receipt.png";
    $ruta_btn = $ruta_global."ticket/".$clave_ticket;    

    $to = $datos_usuario['correo'];

    if($tipo_entrada == 1){      
      $subject = "Recibiste una respuesta para tu ticket de soporte || Folio $folio 🧾";
      $msj_correo = "Recibiste una respuesta para tu ticket de soporte:";
    }else{
      $subject = "Respondiste tu ticket de soporte || Folio $folio 🧾";
      $msj_correo = "Respondiste tu ticket de soporte:";
    }

    $message = "
    <html>
    <head>
      <meta charset='utf-8'>
      <title></title>
      <style>
      * {
        box-sizing: border-box;
      }
      body {
        padding: 0;
        margin: 0;
      }
      .padre {
        width: 100%;
        font-family: arial;
        background-color: #fff;
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 0;
      }
      .contenedor {
        width: 600px;
        background-color: #efefef;
        margin-top: 50px;
        padding: 17px;
      }
      .mensaje {
        width: 100%;
        background-color: #fff;
        display: inline-block;
        margin: 0;
        padding: 14px;
    
      }
      .linea_h {
        width: 100%;
        height: 2px;
        background-color: #ebebeb;
      }
      .mensaje_sub_1 {
        width: 100%;
        font-size: 14px;
        padding: 7px 0 0 7px;
        color: #808080;
      }
      .mensaje_sub_2 {
        width: 100%;
        padding: 22px 14px 5px 14px;
        font-size: 13px;
        color: #999999;
        display: table;
      }
      .cont_boton {
        width: 100%;
        display: inline-block;
        text-align: center;
        align-items: center;
        justify-content: center;
        padding: 25px 0 14px 0;
      }
      .boton_pagar {
        background-color: #000;
        color: #fff;
        /* width: 70px; */
        height: 40px;
        padding: 2%;
        display: inline-block;
        text-align: center;
        font-size: 15px;
        cursor: pointer;
        margin: 0 auto !important;
        position: relative;
      }
      .cont_image {
        width: 100%;
        display: inline-block;
        padding-top: 25px;
        text-align: center;
      }
      .imagen {
        width: auto;    
        max-height: 200px;
      }
      .cont_txt {
        padding: 30px 0 16px 0;
        color: #808080;
      }
      .cont_image_logo {
        width: 100%;
        display: inline-block;
        padding: 20px 0 34px 0;
        text-align: center;
      }
      .logo_escuela {
        max-height: 80px;
      }
      .titulo{
        font-size: 18px;
        text-transform: uppercase;
      }      
      .footer1{
        padding: 10px 0 0 0;
        margin: 0 0 0 auto;
        display: table-cell;
        /* word-break: break-all; */
        width: 40%;
        line-height: 1.5;
      }
      .footer2{
        padding: 10px 0 0 0;
        line-height: 1.5;
        margin: 0 auto 0 0;
        display: table-cell;
        text-align: right;
        word-break: break-all;
        width: 40%;
      }
      </style>
    </head>
    <body>
      <div class='padre'>
        <div class='contenedor'>
          <div class='mensaje'>
            <div class='cont_image_logo'>
              <img class='logo_escuela' src='$img_logo'>
            </div>
            <div class='linea_h'></div>
            <div class='cont_image'>
              <img class='imagen' src='$img_icono'>
              <div class='cont_txt'>
                <span class='titulo'>Nueva Respuesta</span>
              </div>
            </div>
            <div class='linea_h'></div>
            <br>
            <div class='mensaje_sub_1'>
              ".$msj_correo."<br />
              <ul>                
                <li>Título: <b>".$titulo."</b></li>
                <li>Motivo: <b>".$motivo."</b></li>
                <li>Fecha de la nueva respuesta: <b>".$fecha_mostrar."</b></li>
                <li>Hora de la nueva respuesta: <b>".$hora_mostrar." hrs</b></li>
              </ul>          
            </div>
            <div class='cont_boton'>
            <a href='$ruta_btn'>
              <div class='boton_pagar'>
                <span>Ver Respuesta</span>
              </div>
            </a>
            </div>
          </div>
          <div class='mensaje_sub_2'>
            <div class='footer1'>
              ".$datos_ink['nombre']."<br />
              ".$datos_ink['direccion'].".<br />
              ".$correos_ink."<br/>
            </div>
          </div>
        </div>
      </div>
    </body>
    </html>
    ";

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: INK WONDERS<noreply@inkwonders.com>' . "\r\n";

    

    if (mail($to, $subject, $message, $headers)) {

      $msj = "ok";
    } else {

      $msj = "error";
    }

    return $msj;

  }

}


$entrada = new Entradas();


if (!empty($_POST["key"]) && !empty($_POST["descripcion"]) && !empty($_POST['tipo_entrada']) && !empty($_POST['tipo_entrada']) && $_POST['accion'] == 'alta') {

  session_start();

  $id_usuario = $_SESSION['id_usuario'];
  $descripcion = $_POST["descripcion"];
  $total_archivos = $_POST['total_archivos'];
  $tipo_entrada = $_POST['tipo_entrada'];
  $id_ticket = base64_decode($_POST['key']);
  $carpeta_ticket = base64_decode($_POST['cp']);

  if (mb_strlen($descripcion) > 5000) {
    echo 'error';
    exit;
  }

  date_default_timezone_set('America/Mexico_City');
  $fecha_hoy = date("Y-m-d H:i:s");
  $fecha_hoy_corta = date("Y-m-d_H-i-s");

  $consulta_uuid = ControladorTickets::ctrUUID();
  $id_entrada = $consulta_uuid['id'];
  $clave_entrada = genera_clave_unica();
  $hash_entrada = genera_hash();
  $carpeta_entrada = "entrada_" . $clave_entrada;


  $entrada->id_entrada = $id_entrada;
  $entrada->id_ticket = $id_ticket;
  $entrada->clave_entrada = $clave_entrada;
  $entrada->tipo_entrada = $tipo_entrada;
  $entrada->descripcion = $descripcion;
  $entrada->fecha_hoy = $fecha_hoy;
  $entrada->id_usuario = $id_usuario;
  $entrada->hash_entrada = $hash_entrada;
  $entrada->carpeta_entrada = $carpeta_entrada;


  $inserta_entrada = $entrada->altaEntrada();

  $ruta =  "../vistas/assets/archivos/";
  $ruta_carpeta = $ruta . $carpeta_ticket;
  $ruta_carpeta_entrada = $ruta . $carpeta_ticket . "/" . $carpeta_entrada;

  if (!mkdir($ruta_carpeta_entrada, 0777)) {
    $bool_carpeta = false;
  } else {
    $bool_carpeta = true;
  }


  if ($inserta_entrada && $bool_carpeta) {

    for ($index_archivos = 1; $index_archivos <= $total_archivos; $index_archivos++) {

      $name = $_FILES['archivo_' . $index_archivos]['name'];
      $tipo = $_FILES['archivo_' . $index_archivos]['type'];
      $tmp = $_FILES['archivo_' . $index_archivos]['tmp_name'];
      $tam = $_FILES['archivo_' . $index_archivos]['size'];

      $validacion_archivo = validarArchivo($name, $tam);

      if ($validacion_archivo) {

        $consulta_uuid_archivo = ControladorTickets::ctrUUID();
        $id_archivo = $consulta_uuid_archivo['id'];
        $clave_archivo = genera_clave_unica();
        $hash_archivo = genera_hash();

        $arr_extension = explode(".", $name);
        $extension = end($arr_extension);

        $nombre_archivo = "entrada_" . uniqid() . "_" . $clave_entrada . "_" . $fecha_hoy_corta . "." . $extension;

        $entrada->id_archivo = $id_archivo;
        $entrada->clave_archivo = $clave_archivo;
        $entrada->archivo = $nombre_archivo;
        $entrada->nombre_archivo_original = $name;
        $entrada->orden = $index_archivos;
        $entrada->hash_archivo = $hash_archivo;

        $ruta_archivo = $ruta_carpeta_entrada . "/" . $nombre_archivo;

        if (move_uploaded_file($tmp, $ruta_archivo)) {
          $inserta_archivo = $entrada->altaArchivosEntrada();
        } else {
          $borraArchivosEntrada = ControladorTickets::ctrBorrarTodo('tickets_entradas_archivos_c53737c2', 'fk_entrada', $id_ticket);
          $borraEntrada = ControladorTickets::ctrBorrarTodo('tickets_entradas_09f26b6a', 'sk_entrada', $id_ticket);
          rrmdir($ruta_carpeta_entrada);
          echo "error";
          exit;
        }
      } else {
        $borraArchivosEntrada = ControladorTickets::ctrBorrarTodo('tickets_entradas_archivos_c53737c2', 'fk_entrada', $id_ticket);
        $borraEntrada = ControladorTickets::ctrBorrarTodo('tickets_entradas_09f26b6a', 'sk_entrada', $id_ticket);
        rrmdir($ruta_carpeta_entrada);
        echo 'error';
        exit;
      }
    }
    $correo_cliente = $entrada->enviarCorreoCliente();

    if($tipo_entrada == 2){ $correo_adm = $entrada->enviarCorreoAdm();  }

    echo 'ok';
    exit;

  } else {

    echo 'error';
    exit;
  }
} else {
  echo 'error';
}

function genera_hash()
{
  $consulta_uuid = ControladorTickets::ctrUUID();
  $hash = base64_encode($consulta_uuid['id']);
  return $hash;
}
