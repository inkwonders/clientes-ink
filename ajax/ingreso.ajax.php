<?php
  require_once "../controladores/usuarios.controlador.php";
  require_once "../modelos/usuarios.modelo.php";

  class Ingreso{

    public function login(){
      
      $email_login = $this->email_login;
      $pass_login = $this->pass_login;            

      $consulta_usuario = ControladorUsuarios::ctrIngresoUsuario($email_login,$pass_login);            
      echo $consulta_usuario;

    }

  }

  $ingreso = new Ingreso(); 

  
  if(!empty($_POST['email_login']) && !empty($_POST['contraseña_login'])){
    
    $ingreso -> email_login = $_POST['email_login'];
    $ingreso -> pass_login = $_POST["contraseña_login"];       
        
    $ingreso -> login();

  }
