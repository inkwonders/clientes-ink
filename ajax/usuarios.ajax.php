<?php

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";

require_once "../lib/funciones.php";


class Usuarios{

    public function editarUsuario(){
        $u_nombre = $this->u_nombre;
        $apellidoP = $this->apellidoP;
        $apellidoM = $this->apellidoM;
        $fecha_nacimiento = $this->fecha_nacimiento;
        $telefono = $this->telefono;
        $telefono2 = $this->telefono2;
        $telefono3 = $this->telefono3;
        $correo = $this->correo;
        $contrasena = $this->contrasena;        
        $usuario_actualizacion = $this->usuario_actualizacion;
        $fecha = date("Y-m-d H:i:s");
        $sk_usuario = $this->sk_usuario;
        //Update de datos con contrasena
        if($contrasena != NULL || $contrasena != ""){          
            $contrasena = crypt($contrasena, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
            $actualizar_campos_contrasena = ControladorUsuarios::ctrActualizarDatosContrasenaUsuario('usuarios_35d99c1a', $u_nombre, $apellidoP, $apellidoM, $telefono, $correo, $sk_usuario, $fecha_nacimiento, $telefono2, $telefono3, $contrasena, $fecha, $usuario_actualizacion);
            echo $actualizar_campos_contrasena;

        }else{
          //Update de campos sin contrasena        
          $actualizar_campos = ControladorUsuarios::ctrActualizarRequeridosUsuario('usuarios_35d99c1a', $u_nombre, $apellidoP, $apellidoM, $telefono, $correo, $sk_usuario, $fecha_nacimiento, $telefono2, $telefono3, $fecha, $usuario_actualizacion);
          echo $actualizar_campos;
        }
    }

    public function editarStatusUsuario(){
      
        $id_usuario = $this->sk_usuario;
        $status_usuario = $this->status;
        if($status_usuario == "false"){
          $status_usuario = 0;
        }else{
          $status_usuario = 1;
        }
        $fecha = date("Y-m-d H:i:s");
        $id_admin = $this->fk_admin;
        $editar_status_usuario = ControladorUsuarios::ctrStatusUsuario('usuarios_35d99c1a', $status_usuario, $id_usuario, $fecha, $id_admin);
        echo $editar_status_usuario;
      }

      public function eliminarUsuario(){
      
        $id_usuario = $this->sk_usuario;
        $eliminar_relacion = ControladorUsuarios::ctrEliminarRelacion('usuario_clientes_91caadc1', $id_usuario);
        if($eliminar_relacion == "ok"){
            $eliminar_usuario = ControladorUsuarios::ctrEliminarUsuario('usuarios_35d99c1a', $id_usuario);
            echo $eliminar_usuario;
        }
      }
  }
  

  $usuarios = new Usuarios(); 

  if(!empty($_POST['u_nombre']) && !empty($_POST['apellidoP']) && !empty($_POST['apellidoM']) && !empty($_POST['correo']) && !empty($_POST['correo']) && !empty($_POST['telefono'])){

    $usuarios -> u_nombre = $_POST['u_nombre'];
    $usuarios -> apellidoP = $_POST['apellidoP'];
    $usuarios -> apellidoM = $_POST['apellidoM'];
    $usuarios -> fecha_nacimiento = $_POST['fecha_nacimiento'];
    $usuarios -> correo = $_POST['correo'];
    $usuarios -> telefono = $_POST['telefono'];
    $usuarios -> telefono2 = $_POST['telefono2'];
    $usuarios -> telefono3 = $_POST['telefono3'];
    $usuarios -> contrasena = $_POST['contrasena'];
    $usuarios -> usuario_actualizacion= $_POST['usuario_actualizacion'];
    $usuarios -> sk_usuario = base64_decode($_POST['id_usuario']);
    $usuarios -> editarUsuario();

  }else if(!empty($_POST['sk_usuario']) && $_POST['operacion']=="status"){
    $usuarios -> sk_usuario = base64_decode($_POST['sk_usuario']);
    $usuarios -> status = $_POST['status'];
    $usuarios -> fk_admin = base64_decode($_POST['id_admin']);
    $usuarios -> editarStatusUsuario();
  }else if(!empty($_POST['sk_usuario']) && $_POST['operacion']=="eliminar"){
    $usuarios -> sk_usuario = base64_decode($_POST['sk_usuario']);
    $usuarios -> eliminarUsuario();
  }else{
      echo "error";
  }